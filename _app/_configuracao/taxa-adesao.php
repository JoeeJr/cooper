<?php /*autenticador*/ include('../../admin/autenticador.php'); ?>
<?php /*controlador*/ include('../../admin/controler_sys.php'); ?>
<!DOCTYPE html>
<html lang="pt-br">
   <head>
      <meta charset="utf-8">
      <meta name="robots" content="noindex, nofollow">
      <title><?php include('../../includes/title.php'); ?></title>
      <meta name="viewport" content="width=device-width, initial-scale=1">

      <!-- FAV ICON -->
      <link rel="icon" type="image/png" href="http://<?= $server ?>/img/fav.png" />

      <!-- CSS -->
      <link rel="stylesheet" href="http://<?= $server ?>/css/bootstrap3.3.0.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/datepicker.css" />
      <link rel="stylesheet" href="http://<?= $server ?>/css/estilo.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/fontawesome.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/jquery.dataTables.min.css">
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.dataTables.min.js"></script>
      
      <!-- JAVASCRIPTS -->
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/cep.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/datapicker.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/bootstrap330.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.mask.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.maskMoney.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/sweet-alert.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.maskMoney.js"></script>
      
   </head>
   <body>
       <div id="throbber" style="display:none; min-height:120px;"></div>
      <div id="noty-holder"></div>
      <div id="wrapper">
         <!-- Menu Lateral -->
         <?php include('../../includes/menu.php') ?>

           <style>
      label {
        width: auto;
}
@media (min-width: 768px){
#wrapper {
    padding-left: 290px;
}
}
    </style>
    <?php 
      //value taxa adm
      $total_taxa = $class->SelectQtd("id_adesao", "taxa_adesao", "");
      if($total_taxa == 0){
            $valor = 0;
      }else{
            $taxa = $class->Select("valor", "taxa_adesao", "", "");
            $row = $taxa->fetch(PDO::FETCH_OBJ);
            $valor = number_format($row->valor,2,",",".");
      }
    ?>

    <div id="page-wrapper">
            <div class="container-fluid">
               <!-- Page Heading -->
               <div class="row" id="main" >
               
                  <div class="col-md-12 well">
                        <form method="POST" id="FormAdesao">
                     <div class="col-md-12">
                        <h3 class="rlk">Declarar taxa de Adesão</h3>
                     </div>
                     <div class="col-md-12">
                        <div class="col-md-3">
                       
                              <span>Valor:</span>
                              <input data-thousands="." data-decimal="," class="form-control adesao" value="<?= $valor ?>" id="adesao" name="adesao"  type="text">
                              <br>
                           
                        </div>
                        <br>
                        <div class="col-md-2">
                           
                              <button onclick="EditAdesao()"class="btn btn-warning" type="button">Adicionar</button>
                              <br>
                           
                        </div>
                     </div>
               </form>
                  </div>
                </div>
              </div>
            </div>

    <script type="text/javascript" src="http://<?= $server ?>/admin/_class/caminho_controler.js"></script>
    <script type="text/javascript" src="http://<?= $server ?>/_app/_configuracao/js/edit-adesao.js"></script>
   

     
     
     <script type="text/javascript" src="http://<?= $server ?>/js/menu-mobile.js"></script>  

   </body>

   </html>
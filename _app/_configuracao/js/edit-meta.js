var host = location.host;
var server = "";
if (host == 'localhost'){
    server = host+'/'+pasta;
}else{
    server = host;
}
	
	$(document).ready(function(){       
   		//  meta
  	 	$('.meta').mask('000000');

	});


  function EditMeta(){
	var meta = $('#meta').val();
	
	


	if (meta == '' ){
		swal({
	    	title: 'Preencha todos os campos',
	    	icon: 'warning',
	    	confirmButtonColor: '#3085d6',
	    	confirmButtonText: 'Ok'

	    })
	}else{

		// Serializa o form
		var formMeta = $('#FormMeta').serialize();
		$.ajax({

		    url: 'http://'+server+'/_app/_configuracao/class/update-meta.class.php',
		    type: "POST",
		    data:  formMeta,
		    success: function(data){
		    	
		    	if (data.trim() == 'ok'){
		    		swal({
				      title: "Meta Atualizada!",
				      icon: "success",
				    }).then((value) => {
		             	window.location.reload();
		            });

		    	}else{

		    		 swal({
				     	title: 'Verifique as informações e tente novamente!',
				     	icon: 'error',
				    	confirmButtonColor: '#3085d6',
				    	confirmButtonText: 'Ok'
				     })
		    	}
		    }         
	    });
	}

}
var host = location.host;
var server = "";
if (host == 'localhost'){
    server = host+'/'+pasta;
}else{
    server = host;
}

	//moeda
	$(function() {
    $('.50').maskMoney();
  })

  function EditTaxaAluguelCinq(){
	var aluguelCinq_valor = $('#valor').val();
	
	


	if (aluguelCinq_valor == '' ){
		swal({
	    	title: 'Preencha todos os campos',
	    	icon: 'warning',
	    	confirmButtonColor: '#3085d6',
	    	confirmButtonText: 'Ok'

	    })
	}else{

		// Serializa o form
		var formEditAluguelCinq = $('#FormEditAluguelCinq').serialize();
		$.ajax({

		    url: 'http://'+server+'/_app/_configuracao/class/update-taxa-aluguel-cinq.class.php',
		    type: "POST",
		    data:  formEditAluguelCinq,
		    success: function(data){
		    	
		    	if (data.trim() == 'ok'){
		    		swal({
				      title: "Taxa Atualizada!",
				      icon: "success",
				    }).then((value) => {
		             	 window.location.reload();
		            });

		    	}else{

		    		 swal({
				     	title: 'Verifique as informações e tente novamente!',
				     	icon: 'error',
				    	confirmButtonColor: '#3085d6',
				    	confirmButtonText: 'Ok'
				     })
		    	}
		    }         
	    });
	}

}
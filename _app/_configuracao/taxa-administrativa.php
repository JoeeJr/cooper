<?php /*autenticador*/ include('../../admin/autenticador.php'); ?>
<?php /*controlador*/ include('../../admin/controler_sys.php'); ?>
<!DOCTYPE html>
<html lang="pt-br">
   <head>
      <meta charset="utf-8">
      <meta name="robots" content="noindex, nofollow">
      <title><?php include('../../includes/title.php'); ?></title>
      <meta name="viewport" content="width=device-width, initial-scale=1">

      <!-- FAV ICON -->
      <link rel="icon" type="image/png" href="http://<?= $server ?>/img/fav.png" />

      <!-- CSS -->
      <link rel="stylesheet" href="http://<?= $server ?>/css/bootstrap3.3.0.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/datepicker.css" />
      <link rel="stylesheet" href="http://<?= $server ?>/css/estilo.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/fontawesome.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/jquery.dataTables.min.css">
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.dataTables.min.js"></script>
      
      <!-- JAVASCRIPTS -->
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/cep.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/datapicker.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/bootstrap330.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.mask.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.maskMoney.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/sweet-alert.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.maskMoney.js"></script>
      
   </head>
   <body>
       <div id="throbber" style="display:none; min-height:120px;"></div>
      <div id="noty-holder"></div>
      <div id="wrapper">
         <!-- Menu Lateral -->
         <?php include('../../includes/menu.php') ?>

           <style>
      label {
        width: auto;
}
@media (min-width: 768px){
#wrapper {
    padding-left: 145px;
}
}
    </style>


    <?php
      //value taxa adm
      $total_taxa = $class->SelectQtd("id", "taxa_adm", "");
      if($total_taxa == 0){
            $valor = 0;
      }else{
            $taxa = $class->Select("valor", "taxa_adm", "", "");
            $row = $taxa->fetch(PDO::FETCH_OBJ);
            $valor = number_format($row->valor,2,",",".");
      }
      //value taxa aluguel
       $total_taxa_aluguel = $class->SelectQtd("id", "taxa_aluguel", "");
      if($total_taxa_aluguel == 0){
            $valorA = 0;
      }else{
            $taxaA = $class->Select("valor", "taxa_aluguel", "", "");
            $rowA = $taxaA->fetch(PDO::FETCH_OBJ);
            $valorA = number_format($rowA->valor,2,",",".");

      }
      //value taxa 50%
       //value taxa aluguel
       $total_taxa_50 = $class->SelectQtd("id", "taxa_aluguel_cinq", "");
      if($total_taxa_50 == 0){
            $valor50 = 0;
      }else{
            $taxa50 = $class->Select("valor", "taxa_aluguel_cinq", "", "");
            $row50 = $taxa50->fetch(PDO::FETCH_OBJ);
            $valor50 = number_format($row50->valor,2,",",".");
      }

      
    ?>
      

      <div id="throbber" style="display:none; min-height:120px;"></div>
      <div id="noty-holder"></div>
      <div id="wrapper">
         <!-- Navigation -->
        <?php include('../../includes/menu.php') ?>
         <div id="page-wrapper">
            <div class="container-fluid">
               <!-- Page Heading -->
               <div class="row" id="main" >
               
                  <div class="col-md-12 well">
                        <form method="POST" id="FormEditAdm">
                     <div class="col-md-12">
                        <h3 class="rlk">Taxa Administrativa</h3>
                     </div>
                     <div class="col-md-12">
                        <div class="col-md-3">
                         <form>
                              <span>Valor:</span>
                              <input data-thousands="." data-decimal="," class="form-control Adm" value="<?= $valor ?>" id="valorAdm" name="valorAdm"  type="text">
                              <br>
                           </form>
                        </div>
                        <br>
                        <div class="col-md-2">
                           <form>
                              <button onclick="EditTaxaAdm()"class="btn btn-warning" type="button">Adicionar</button>
                              <br>
                           </form>
                        </div>
                     </div>
               </form>
                  </div>
            <div class="col-md-12 well">
                  <form method="POST" id="FormEditAluguel">
                     <div class="col-md-12">
                        <h3 class="rlk">Taxa Aluguel</h3>
                     </div>
                     <div class="col-md-12">
                        <div class="col-md-3">
                           
                              <span>Valor:</span>
                              <input data-thousands="." data-decimal="," class="form-control Aluguel" value="<?= $valorA ?>" id="valorAluguel" name="valorAluguel"  type="text">
                              <br>
                           
                        </div>
                        <br>
                        <div class="col-md-2">
                           
                              <button type="button" onclick="EditTaxaAluguel()"class="btn btn-warning">Adicionar</button>
                              <br>
                           
                        </div>
                     </div>
               </form>
                  </div>

                  <div class="col-md-12 well">
                     <form method="POST" id="FormEditAluguelCinq">
                     <div class="col-md-12">
                        <h3 class="rlk">Taxa Aluguel 50%</h3>
                     </div>
                     <div class="col-md-12">
                        <div class="col-md-3">
                          
                              <span>Valor:</span>
                              <input data-thousands="." data-decimal="," class="form-control 50" value="<?= $valor50 ?>" id="valor" name="valor"  type="text">
                              <br>
                         
                        </div>
                        <br>
                        <div class="col-md-2">
                           
                              <button type="button" onclick="EditTaxaAluguelCinq()" class="btn btn-warning">Adicionar</button> 
                              <br>
                           </form>
                        </div>
                     </div>
                  </div>


            </div>





          </div>
        </div>
      </div>
    </div>
  </div>   
             


           



      <script type="text/javascript" src="http://<?= $server ?>/admin/_class/caminho_controler.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/_app/_configuracao/js/edit-taxa-adm.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/_app/_configuracao/js/edit-taxa-aluguel.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/_app/_configuracao/js/edit-taxa-aluguel-cinq.js"></script>

     
     
     <script type="text/javascript" src="http://<?= $server ?>/js/menu-mobile.js"></script>  

   </body>

   </html>
<?php 
	/*controlador*/ 
	include('../controler.php');

	$matricula = $_POST['matricula'];

	// Query
	$sql = "SELECT id, nome
			FROM cooperados
			WHERE id = ?";


	// Prepara a query
	$query = $pdo->prepare($sql);

	// Passa o parametro
	$query->bindParam(1, $matricula, PDO::PARAM_INT);

	// Executa
	$query->execute();

	// Faz o array da query
	$row = $query->fetch(PDO::FETCH_OBJ);

	// Quantidade de registros encontrados
	$total = $query->rowCount();

	if ($total == 0){
		echo json_encode('not_find');
	}else{
		$array = array();
		$array[0] = $row->id;
		$array[1] = $row->nome;

		echo json_encode($array);
	}



	
?>
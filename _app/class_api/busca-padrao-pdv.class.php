<?php
	/*controlador*/ 
	include('../controler.php');

	// Verifica se o POST de delete chegou
	if ($_POST['buscaModelo']){

		// Busca o bairro
		$modelo = $_POST['buscaModelo'];
		$sql = $pdo->prepare("SELECT id, id_marca, modelo, sequencia FROM modelo_pdv WHERE id = ?");
		$sql->bindValue(1, $modelo);
		$sql->execute();

		$fetch = $sql->fetch(PDO::FETCH_OBJ);

		$array = array();
		$array[0] = $fetch->modelo;
		$array[1] = $fetch->id_marca;
		$array[2] = $fetch->id;
		$array[3] = $fetch->sequencia;


		echo json_encode($array);
	}
?>
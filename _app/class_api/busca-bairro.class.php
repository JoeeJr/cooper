<?php
	/*controlador*/ 
	include('../controler.php');

	// Verifica se o POST de delete chegou
	if ($_POST['buscaBairro']){

		// Busca o bairro
		$bairro = $_POST['buscaBairro'];
		$sql = $pdo->prepare("SELECT id, id_regiao, bairro FROM bairros WHERE id = ?");
		$sql->bindValue(1, $bairro);
		$sql->execute();

		$fetch = $sql->fetch(PDO::FETCH_OBJ);

		$array = array();
		$array[0] = $fetch->bairro;
		$array[1] = $fetch->id_regiao;
		$array[2] = $fetch->id;


		echo json_encode($array);
	}
?>
<?php
	/*controlador*/ 
	include('../controler.php');

	// Verifica se o POST de delete chegou
	if ($_POST['modelo']){

		// Busca o bairro
		$modelo = $_POST['modelo'];
		$sql = $pdo->prepare("SELECT sequencia FROM modelo_pdv WHERE id = ?");
		$sql->bindValue(1, $modelo);
		$sql->execute();

		$fetch = $sql->fetch(PDO::FETCH_OBJ);

		$array = array();
		$array[0] = $fetch->sequencia;


		echo json_encode($array);
	}
?>
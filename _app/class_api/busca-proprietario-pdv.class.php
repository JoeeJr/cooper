<?php
	/*controlador*/ 
	include('../controler.php');

	// Verifica se o POST de delete chegou
	if ($_POST['buscaPropietario']){

		// Busca o bairro
		$proprietario = $_POST['buscaPropietario'];
		$sql = $pdo->prepare("SELECT id_prop, nome, rg, cpf_cnpj FROM proprietarios_pdv WHERE id_prop = ?");
		$sql->bindValue(1, $proprietario);
		$sql->execute();

		$fetch = $sql->fetch(PDO::FETCH_OBJ);

		$array = array();
		$array[0] = $fetch->id_prop;
		$array[1] = $fetch->nome;
		$array[2] = $fetch->rg;
		$array[3] = $fetch->cpf_cnpj;


		echo json_encode($array);
	}
?>
<?php

include('../controler.php');

	$keyword = '%'.$_POST['keyword'].'%';

	$sql = "SELECT id_prop, nome
			FROM proprietarios_pdv
			WHERE  nome LIKE (:keyword)
			ORDER BY nome ASC 
			LIMIT 0, 3";



	$query = $pdo->prepare($sql);

	$query->bindParam(':keyword', $keyword, PDO::PARAM_STR);

	$query->execute();

	$list = $query->fetchAll();

	foreach ($list as $rs) {
		// put in bold the written text
		$country_name = str_replace($_POST['keyword'], '<b>'.$_POST['keyword'].'</b>', ($rs['nome']));
		// add new option
	    echo '<li class="listagem" onclick="set_item_pdv_prop(\''.str_replace("'", "\'", ($rs['nome'])).'\', \''.str_replace("'", "\'", $rs['id_prop']).'\')">'.$country_name.'</li>';
	}
?>
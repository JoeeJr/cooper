<?php 
	/*controlador*/ 
	include('../controler.php');

	$keyword = '%'.$_POST['keyword'].'%';

	$sql = "SELECT id, nome
			FROM cooperados
			WHERE nome LIKE (:keyword)
			ORDER BY nome ASC 
			LIMIT 0, 10";



	$query = $pdo->prepare($sql);

	$query->bindParam(':keyword', $keyword, PDO::PARAM_STR);

	$query->execute();

	$list = $query->fetchAll();

	foreach ($list as $rs) {
		// put in bold the written text
		$country_name = str_replace($_POST['keyword'], '<b>'.$_POST['keyword'].'</b>', utf8_decode($rs['nome']));
		// add new option
	    echo '<li class="listagem_prop_coop" onclick="set_prop_cooperado(\''.str_replace("'", "\'", utf8_decode($rs['nome'])).'\', \''.str_replace("'", "\'", $rs['id']).'\')">'.'('.$rs['id'].') - '.$country_name.'</li>';
	}
?>
<?php /*autenticador*/ include('../../admin/autenticador.php'); ?>
<?php /*controlador*/ include('../../admin/controler_sys.php'); ?>
<!DOCTYPE html>
<html lang="pt-br">
   <head>
      <meta charset="utf-8">
      <meta name="robots" content="noindex, nofollow">
      <title><?php include('../../includes/title.php'); ?></title>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- FAV ICON -->
      <link rel="icon" type="image/png" href="http://<?= $server ?>/img/fav.png" />
      <!-- CSS -->
      <link rel="stylesheet" href="http://<?= $server ?>/css/bootstrap3.3.0.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/estilo.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/fontawesome.css">

      <!-- JAVASCRIPTS -->
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/bootstrap330.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.mask.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/sweet-alert.js"></script>
      <script src="//cdn.ckeditor.com/4.9.2/basic/ckeditor.js"></script>
   </head>
   <style type="text/css">
      .trumbowyg-editor .trumbowyg-textarea{
        background: white!important;
      }
   </style>
   <body>
      <div id="throbber" style="display:none; min-height:120px;"></div>
      <div id="noty-holder"></div>
      <div id="wrapper">
         <!-- Navigation -->
         <?php include('../../includes/menu.php') ?>
         <div id="page-wrapper">
            <div class="container-fluid">
               <!-- Page Heading -->
               <div class="row" id="main" >
                  <div class="col-md-12 well">
                     <div class="col-md-12">
                        <h3 class="rlk">CADASTRO DE TERMOS</h3>
                     </div>
                     <div class="col-md-12">
                      <form method="post" id="form_cad_termos">
                        <div class="col-md-6">
                          <span>Título do Termos</span><b style="color:red">*</b>
                          <input id="titulo" name="titulo" placeholder="Ex: Nome do termo" class="form-control" type="text">
                        </div>

                        <br><br><br><br>

                        <div style="margin-bottom: 1%;" class="col-md-12">
                          <span>Termo</span><b style="color:red">*</b>
                          <textarea class="form-control cl_termo" id="termo"></textarea>
                        </div>

                        <div class="col-md-12">
                           <div class="form-inline required">

                              <div style="padding:0" class="col-md-2">
                                 <br>
                                 <div class="form-group has-feedback">
                                    <label class="input-group">
                                       <span class="input-group-addon">
                                       <input type="checkbox" name="cooperado" value="1" />
                                       </span>
                                       <div class="form-control form-control-static">
                                          Cooperado
                                       </div>
                                       <span class="glyphicon form-control-feedback "></span>
                                    </label>
                                 </div>
                              </div>

                              <div class="col-md-1">
                                &nbsp;
                              </div>

                              <div style="padding:0" class="col-md-2">
                                <br> 
                                 <div class="form-group has-feedback ">
                                    <label class="input-group">
                                       <span class="input-group-addon">
                                       <input type="checkbox" name="preposto" value="1" />
                                       </span>
                                       <div class="form-control form-control-static">
                                          Preposto 
                                       </div>
                                       <span class="glyphicon form-control-feedback "></span>
                                    </label>
                                 </div>
                              </div>

                           </div>

                           <div class="col-md-12">
                             &nbsp;
                           </div>

                           <div class="col-md-4 pull-right">
                              <span>&nbsp;</span>
                              <button type="button" class="btn btn-warning cad_termo form-control">CADASTRAR</button>
                           </div>
                          </form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

      
      <script type="text/javascript" src="http://<?= $server ?>/admin/_class/caminho_controler.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/menu-mobile.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/_app/_cadastros/js/cadastro-termos.js"></script>

      <script type="text/javascript">
          // Termo
          CKEDITOR.replace( 'termo' );
      </script>

   </body>
</html>
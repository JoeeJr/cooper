<?php
	// Caminho da chave de conexao ao banco
	include('../../controler.php');

	// Verifica se o POST de delete chegou
	if ($_POST['delPadrao']){
		// ID do modelo
		$modelo = $_POST['delPadrao'];

		// Remove o bairro
		$sql = $pdo->prepare("DELETE FROM modelo_pdv WHERE id = ?");
		$sql->bindValue(1, $modelo);
		$sql->execute();

		echo "ok";
	}
?>
<?php
	// Caminho da chave de conexao ao banco
	include('../../controler.php');

	// Verifica se o POST de delete chegou
	if ($_POST['delBairro']){

		// Atualiza os cooperados retirando o bairro
		$bairro = $_POST['delBairro'];
		$upFunc = $pdo->prepare("UPDATE cooperados SET bairro_com = '' WHERE bairro_com = ?");
		$upFunc->bindValue(1, $bairro);
		$upFunc->execute();

		// Remove o bairro
		$sql = $pdo->prepare("DELETE FROM bairros WHERE id = ?");
		$sql->bindValue(1, $bairro);
		$sql->execute();

		echo "ok";
	}
?>
<?php
	// Caminho da chave de conexao ao banco
	include('../../controler.php');

	// Verifica se o POST de delete chegou
	if ($_POST['delProp']){

		// Atualiza o PDV
		// $upFunc = $pdo->prepare("UPDATE cooperados SET bairro_com = '' WHERE bairro_com = ?");
		// $upFunc->bindValue(1, $bairro);
		// $upFunc->execute();

		// Remove o bairro
		$proprietario = $_POST['delProp'];
		$sql = $pdo->prepare("DELETE FROM proprietarios_pdv WHERE id_prop = ?");
		$sql->bindValue(1, $proprietario);
		$sql->execute();

		echo "ok";
	}
?>
<?php 
	// Caminho da chave de conexao ao banco
	include('../../controler.php');

	// Se recebeu os POSTS
	if ($_POST){
			$cad_marca = strtoupper($_POST['cad_marca']);
			$cad_modelo = strtoupper($_POST['cad_modelo']);
			$sequencia = strtoupper($_POST['sequencia']);

			// Busca a Região na base para verificar se há cadastro
			$sqlMAR = $pdo->prepare("SELECT id FROM marca_pdv WHERE marca = '$cad_marca'");
			$sqlMAR->execute();

			// Total de registros
			$total = $sqlMAR->rowCount();

			// Se nao encontrou registros, insere a REGIAO normalmente normalmente
			if ($total == 0){
				// INSERT da regiao
				$sql = $pdo->prepare("INSERT INTO marca_pdv (marca) VALUES (?)");

				$sql->bindValue(1, $cad_marca);
				$sql->execute();

				// Retorna o ID da regiao inserida
				$idMarca = $pdo->lastInsertId();
			}else{
				// Pega o ID selecionado na linha 11
				$arrayMAR = $sqlMAR->fetch(PDO::FETCH_ASSOC);
				$idMarca = $arrayMAR['id'];
			}


			// Cadastra o bairro
			$sqlMod = $pdo->prepare("INSERT INTO modelo_pdv (id_marca, modelo, sequencia) VALUES (?, ?, ?)");
			$sqlMod->bindValue(1, $idMarca);
			$sqlMod->bindValue(2, $cad_modelo);
			$sqlMod->bindValue(3, $sequencia);
			$sqlMod->execute();

		echo "ok";

	}else{
		echo "error";
		header("http://$server/");
	}

	
?>
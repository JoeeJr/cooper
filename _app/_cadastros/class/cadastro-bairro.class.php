<?php 
	// Caminho da chave de conexao ao banco
	include('../../controler.php');

	// Se recebeu os POSTS
	if ($_POST){
			$cad_regiao = strtoupper($_POST['cad_regiao']);
			$cad_bairro = strtoupper($_POST['cad_bairro']);

			// Busca a Região na base para verificar se há cadastro
			$sqlREG = $pdo->prepare("SELECT id FROM regioes WHERE regiao = '$cad_regiao'");
			$sqlREG->execute();

			// Total de registros
			$total = $sqlREG->rowCount();

			// Se nao encontrou registros, insere a REGIAO normalmente normalmente
			if ($total == 0){
				// INSERT da regiao
				$sql = $pdo->prepare("INSERT INTO regioes (regiao) VALUES (?)");

				$sql->bindValue(1, $cad_regiao);
				$sql->execute();

				// Retorna o ID da regiao inserida
				$idRegiao = $pdo->lastInsertId();
			}else{
				// Pega o ID selecionado na linha 11
				$arrayREG = $sqlREG->fetch(PDO::FETCH_ASSOC);
				$idRegiao = $arrayREG['id'];
			}


			// Cadastra o bairro
			$sqlBAI = $pdo->prepare("INSERT INTO bairros (id_regiao, bairro) VALUES (?, ?)");
			$sqlBAI->bindValue(1, $idRegiao);
			$sqlBAI->bindValue(2, $cad_bairro);
			$sqlBAI->execute();

		echo "ok";

	}else{
		echo "error";
		header("http://$server/");
	}

	
?>
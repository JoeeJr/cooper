<?php 
	// Caminho da chave de conexao ao banco
	include('../../controler.php');

	// Inicia a sessão
	if (!isset($_SESSION)){	session_start(); }

	// Se recebeu os POSTS
	if ($_POST){
			$cad_marca = $_POST['cad_marca'];
			$cad_modelo = $_POST['cad_modelo'];
			$cad_sequencia = $_POST['cad_sequencia'];
			$cad_matricula = $_POST['cad_matricula'];
			$cad_proprietario = strtoupper($_POST['cad_proprietario']);
			$cid_status = $_POST['cid_status'];
			$cad_obs = $_POST['cad_obs'];
			$cad_matricula_prop = $_POST['cad_matricula_prop'];
			$responsavel = $_SESSION['autenticador'];

			// Verifica se nao tem um numero de serie igual ja cadastrado
			$sqlV = $pdo->prepare("SELECT id_pdv FROM cadastro_pdv WHERE marca = ? AND modelo = ? AND serie = ?");
			$sqlV->bindValue(1, $cad_marca);
			$sqlV->bindValue(2, $cad_modelo);
			$sqlV->bindValue(3, $cad_sequencia);
			$sqlV->execute();
			// registros encontrados
			$totalV = $sqlV->rowCount();

			if ($totalV > 0){
				echo "ja_cadastrado";
			}else{
				// $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				// Cadastra o bairro
				$sql = $pdo->prepare("	INSERT INTO cadastro_pdv (marca, modelo, serie, matricula_cooperado, proprietario_pdv, matricula_proprietario, status_pdv, obs_pdv, responsavel_operacao) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
				$sql->bindValue(1, $cad_marca);
				$sql->bindValue(2, $cad_modelo);
				$sql->bindValue(3, $cad_sequencia);
				$sql->bindValue(4, $cad_matricula);
				$sql->bindValue(5, $cad_proprietario);
				$sql->bindValue(6, $cad_matricula_prop);
				$sql->bindValue(7, $cid_status);
				$sql->bindValue(8, $cad_obs);
				$sql->bindValue(9, $responsavel);
				$sql->execute();
				//retorna id do PDV
				$id = $pdo->lastInsertId();
				// exit();

				//insere os dados no historico
				
			$sql = $pdo->prepare("INSERT INTO historico_busca_pdv ( id_pdv, operador, proprietario, status)VALUES (?,?,?,?)");

			$sql->bindValue(1, $id);
			$sql->bindValue(2, $cad_matricula);
			$sql->bindValue(3, $cad_matricula_prop);
			$sql->bindValue(4, $cid_status);
			$sql->execute();


				echo "ok";
			}
		

	}else{
		echo "error";
		header("http://$server/");
	}

	
?>
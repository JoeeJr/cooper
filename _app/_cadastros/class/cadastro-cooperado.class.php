<?php 
	// Caminho da chave de conexao ao banco
	include('../../controler.php');

	// Se recebeu os POSTS
	if ($_POST){
		$cad_nome = strtoupper($_POST['cad_nome']);
		$cad_rg = strtoupper($_POST['cad_rg']);
		$cad_emissor = strtoupper($_POST['cad_emissor']);
		$cad_cpf = strtoupper($_POST['cad_cpf']);
		$cad_drt = strtoupper($_POST['cad_drt']);
		$cad_cnh = strtoupper($_POST['cad_cnh']);
		$cad_titulo_eleitor = strtoupper($_POST['cad_titulo_eleitor']);

		// Formatar data para o banco 
		$data_nasc = $_POST['cad_data_nasc']; 
		$dateTime = explode("/", $data_nasc);
		$cad_data_nasc = $dateTime[2].'-'.$dateTime[1].'-'.$dateTime[0];

		$cad_estado_civil = strtoupper($_POST['cad_estado_civil']);
		$cad_profissao = strtoupper($_POST['cad_profissao']);
		$cad_naturalidade = strtoupper($_POST['cad_naturalidade']);
		$cad_nacionalidade = strtoupper($_POST['cad_nacionalidade']);
		$cad_fixo = strtoupper($_POST['cad_fixo']);
		$cad_cel = strtoupper($_POST['cad_cel']);
		$cad_recado = strtoupper($_POST['cad_recado']);
		$cad_email = strtoupper($_POST['cad_email']);
		$cad_pis = strtoupper($_POST['cad_pis']);
		$cad_reservista = strtoupper($_POST['cad_reservista']);
		$cad_ctps = strtoupper($_POST['cad_ctps']);
		$cad_serie_ctps = strtoupper($_POST['cad_serie_ctps']);
		$cad_ctpd = strtoupper($_POST['cad_ctpd']);
		$cad_cep = strtoupper($_POST['cad_cep']);
		$cad_endereco = strtoupper($_POST['cad_endereco']);
		$cad_numero = strtoupper($_POST['cad_numero']);
		$cad_complemento = strtoupper($_POST['cad_complemento']);
		$cad_bairro = strtoupper($_POST['cad_bairro']);
		$cad_cidade = strtoupper($_POST['cad_cidade']);
		$cad_regiao = strtoupper($_POST['cad_regiao']);
		$cad_bairro_com = strtoupper($_POST['cad_bairro_com']);
		$cad_end_com = strtoupper($_POST['cad_end_com']);
		$cad_numero_com = strtoupper($_POST['cad_numero_com']);
		$color = strtoupper($_POST['color']);

		$meta = $_POST['valor_meta'];
		$preposto_preponente = $_POST['tipo_coop'];
		$preponente = $_POST['cad_preponente'];
		$cad_preposto = $_POST['cad_preposto'];

		// Valores de moeda
		$cad_taxa_adm = str_replace(".", "", $_POST['cad_taxa_adm']);
		$cad_taxa_aluguel = str_replace(".", "", $_POST['cad_taxa_aluguel']);
		$valor_cad = str_replace(".", "", $_POST['valor_cad']);
		$taxa_aluguel_cinq = str_replace(".", "", $_POST['taxa_aluguel_50']);

		$cad_taxa_adm = str_replace(",", ".", $cad_taxa_adm);
		$cad_taxa_aluguel = str_replace(",", ".", $cad_taxa_aluguel);
		$valor_cad = str_replace(",", ".", $valor_cad);
		$taxa_aluguel_cinq = str_replace(",", ".", $taxa_aluguel_cinq);

		// Foto
		$change = "";
		$caminho = "";
		if (!empty($_FILES['foto_cooperado']['name'])){

			// Nome do arquivo
			$name = $_FILES['foto_cooperado']['name'];

			// Extensao
			$path = pathinfo($name, PATHINFO_EXTENSION);

			// Troca o nome da imagem
			$change = md5(uniqid($name)).'.'.$path;

			// Caminho de destino da imagem BD
			$caminho = "_app/_cadastros/img/cooperados/".$change;

			// Diretorio
			$dir_img = dirname(dirname(__FILE__));
			$local_img = str_replace('\\', '/', $dir_img);
			$local_img .= '/img/cooperados/'.$change;

			// Temporario
			$tmp = $_FILES['foto_cooperado']['tmp_name'];

			// Move a imagem
			move_uploaded_file($tmp, $local_img);
		}

		// Busca o CPF na base para verificar se há cadastro
		$sqlCPF = $pdo->prepare("SELECT cpf FROM cooperados WHERE cpf = '$cad_cpf'");
		$sqlCPF->execute();

		// Total de registros
		$total = $sqlCPF->rowCount();


		// Busca se já tem um preponente igual na coluna
		if ($preponente != ''){
			$Bpreponente = $pdo->prepare("SELECT id FROM cooperados WHERE preponente = '$preponente'");
			$Bpreponente->execute();

			// Total de preponentes encontrados
			$totalPreponente = $Bpreponente->rowCount();
		}else{
			$totalPreponente = 0;
		}

		// Busca se já tem um preposto igual na coluna
		if ($cad_preposto != ''){
			$Bpreposto = $pdo->prepare("SELECT id FROM cooperados WHERE preposto = '$cad_preposto'");
			$Bpreposto->execute();

			// Total de preponentes encontrados
			$totalPreposto = $Bpreposto->rowCount();
		}else{
			$totalPreposto = 0;
		}



		// Se nao encontrou registros, insere normalmente
		if ($total > 0){
			echo "error_cpf";
		}else if ($totalPreponente > 0){
			echo "error_preponente";
		}else if ($totalPreposto > 0){
			echo "error_preposto";
		}else{
			// INSERT
			$sql = $pdo->prepare("INSERT INTO cooperados (	foto,
															nome, 
															rg, 
															emissor, 
															cpf, 
															drt, 
															cnh, 
															titulo_eleitor, 
															nascimento, 
															estado_civil, 
															profissao, 
															naturalidade, 
															nacionalidade,
															tel_fixo, 
															tel_cel, 
															tel_recado, 
															email, 
															pis, 
															reservista, 
															ctps,
															serie_ctps,
															ctpd,															
															cep, 
															endereco, 
															numero, 
															compl, 
															bairro, 
															cidade, 
															regiao_com, 
															bairro_com, 
															endereco_com, 
															numero_com, 
															tipo_plano, 
															taxa_administrativa, 
															taxa_aluguel, 
															valor_cad,
															meta,
															taxa_aluguel_cinq,
															preposto_preponente,
															preponente,
															preposto)
															VALUES (?,
																	?,
																	?,
																	?,
																	?,
																	?,
																	?,
																	?,
																	?,
																	?,
																	?,
																	?,
																	?,
																	?,
																	?,
																	?,
																	?,
																	?,
																	?,
																	?,
																	?,
																	?,
																	?,
																	?,
																	?,
																	?,
																	?,
																	?,
																	?,
																	?,
																	?,
																	?,
																	?,
																	?,
																	?,
																	?,
																	?,
																	?,
																	?,
																	?,
																	?)");

									$sql->bindValue(1, $caminho);
									$sql->bindValue(2, $cad_nome);
									$sql->bindValue(3, $cad_rg);
									$sql->bindValue(4, $cad_emissor);
									$sql->bindValue(5, $cad_cpf);
									$sql->bindValue(6, $cad_drt);
									$sql->bindValue(7, $cad_cnh);
									$sql->bindValue(8, $cad_titulo_eleitor);
									$sql->bindValue(9, $cad_data_nasc);
									$sql->bindValue(10, $cad_estado_civil);
									$sql->bindValue(11, $cad_profissao);
									$sql->bindValue(12, $cad_naturalidade);
									$sql->bindValue(13, $cad_nacionalidade);
									$sql->bindValue(14, $cad_fixo);
									$sql->bindValue(15, $cad_cel);
									$sql->bindValue(16, $cad_recado);
									$sql->bindValue(17, $cad_email);
									$sql->bindValue(18, $cad_pis);
									$sql->bindValue(19, $cad_reservista);
									$sql->bindValue(20, $cad_ctps);
									$sql->bindValue(21, $cad_serie_ctps);
									$sql->bindValue(22, $cad_ctpd);
									$sql->bindValue(23, $cad_cep);
									$sql->bindValue(24, $cad_endereco);
									$sql->bindValue(25, $cad_numero);
									$sql->bindValue(26, $cad_complemento);
									$sql->bindValue(27, $cad_bairro);
									$sql->bindValue(28, $cad_cidade);
									$sql->bindValue(29, $cad_regiao);
									$sql->bindValue(30, $cad_bairro_com);
									$sql->bindValue(31, $cad_end_com);
									$sql->bindValue(32, $cad_numero_com);
									$sql->bindValue(33, $color);
									$sql->bindValue(34, $cad_taxa_adm);
									$sql->bindValue(35, $cad_taxa_aluguel);
									$sql->bindValue(36, $valor_cad);
									$sql->bindValue(37, $meta);
									$sql->bindValue(38, $taxa_aluguel_cinq);
									$sql->bindValue(39, $preposto_preponente);
									$sql->bindValue(40, $preponente);
									$sql->bindValue(41, $cad_preposto);
									$sql->execute();

									echo "ok";
		}

	}else{
		echo "error";
		header("http://$server/");
	}

	
?>
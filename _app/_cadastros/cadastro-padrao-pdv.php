<?php /*autenticador*/ include('../../admin/autenticador.php'); ?>
<?php /*controlador*/ include('../../admin/controler_sys.php'); ?>
<!DOCTYPE html>
<html lang="pt-br">
   <head>
      <meta charset="utf-8">
      <meta name="robots" content="noindex, nofollow">
      <title><?php include('../../includes/title.php'); ?></title>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      
      <!-- FAV ICON -->
      <link rel="icon" type="image/png" href="http://<?= $server ?>/img/fav.png" />

      <!-- CSS -->
      <link rel="stylesheet" href="http://<?= $server ?>/css/bootstrap3.3.0.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/estilo.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/fontawesome.css">
      <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.1/css/responsive.dataTables.min.css">
      <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
      

      <!-- JAVASCRIPTS -->
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/bootstrap330.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.mask.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/sweet-alert.js"></script>
      <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js" ></script>
      <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js" ></script>
   </head>
   <style type="text/css">
      #sequencia{
        text-transform: uppercase;
      }

      label {
        width: auto;
        height: 34px;
        border-radius: 50px;
      }

      #form-edit-padrao{
        display: none;
      }

      .war{
        background-color: #f0ad4e;
        border-color: #eea236;
      }

      .war:hover{
        background-color: #ff9703;
        border-color: #ffc276;
      }
   </style>
   <body>
      <div id="throbber" style="display:none; min-height:120px;"></div>
      <div id="noty-holder"></div>
      <div id="wrapper">
         <!-- Navigation -->
        <?php include('../../includes/menu.php') ?>
         <div id="page-wrapper">
            <div class="container-fluid">
               <!-- Page Heading -->
               <div class="row" id="main">               
                  <div class="col-md-12 well">                    
                    <div class="col-md-12">
                       <h3 class="rlk">PADRÃO DE PDV</h3>
                    </div>
                    <div class="col-md-12">
                      <form id="form_padrao_pdv">
                       <div class="col-md-3">
                         <span>Marca</span><b style="color:red">*</b>           

                         <select onChange="NovaMarca(this.value)" class="form-control" name="cad_marca" id="marca">
                          <option value="">Selecione...</option>
                          <option class='pers_opt' value="nova-marca">Nova Marca +</option>
                           <?php
                              // Lista as Regiões cadastradas
                              $marca = $class->Select("id, marca", "marca_pdv", "", "ORDER BY marca ASC");
                              while($rowM = $marca->fetch(PDO::FETCH_OBJ)){
                                 echo "<option value='".$rowM->marca."'>".$rowM->marca."</option>";
                              }
                           ?>
                         </select>


                         <!-- Nova Marca -->
                         <span>&nbsp;</span>
                         <div class="appendPadrao"></div>
                         <br>
                       </div>


                       <div class="col-md-3">
                         <span>Modelo</span><b style="color:red">*</b>
                         <input class="form-control" name="cad_modelo" id="modelo">
                         <br>
                       </div>

                       <div class="col-md-4">
                         <span>Sequência</span><b style="color:red">*</b>
                         <input name="sequencia" id="sequencia" placeholder="Ex: 000-000-000 ou AAA-000-AAA" class="form-control" type="text">
                         <br>
                       </div>
                      
                       <div class="col-md-1">
                         <span>&nbsp;</span>
                         <button type="button" class="btn btn-warning cad_padrao">CADASTRAR</button>
                       </div>
                      </form> 

                      <div class="col-md-12">
                        &nbsp;
                      </div>


                      <!-- FORMULARIO DE EDIÇÃO -->
                      <form id="form-edit-padrao">
                       <input id="id_modelo" type="hidden" name="id_modelo" value="">
                       <div class="col-md-3">
                         <span>Marca</span><b style="color:red">*</b>           

                         <select class="form-control" name="edit_marca" id="edit_marca">
                          <option value="">Selecione...</option>
                           <?php
                              // Lista as Regiões cadastradas
                              $marca = $class->Select("id, marca", "marca_pdv", "", "ORDER BY marca ASC");
                              while($rowM = $marca->fetch(PDO::FETCH_OBJ)){
                                 echo "<option value='".$rowM->id."'>".$rowM->marca."</option>";
                              }
                           ?>
                         </select>


                         <!-- Nova Marca -->
                         <span>&nbsp;</span>
                         <div class="appendPadrao"></div>
                         <br>
                       </div>


                       <div class="col-md-3">
                         <span>Modelo</span><b style="color:red">*</b>
                         <input class="form-control" name="edit_modelo" id="edit_modelo">
                         <br>
                       </div>

                       <div class="col-md-4">
                         <span>Sequência</span><b style="color:red">*</b>
                         <input name="edit_sequencia" id="edit_sequencia" placeholder="Ex: 000-000-000 ou AAA-000-AAA" class="form-control" type="text">
                         <br>
                       </div>
                      
                       <div class="col-md-2">
                         <span>&nbsp;</span>
                         <button type="button" class="btn btn-warning war cad_edit_padrao">ATUALIZAR</button>
                       </div>
                      </form> 
                    </div>  
                      



                    <!-- FORMULARIO DE EDIÇÃO -->

                  </div>

                  <div class="col-md-12 well">
                      <table id="example" class="display  nowrap" style="width:100%">
                          <thead>
                              <tr>
                                  <th>#</th>
                                  <th>Marca</th>
                                  <th>Modelo</th>
                                  <th>Sequência</th>
                                  <th>Editar</th>
                                  <th>Remover</th>
                              </tr>
                          </thead>
                          <tbody>
                            <?php
                              $bairro = $class->Select("*", "modelo_pdv", "", "ORDER BY modelo ASC");
                              while($row = $bairro->fetch(PDO::FETCH_OBJ)){
                            ?>
                              <tr>
                                  <td><?= $row->id; ?></td>
                                  <td><?= $class->SelectEsp("marca", "marca_pdv", "WHERE id = '".$row->id_marca."'"); ?></td>
                                  <td><?= $row->modelo; ?></td>
                                  <td><?= $row->sequencia; ?></td>
                                  <td><div onClick="editaModelo(<?= $row->id; ?>)" class="edit"></div></td>
                                  <td><div onClick="RemoverPadrao(<?= $row->id; ?>)" class="lixeira"></div></td>
                              </tr>
                            <?php
                              }
                            ?>
                          </tbody>
                      </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
      </div>
      <script type="text/javascript" src="http://<?= $server ?>/admin/_class/caminho_controler.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/menu-mobile.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/_app/_cadastros/js/cadastro-padrao-pdv.js"></script>
      <script type="text/javascript">
        $("#sequencia").on("input", function(){
          var regexp = /[^aA0\-]/g;
          if(this.value.match(regexp)){
            $(this).val(this.value.replace(regexp,''));
          }
        });

        $(document).ready(function() {
            var table = $('#example').DataTable( {
                rowReorder: {
                    selector: 'td:nth-child(2)'
                },
                responsive: true,
                "language": {
                  "url": "https://cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
               }
            } );
        } );
      </script>
   </body>
</html>
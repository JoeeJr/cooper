<?php /*autenticador*/ include('../../admin/autenticador.php'); ?>
<?php /*controlador*/ include('../../admin/controler_sys.php'); ?>
<!DOCTYPE html>
<html lang="pt-br">
   <head>
      <meta charset="utf-8">
      <meta name="robots" content="noindex, nofollow">
      <title><?php include('../../includes/title.php'); ?></title>
      <meta name="viewport" content="width=device-width, initial-scale=1">

      <!-- FAV ICON -->
      <link rel="icon" type="image/png" href="http://<?= $server ?>/img/fav.png" />

      <!-- CSS -->
      <link rel="stylesheet" href="http://<?= $server ?>/css/bootstrap3.3.0.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/datepicker.css" />
      <link rel="stylesheet" href="http://<?= $server ?>/css/estilo.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/fontawesome.css">
      
      <!-- JAVASCRIPTS -->
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/cep.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/datapicker.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/bootstrap330.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.mask.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.maskMoney.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/sweet-alert.js"></script>
      
   </head>
   <body>
      
      <div id="throbber" style="display:none; min-height:120px;"></div>
      <div id="noty-holder"></div>
      <div id="wrapper">
         <!-- Menu Lateral -->
         <?php include('../../includes/menu.php') ?>
         <div id="page-wrapper">
            <div class="container-fluid">
               <form enctype="multipart/form-data" id="form_cooperado" method="post">
                  <!-- Page Heading -->
                  <div class="row" id="main" >
                     <div class="col-sm-12 col-md-12 well" id="content">
                        <div class="col-md-12">
                           <h3 class="rlk">Cadastro de Cooperados - 
                              <b style="color:#c42c29">
                                 <?php
                                    // Proximo ID
                                    $selID = $class->Select("MAX(id) AS next", "cooperados", "", "");
                                    $get = $selID->fetch(PDO::FETCH_OBJ);
                                    $nextID = $get->next + 1;

                                    // Exibe o proximo ID
                                    echo '#'.$nextID;
                                 ?>
                              </b>

                              <b class="data-ativacao pull-right">
                                 &nbsp; Data de ativação : <?= date('d/m/Y'); ?>
                              </b>
                           </h3>

                           
                        </div>

                        <div class="col-md-3">
                           <img class="thumbnail img-responsive photoCoop" src="http://<?= $server ?>/img/user.png">
                           <label style="height:37px; width:100%" type="button" for="foto_cooperado" class="btn btn-primary">
                              Arquivo &nbsp;
                              <i class="fas fa-cloud-upload-alt"></i>
                           </label>
                           <input name="foto_cooperado" style="display:none" id="foto_cooperado" type="file">
                        </div>

                        <div class="col-md-9">
                           <div class="col-md-6">
                              <span>Nome</span><b style="color:red">*</b>
                              <input placeholder="Nome da pessoa" class="form-control cad_nome" name="cad_nome" type="text">
                              <br>
                           </div>
                           <div class="col-md-4">
                              <span>RG</span><b style="color:red">*</b>
                              <input placeholder="00.000.000-0"  class="form-control cad_rg" name="cad_rg" type="text">
                              <br>
                           </div>
                           <div class="col-md-2">
                              <span>Emissor</span><b style="color:red">*</b>
                              <input placeholder="SSP" class="form-control cad_emissor" name="cad_emissor" type="text">
                              <br>
                           </div>
                           <div class="col-md-4">
                              <span>CPF</span><b style="color:red">*</b>
                              <input id="cpf" placeholder="000.000.000-00" class="form-control cad_cpf" name="cad_cpf" type="text">
                              <br>
                           </div>
                           <div class="col-md-4">
                              <span>DRT</span>
                              <input placeholder="000001" class="form-control cad_drt" name="cad_drt" type="text">
                              <br>
                           </div>
                           <div class="col-md-4">
                              <span>CNH</span>
                              <input placeholder="00000000001" class="form-control cad_cnh" name="cad_cnh" type="text">
                              <br>
                           </div>
                           <div class="col-md-4">
                              <span>Título de eleitor</span>
                              <input placeholder="00000000001" class="form-control cad_titulo_eleitor" name="cad_titulo_eleitor" type="text">
                              <br>
                           </div>
                           <div class="col-md-4">
                              <div class="form-group">
                                 <span>Data Nasc.</span><b style="color:red">*</b>
                                 <div class="input-group input-append date" id="datePicker">
                                    <input placeholder="01/01/2018" type="text" class="form-control cad_data_nasc" name="cad_data_nasc" />
                                    <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                                 </div>
                              </div>
                           </div>
                           <div class="col-md-4">
                              <span>Estado Civil</span><b style="color:red">*</b>
                              <select class="form-control cad_estado_civil" name="cad_estado_civil">
                                 <option value="">Selecione</option>
                                 <option value="Solteiro(a)">Solteiro(a)</option>
                                 <option value="Casado(a)">Casado(a)</option>
                                 <option value="Separado(a)">Separado(a)</option>
                                 <option value="Divorciado(a)">Divorciado(a)</option>
                                 <option value="Viúvo(a)">Viúvo(a)</option>
                                 <option value="União Estável">União Estável</option>
                              </select>
                              <br>
                           </div>
                           <div class="col-md-3">
                              <span>Profissão</span><b style="color:red">*</b>
                              <input placeholder="Ex: Guardador" class="form-control cad_profissao" name="cad_profissao" type="text">
                              <br>
                           </div>
                           <div class="col-md-3">
                              <span>Naturalidade</span><b style="color:red">*</b>
                              <input placeholder="Ex: Santo André" class="form-control cad_naturalidade" name="cad_naturalidade" type="text">
                              <br>
                           </div>
                           <div class="col-md-3">
                              <span>Nacionalidade</span>
                              <input placeholder="Ex: Brasil" class="form-control cad_nacionalidade" name="cad_nacionalidade" type="text">
                              <br>
                           </div>
                           <div class="col-md-3">
                              <span>Tel. Fixo</span>
                              <input placeholder="Ex: (11) 2222-2222" class="form-control fixo cad_fixo" name="cad_fixo" type="text">
                              <br>
                           </div>
                           <div class="col-md-3">
                              <span>Tel.  Whatsapp</span><b style="color:red">*</b>
                              <input placeholder="Ex: (11) 92222-2222" class="form-control celular cad_cel" name="cad_cel" type="text">
                              <br>
                           </div>
                           <div class="col-md-3">
                              <span>Tel.  Recado</span>
                              <input placeholder="Ex: (11) 92222-2222" class="form-control fixo cad_recado" name="cad_recado" type="text">
                              <br>
                           </div>
                           <div class="col-md-6">
                              <span>Email</span><b style="color:red">*</b>
                              <input placeholder="Ex: contato@email.com.br" class="form-control cad_email" name="cad_email" type="text">
                              <br>
                           </div>
                           <div class="col-md-3">
                              <span>PIS</span>
                              <input placeholder="Ex: 000.00000.00-00" class="form-control cad_pis" name="cad_pis" type="text">
                              <br>
                           </div>
                           <div class="col-md-3">
                              <span>Reservista</span>
                              <input  placeholder="1234567" class="form-control cad_reservista" name="cad_reservista" type="text">
                              <br>
                           </div>
                           <div class="col-md-3">
                              <span>Numero de CTPS</span>
                              <input placeholder="1234567" class="form-control cad_ctps" name="cad_ctps" type="text">
                              <br>
                           </div>
                           <div class="col-md-3">
                              <span>Série CTPS</span>
                              <input placeholder="07066" class="form-control cad_serie_ctps" name="cad_serie_ctps" type="text">
                           </div>
                           <div class="col-md-4">
                              <span>CTPD</span>
                              <input placeholder="1234567" class="form-control cad_ctpd" name="cad_ctpd" type="text">
                              <br>
                           </div>
                        </div>
                     </div>


                     <div class="col-md-12 well">
                        <div class="col-md-12">
                           <h3 class="rlk">Endereço do Cooperado</h3>
                        </div>
                        <div class="col-md-2">
                           <span>CEP</span><b style="color:red">*</b>
                           <input placeholder="00000-030" class="form-control cep cad_cep" name="cad_cep" type="text">
                           <br>
                        </div>
                        <div class="col-md-6">
                           <span>Endereço</span><b style="color:red">*</b>
                           <input placeholder="Ex: Rua Nome da Rua" class="form-control cad_endereco" name="cad_endereco" type="text">
                           <br>
                        </div>
                        <div class="col-md-2">
                           <span>Nº</span><b> *</b>
                           <input placeholder=" 4" class="form-control cad_numero" name="cad_numero" type="text">
                           <br>
                        </div>
                        <div class="col-md-2">
                           <span>Complemento</span>
                           <input placeholder="Ex: Casa A" class="form-control cad_complemento" name="cad_complemento" type="text">
                           <br>
                        </div>
                        <div class="col-md-4">
                           <span>Bairro</span><b style="color:red">*</b>
                           <input placeholder="Ex: Santo André" class="form-control cad_bairro" name="cad_bairro" type="text">
                           <br>
                        </div>
                        <div class="col-md-4">
                           <span>Cidade</span><b style="color:red">*</b>
                           <input placeholder="Ex: São Paulo" class="form-control cad_cidade" name="cad_cidade" type="text">
                           <br>
                        </div>
                     </div>

                     <div class="col-md-12 well">
                        <div class="col-md-12">
                           <h3 class="rlk">Endereço Comercial</h3>
                        </div>
                        <div class="col-md-2">
                           <span>Região</span><b style="color:red">*</b>
                           <select class="form-control cad_regiao" name="cad_regiao"  id="cad_regiao">
                              <option value="">Selecione...</option>
                              <?php
                                 // Lista as Regiões cadastradas
                                 $regioes = $class->Select("id, regiao", "regioes", "", "ORDER BY regiao ASC");
                                 while($reg = $regioes->fetch(PDO::FETCH_OBJ)){
                                    echo "<option value='".$reg->id."'>".$reg->regiao."</option>";
                                 }
                              ?>
                           </select>
                           <br>
                        </div>
                        <div class="col-md-3">
                           <span>Bairro Comercial</span><b style="color:red">*</b>
                           <select class="form-control cad_bairro_com" name="cad_bairro_com"  id="cad_bairro_com">
                              <option value="">Selecione...</option>
                           </select>
                           <br>
                        </div>
                        <div class="col-md-4">
                           <span>End. Comercial</span><b style="color:red">*</b>
                           <input placeholder="Ex: Rua Nome da Rua" class="form-control cad_end_com" name="cad_end_com" type="text">
                           <br>
                        </div>
                        <div class="col-md-2">
                           <span>Nº</span><b style="color:red">*</b>
                           <input placeholder="4" class="form-control cad_numero_com" name="cad_numero_com" type="text">
                           <br>
                        </div>                        
                     </div>
                        <?php 


                        ?>

                     <div style="    padding-bottom: 5rem;" class="col-md-12 well">
                        <div class="col-md-12">
                           <h3 class="rlk">Planos<b style="color:red"> *</b></h3>
                        </div>
                        <div class="col-md-3">
                           <input onClick="TipoPlano(this.value)" type="radio" name="color" value="pre" id="blue"/>
                           <span style="font-size: 2rem;color: #1c62b5;"> &nbsp; Pré-Pago &nbsp;</span>
                           <label style="margin-bottom: -11px;" for="blue" class="blue"></label>
                           <br><br>
                           <span class="taxa_adm">Taxa administrativa<b style="color:red">*</b></span>
                           <input maxlength="7" data-affixes-stay="true" data-thousands="." data-decimal="," placeholder="R$: 1,00" class="form-control taxa_adm cad_taxa_adm" name="cad_taxa_adm" value="" type="text">
                        </div>


                        <div class="col-md-3">
                           <input onClick="TipoPlano(this.value)" type="radio" name="color" value="pos" id="cyan"/>
                           <span style="font-size: 2rem;color: #af222c;"> &nbsp; Pós-Pago &nbsp;</span>
                           <label style="margin-bottom: -11px;" for="cyan" class="cyan"></label>
                           <br><br>
                           <span class="taxa_aluguel">Taxa de aluguel<b style="color:red">*</b></span>
                           <input maxlength="7" data-affixes-stay="true" data-thousands="." data-decimal="," placeholder="R$: 1,00" class="form-control taxa_aluguel cad_taxa_aluguel" name="cad_taxa_aluguel" type="text">
                        </div>
                     </div>

                     <div class="col-md-12 well">
                        <div class="col-md-12">
                           <h3 class="rlk">Valores e Meta</h3>
                        </div>
                        <div class="col-md-3">
                           <span>Valor do CAD</span><b style="color:red">*</b>
                           <input maxlength="7" data-affixes-stay="true" data-thousands="." data-decimal="," placeholder="Ex: R$ 30,00" class="form-control valor_cad" name="valor_cad" type="text">
                           <br>
                        </div>

                        <div class="col-md-3">
                           <span>Meta</span><b style="color:red">*</b>
                           <input placeholder="250" class="form-control valor_meta" name="valor_meta" type="text">
                           <br>
                        </div>

                        <div class="col-md-3">
                           <span>Taxa de Aluguel 50%</span><b style="color:red">*</b>
                           <input maxlength="7" data-affixes-stay="true" data-thousands="." data-decimal="," placeholder="250" class="form-control taxa_aluguel_50" name="taxa_aluguel_50" type="text">
                           <br>
                        </div>

                        <div class="col-md-12">
                           &nbsp;
                        </div>
                     </div>

                     <div class="col-md-12 well">
                        <div class="col-md-12">
                           <h3 class="rlk">Tipo de Cooperado</h3>
                        </div>
                        <div class="col-md-4">
                           <input onClick="TipoCooperado(this.value)" type="radio" name="tipo_coop" class="tipo_coop" value="preponente" id="coop_preponente"/>
                           <span style="font-size: 2rem;color: #1c62b5;"> &nbsp; Preponente &nbsp;</span>
                           <label style="margin-bottom: -11px;" for="coop_preponente" class="coop_preponente"></label>
                           <br><br>

                           <span class="txt_preponente sec_preposto">Nome do Preposto <b style="color:red">*</b></span>
                           <input placeholder="Ex : João Ferreira" class="form-control txt_preponente cad_preposto sec_preposto" type="text" onkeyup="autocomplet()" onkeydown="verificacampo()" id="country_id">
                           <input class="cadastro_preposto sec_preposto" type="hidden" name="cad_preponente">

                           <!-- Chip View -->
                           <span id="chipview-preposto" class="chipview">
                              <span id="txt_name_prepo" class="txt_name_prepo"></span>
                              <button onClick="closeChip(this.value)" value="" type="button" id="close-chip" class="close-chip">
                                 <b class="fa fa-close"></b>
                              </button>
                           </span>

                           <!-- Auto Complete -->
                           <div style="display:none" id="country_list_id" class="history_list">
                              <div class="history_item_name">         
                                 <li class="listagem">
                                 </li>
                              </div>
                            </div>
                        </div>


                        <div class="col-md-4">
                           <input onClick="TipoCooperado(this.value)" type="radio" name="tipo_coop" class="tipo_coop" value="preposto" id="coop_preposto"/>
                           <span style="font-size: 2rem;color: #af222c;"> &nbsp; Preposto &nbsp;</span>
                           <label style="margin-bottom: -11px;" for="coop_preposto" class="coop_preposto"></label>
                           <br><br>
                           <span class="txt_preposto sec_preponente">Nome do Preponente<b style="color:red">*</b></span>
                           <input placeholder="Ex : João Ferreira" class="form-control txt_preposto cad_preponente sec_preponente" type="text" onkeyup="BuscaPreponente()" onkeydown="verificaPreponente()" id="busca_preponente">
                           <input class="cadastro_preponente sec_preponente" type="hidden" name="cad_preposto">

                           <!-- Chip View -->
                           <span id="chipview-preponente" class="chipview">
                              <span id="txt_name_prepo2" class="txt_name_prepo"></span>
                              <button onClick="closeChip2(this.value)" value="" type="button" id="close-chip2" class="close-chip">
                                 <b class="fa fa-close"></b>
                              </button>
                           </span>

                           <!-- Auto Complete -->
                           <div style="display:none" id="lista_preponente" class="history_list">
                              <div class="history_item_name">         
                                 <li class="listagem">
                                 </li>
                              </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                           <input checked onClick="TipoCooperado(this.value)" type="radio" name="tipo_coop" class="tipo_coop" value="nenhum" id="coop_nenhum"/>
                           <span style="font-size: 2rem;color: #9e9631;"> &nbsp; Nenhum &nbsp;</span>
                           <label style="margin-bottom: -11px;" for="coop_nenhum" class="coop_nenhum"></label>
                        </div>

                       

                        <div class="col-md-12">
                           <hr>
                        </div>


                        <div class="col-md-12">
                           &nbsp;
                        </div>

                        <div class="col-md-2 pull-right">
                           <button type="button" id="cadastrar-cooperado" class="btn btn-primary">
                              Cadastrar
                           </button>
                        </div>
                     </div>                     
                  </div>
               </form>
            </div>
         </div>
      </div>
      </div>
      <script type="text/javascript" src="http://<?= $server ?>/admin/_class/caminho_controler.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/_app/_cadastros/js/cadastro-cooperado.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/_app/_cadastros/js/busca-preposto.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/_app/_cadastros/js/busca-preponente.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/menu-mobile.js"></script>
   </body>
</html>


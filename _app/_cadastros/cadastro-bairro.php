<?php /*autenticador*/ include('../../admin/autenticador.php'); ?>
<?php /*controlador*/ include('../../admin/controler_sys.php'); ?>
<!DOCTYPE html>
<html lang="pt-br">
   <head>
      <meta charset="utf-8">
      <meta name="robots" content="noindex, nofollow">
      <title><?php include('../../includes/title.php'); ?></title>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      
      <!-- FAV ICON -->
      <link rel="icon" type="image/png" href="http://<?= $server ?>/img/fav.png" />

      <!-- CSS -->
      <link rel="stylesheet" href="http://<?= $server ?>/css/bootstrap3.3.0.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/estilo.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/fontawesome.css">
      <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.1/css/responsive.dataTables.min.css">
      <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
      

      <!-- JAVASCRIPTS -->
      <script type="text/javascript" src="http://<?= $server ?>/js/bootstrap330.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.mask.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/sweet-alert.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.js"></script>
      <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js" ></script>
      <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js" ></script>

   </head>

    <style>
     label {
      width: auto;
      height: 34px;
      border-radius: 50px;
    }

    #form-edit-bairro{
      display: none;
    }

    .war{
      background-color: #f0ad4e;
      border-color: #eea236;
    }

    .war:hover{
      background-color: #ff9703;
      border-color: #ffc276;
    }
   </style>

   <body>
      <div id="throbber" style="display:none; min-height:120px;"></div>
      <div id="noty-holder"></div>
      <div id="wrapper">
         <!-- Menu Lateral -->
         <?php include('../../includes/menu.php') ?>
         <div id="page-wrapper">
            <div class="container-fluid">
               <!-- Page Heading -->
               <div class="row" id="main" >
                  <div class="col-md-12 well">
                     <div class="col-md-12">
                        <h3 class="rlk">Endereço Comercial</h3>
                     </div>
                     <div class="col-md-12">
                      <form id="form-bairro" method="post">
                        <div class="col-md-3">
                          <span>Região</span><b style="color:red">*</b>
                          <select onChange="NovaRegiao(this.value)" class="form-control cad_regiao" name="cad_regiao" id="nova-regiao">
                             <option value="">Selecione...</option>
                             <option class='pers_opt' value="nova-regiao">Nova Região +</option>
                              <?php
                                // Lista as Regiões cadastradas
                                $regioes = $class->Select("id, regiao", "regioes", "", "ORDER BY regiao ASC");
                                while($reg = $regioes->fetch(PDO::FETCH_OBJ)){
                                   echo "<option value='".$reg->regiao."'>".$reg->regiao."</option>";
                                }
                              ?>
                          </select>

                          <!-- Nova Regiao -->
                          <span>&nbsp;</span>
                          <div class="appendRegiao"></div>
                          <br>
                        </div>

                        <div class="col-md-3">
                          <span>Bairro Comercial</span><b style="color:red">*</b>
                          <input type="text" class="form-control cad_bairro" name="cad_bairro">
                          <br>
                        </div>

                        <div class="col-md-2">
                           <span>&nbsp;</span><br>
                           <button id="cadastrar-bairro" type="button" class="btn btn-warning">CADASTRAR</button>
                        </div>
                      </form>



                      <!-- Edição do Bairro -->
                      <form id="form-edit-bairro" method="post">
                        <input class="id_bairro" type="hidden" name="id_bairro" value="">
                        <div class="col-md-3">
                          <span>Região</span><b style="color:red">*</b>
                          <select class="form-control edit_regiao" name="edit_regiao" id="edit-regiao">
                              <?php
                                // Lista as Regiões cadastradas
                                $regioes = $class->Select("id, regiao", "regioes", "", "ORDER BY regiao ASC");
                                while($reg = $regioes->fetch(PDO::FETCH_OBJ)){
                                   echo "<option value='".$reg->id."'>".$reg->regiao."</option>";
                                }
                              ?>
                          </select>
                        </div>

                        <div class="col-md-3">
                          <span>Bairro Comercial</span><b style="color:red">*</b>
                          <input type="text" class="form-control edit_bairro" name="edit_bairro">
                          <br>
                        </div>

                        <div class="col-md-2">
                           <span>&nbsp;</span>
                           <button id="atualizar-bairro" type="button" class="btn btn-warning war">ATUALIZAR</button>
                        </div>
                      </form>
                      


                     </div>
                  </div>

                  <div class="col-md-12 well">
                      <table id="example" class="display  nowrap" style="width:100%">
                          <thead>
                              <tr>
                                  <th>#</th>
                                  <th>Região</th>
                                  <th>Bairro</th>
                                  <th>Editar</th>
                                  <th>Remover</th>
                              </tr>
                          </thead>
                          <tbody>
                            <?php
                              $bairro = $class->Select("*", "bairros", "", "ORDER BY bairro ASC");
                              while($row = $bairro->fetch(PDO::FETCH_OBJ)){
                            ?>
                              <tr>
                                  <td><?= $row->id; ?></td>
                                  <td><?= $class->SelectEsp("regiao", "regioes", "WHERE id = '".$row->id_regiao."'"); ?></td>
                                  <td><?= $row->bairro; ?></td>
                                  <td><div onClick="editaBairro(<?= $row->id; ?>)" class="edit"></div></td>
                                  <td><div onClick="RemoverBairro(<?= $row->id; ?>)" class="lixeira"></div></td>
                              </tr>
                            <?php
                              }
                            ?>
                          </tbody>
                      </table>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <script type="text/javascript" src="http://<?= $server ?>/admin/_class/caminho_controler.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/_app/_cadastros/js/cadastro-bairro.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/menu-mobile.js"></script>

      <script type="text/javascript">
        $(document).ready(function() {
            var table = $('#example').DataTable( {
                rowReorder: {
                    selector: 'td:nth-child(2)'
                },
                responsive: true,
                "language": {
                  "url": "https://cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
               }
            } );
        } );
      </script>
   </body>
</html>
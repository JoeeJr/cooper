<input type="hidden" name="cad_matricula_prop" id="cad_matricula_prop">

<div class="col-md-2 cl_prop_coop">
   <span>Matrícula</span><b style="color:red">*</b>
   <input autocomplete="off" id="matricula_prop_cooperado_temp" placeholder="000000000" class="form-control buscaMatPropCoop" onBlur="BuscaMatriculaCoop(this.value)" type="text">
   <br>
</div>
<div class="col-md-6 cl_prop_coop">
   <span>Cooperado</span><b style="color:red">*</b>
   <input autocomplete="off" onkeyup="BuscaPropCoop()" onkeydown="verificaPropCoop()" id="busca_prop_cooperado" placeholder="Ex: João da Silva" class="form-control" type="text">
</div>

<div class="col-md-6">
	<!-- Auto Complete -->
    <div style="display:none" id="lista_prop_coop" class="history_list">
	    <div class="history_item_name">     
	       <li class="listagem_prop_coop">    
	       </li>
	    </div>
    </div>

    <!-- Chip View -->
    <span id="chipview-prop-cooperado" class="chipview">
       <span id="txt_name_prop_coop" class="txt_name_prepo"></span>
       <button onClick="closeChipPropCoop(this.value)" value="" type="button" id="close-chip-prop-coop" class="close-chip">
          <b class="fa fa-close"></b>
       </button>
    </span>
</div>

<script type="text/javascript">
	// Mascara para aceitar apenas numeros na matricula
	$('#matricula_prop_cooperado_temp').mask('00000000');	
</script>
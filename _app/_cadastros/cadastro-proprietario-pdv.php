<?php /*autenticador*/ include('../../admin/autenticador.php'); ?>
<?php /*controlador*/ include('../../admin/controler_sys.php'); ?>
<!DOCTYPE html>
<html lang="pt-br">
   <head>
      <meta charset="utf-8">
      <meta name="robots" content="noindex, nofollow">
      <title><?php include('../../includes/title.php'); ?></title>
      <meta name="viewport" content="width=device-width, initial-scale=1">

      <!-- FAV ICON -->
      <link rel="icon" type="image/png" href="http://<?= $server ?>/img/fav.png" />

      <!-- CSS -->
      <link rel="stylesheet" href="http://<?= $server ?>/css/bootstrap3.3.0.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/estilo.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/fontawesome.css">
      <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.1/css/responsive.dataTables.min.css">
      <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">

      <!-- JAVASCRIPTS -->
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/bootstrap330.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.mask.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/sweet-alert.js"></script>
      <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js" ></script>
      <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js" ></script>
   </head>
   <style type="text/css">
    label {
      width: auto;
      height: 34px;
      border-radius: 50px;
    }

    .war{
      background-color: #f0ad4e;
      border-color: #eea236;
    }

    .war:hover{
      background-color: #ff9703;
      border-color: #ffc276;
    }

    #form-edit-proprietarios{
      display: none;
    }
   </style>
   <body>
      <div id="throbber" style="display:none; min-height:120px;"></div>
      <div id="noty-holder"></div>
      <div id="wrapper">
         <!-- Navigation -->
         <?php include('../../includes/menu.php') ?>
         <div id="page-wrapper">
            <div class="container-fluid">
               <!-- Page Heading -->
               <div class="row" id="main" >
                  <div class="col-md-12 well">
                     <div class="col-md-12">
                        <h3 class="rlk">Proprietários de PDV</h3>
                     </div>
                     <div class="col-md-12">
                      <form id="proprietario_pdv">
                        <div class="col-md-4">
                            <span>Nome</span><b style="color:red">*</b>
                            <input id="prop_nome" name="prop_nome" placeholder="Ex: João da Silva" class="form-control" type="text">
                            <br>
                        </div>
                        <div class="col-md-3">
                            <span>RG</span><b style="color:red">*</b>
                            <input id="prop_rg" name="prop_rg" placeholder="000-X" class="form-control" type="text">
                            <br>
                        </div>
                        <div class="col-md-3">
                            <span>CPF / CNPJ</span><b style="color:red">*</b>
                            <input id="prop_cpf" name="prop_cpf" placeholder="000.000.000-00" class="form-control" type="text">
                            <br>
                        </div>
                        <div class="col-md-1">
                           <span>&nbsp;</span>
                           <button type="button" class="btn btn-warning cad_prop_pdv">CADASTRAR</button>
                        </div>
                      </form>


                      <!-- Edição do Bairro -->
                      <form id="form-edit-proprietarios" method="post">
                        <input class="edit_prop" type="hidden" name="edit_prop" value="">
                        <div class="col-md-4">
                            <span>Nome</span><b style="color:red">*</b>
                            <input id="prop_edit_nome" name="prop_edit_nome" placeholder="Ex: João da Silva" class="form-control" type="text">
                            <br>
                        </div>
                        <div class="col-md-3">
                            <span>RG</span><b style="color:red">*</b>
                            <input id="prop_edit_rg" name="prop_edit_rg" placeholder="000-X" class="form-control" type="text">
                            <br>
                        </div>
                        <div class="col-md-3">
                            <span>CPF / CNPJ</span><b style="color:red">*</b>
                            <input id="prop_edit_cpf" name="prop_edit_cpf" placeholder="000.000.000-00" class="form-control" type="text">
                            <br>
                        </div>
                        <div class="col-md-2">
                           <span>&nbsp;</span>
                           <button type="button" class="btn btn-warning edit_prop_pdv war">ATUALIZAR</button>
                        </div>
                      </form>
                     </div>                     
                  </div>
                  <div class="col-md-12 well">
                      <table id="example" class="display  nowrap" style="width:100%">
                          <thead>
                              <tr>
                                  <th>#</th>
                                  <th>Nome</th>
                                  <th>RG</th>
                                  <th>CPF / CNPJ</th>
                                  <th>Editar</th>
                                  <th>Remover</th>
                              </tr>
                          </thead>
                          <tbody>
                            <?php
                              $bairro = $class->Select("*", "proprietarios_pdv", "", "ORDER BY nome ASC");
                              while($row = $bairro->fetch(PDO::FETCH_OBJ)){
                            ?>
                              <tr>
                                  <td><?= $row->id_prop; ?></td>
                                  <td><?= $row->nome; ?></td>
                                  <td><?= $row->rg; ?></td>
                                  <td><?= $row->cpf_cnpj; ?></td>
                                  <td><div onClick="editaProprietario(<?= $row->id_prop; ?>)" class="edit"></div></td>
                                  <td><div onClick="RemoverProprietario(<?= $row->id_prop; ?>)" class="lixeira"></div></td>
                              </tr>
                            <?php
                              }
                            ?>
                          </tbody>
                      </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
      </div>
      <script type="text/javascript" src="http://<?= $server ?>/admin/_class/caminho_controler.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/menu-mobile.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/_app/_cadastros/js/cadastro-proprietario-pdv.js"></script>
      <script type="text/javascript">
        $(document).ready(function() {
            var table = $('#example').DataTable( {
                rowReorder: {
                    selector: 'td:nth-child(2)'
                },
                responsive: true,
                "language": {
                  "url": "https://cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
               }
            } );
        } );
      </script>
   </body>
</html>
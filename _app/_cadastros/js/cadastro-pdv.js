var host = location.host;
var server = "";
if (host == 'localhost'){
    server = host+'/'+pasta;
}else{
    server = host;
}

//  Seta a sequencia como mascara
$('#busca_matricula').mask('000000000');

// Busca os bairros relacionados a região selecionada
$('#cad_marca').change(function() {

	var marca = $('#cad_marca option:selected').val();

	if (marca != ''){

		swal("Aguarde...!", {
		  closeOnClickOutside: false,
		  buttons: false
		});

		jQuery.ajax({
		  url: 'http://'+server+'/_app/class_api/busca-modelo-pdv.php',
		  type: 'POST',
		  dataType: 'json',
		  data: {marca: marca},
		  success: function(data) {
		  	if(data[0] != ''){
			  	$('#cad_modelo').attr("disabled", false);
			  	$('#cad_modelo').html(data[0]);
			  	$('#sequencia').attr("disabled", true);
			  	swal.close();
			  }else{
			  	$('#sequencia').attr("disabled", true);
			  	$('#cad_modelo').attr("disabled", true);
			  	$('#cad_modelo').val('');
			  	$('#sequencia_bd').html('');
			  	swal({
			  	  icon: "error",
				  title: "Não há modelos cadastrados nesta marca!"
				});
			  }
		  },
		});	
	}else{
		$('#sequencia').attr("disabled", true);
		$('#sequencia').val('');
		$('#sequencia_bd').html('');

		$('#cad_modelo').attr("disabled", true);
		$('#cad_modelo').val('');
	}
});


// Busca os bairros relacionados a região selecionada
$('#cad_modelo').change(function() {

	var modelo = $('#cad_modelo option:selected').val();

	if (modelo != ''){

		swal("Aguarde...!", {
		  closeOnClickOutside: false,
		  buttons: false
		});

		jQuery.ajax({
		  url: 'http://'+server+'/_app/class_api/busca-sequencia.class.php',
		  type: 'POST',
		  dataType: 'json',
		  data: {modelo: modelo},
		  success: function(data) {
		  	if(data[0] != ''){
			  	$('#sequencia').attr("disabled", false);

			  	// Exibe a sequencia na paquina a ser seguida
			  	$('#sequencia_bd').html(data[0]);

			  	//  Seta a sequencia como mascara
   				$('#sequencia').mask(data[0]);
			  	swal.close();
			  }else{
			  	$('#sequencia').attr("disabled", true);
			  	$('#sequencia').val('');
			  	swal({
			  	  icon: "error",
				  title: "Não há sequência cadastrada neste modelo!"
				});
			  }
		  },
		});
	}else{
		$('#sequencia').attr("disabled", true);
		$('#sequencia').val('');
		$('#sequencia_bd').html('');
	}	
});

// Cadastra o PDV
$('#cad_pdv').click(function() {

	var cad_marca = $('#cad_marca').val();
	var cad_modelo = $('#cad_modelo').val();
	var sequencia = $('#sequencia').val();
	var matricula_operador = $('#matricula_operador').val();
	var tipo_prop = $('.tipo_prop').val();
	var cad_matricula_prop = $('#cad_matricula_prop').val();
	var cid_status = $('#cid_status').val();

	if (cad_marca == '' ||
		cad_modelo == '' ||
		sequencia == ''||
		matricula_operador == '' ||
		tipo_prop == ''||
		cad_matricula_prop == '' ||
		cid_status == ''){

		// alert(cad_marca+' - '+cad_modelo+' - '+sequencia+' - '+matricula_operador+' - '+tipo_prop+' - '+cad_matricula_prop+' - '+cid_status);

		swal({
			title: 'Preencha os campos obrigatórios!',
			icon: 'warning',
			confirmButtonColor: '#3085d6',
			confirmButtonText: 'Ok'
		})
	}else{
		var formPDV = $('#form-pdv').serialize();

		swal("Aguarde...!", {
		  closeOnClickOutside: false,
		  buttons: false
		});

		jQuery.ajax({
		  url: 'http://'+server+'/_app/_cadastros/class/cadastro-pdv.class.php',
		  type: 'POST',
		  data: formPDV,
		  success: function(data) {
		  	if(data == 'ok'){
			  	swal({
					title: 'Cadastro Realizado!',
					icon: 'success',
					confirmButtonColor: '#3085d6',
					confirmButtonText: 'Ok'
				}).then((value) => {
		         	window.location.reload();
		        });
			  }else if (data == 'ja_cadastrado'){
			  	swal({
					title: 'Esse PDV já está cadastrado!',
					icon: 'error',
					confirmButtonColor: '#3085d6',
					confirmButtonText: 'Ok'
				})
			  }else if (data == 'error'){
			  	swal({
					title: 'Tente Novamente!',
					icon: 'error',
					confirmButtonColor: '#3085d6',
					confirmButtonText: 'Ok'
				})
			  }
		  },
		});
	}	
});

var host = location.host;
var server = "";
if (host == 'localhost'){
    server = host+'/'+pasta;
}else{
    server = host;
}


// Nova Marca
function NovaMarca(valor){
	if (valor == 'nova-marca'){
		$('#marca').remove();

		// Cria o campo input da região
		$('<input>').attr({
		    type: 'text',
		    class: 'form-control focus',
		    name: 'cad_marca'
		}).appendTo('.appendPadrao');

		// Seta o campo
		$('.focus').focus();
	}
}


// Cadastro de padrao de pdv
$('.cad_padrao').click(function(){
	var marca = $('#marca').val();
	var modelo = $('#modelo').val();
	var sequencia = $('#sequencia').val();

	if (marca == '' || modelo == '' || sequencia == ''){
		swal({
	    	title: 'Preencha todos os campos obrigatórios (*)',
	    	icon: 'warning',
	    	confirmButtonColor: '#3085d6',
	    	confirmButtonText: 'Ok'
	    })
	}else{
		swal("Aguarde...!", {
		  closeOnClickOutside: false,
		  buttons: false
		});

		// Serializa o formulario
		var form = $('#form_padrao_pdv').serialize();

		$.ajax({
		    url: 'http://'+server+'/_app/_cadastros/class/cadastro-padrao-pdv.class.php',
		    type: "POST",
		    data:  form,
		    success: function(data){
		    	if (data == 'ok'){
		    		swal({
				    	title: 'Cadastro Realizado!',
				    	icon: 'success',
				    	confirmButtonColor: '#3085d6',
				    	confirmButtonText: 'Ok'
				    }).then((value) => {
		             	window.location.reload();
		            });
		    	}else{
		    		swal({
				    	title: 'Verifique as informações e tente novamente!',
				    	icon: 'warning',
				    	confirmButtonColor: '#3085d6',
				    	confirmButtonText: 'Ok'
				    })
		    	}
		    }         
	    });
	}
});


$('.cad_edit_padrao').click(function(){
	var id_modelo = $('#id_modelo').val();
	var edit_marca = $('#edit_marca').val();
	var edit_modelo = $('#edit_modelo').val();
	var edit_sequencia = $('#edit_sequencia').val();

	if (id_modelo=='' ||edit_marca == '' || edit_modelo == '' || edit_sequencia == ''){
		swal({
	    	title: 'Preencha todos os campos obrigatórios (*)',
	    	icon: 'warning',
	    	confirmButtonColor: '#3085d6',
	    	confirmButtonText: 'Ok'
	    })
	}else{
		// Serializa o form
		var formEditProp = $('#form-edit-padrao').serialize();
		$.ajax({
		    url: 'http://'+server+'/_app/_cadastros/class/atualiza-padrao-pdv.class.php',
		    type: "POST",
		    data:  formEditProp,
		    success: function(data){
		    	if (data == 'ok'){
		    		swal({
				      title: "Padrão PDV Atualizado!",
				      icon: "success",
				    }).then((value) => {
		             	window.location.reload();
		            });
		    	}else{
		    		swal({
				    	title: 'Verifique as informações e tente novamente!',
				    	icon: 'error',
				    	confirmButtonColor: '#3085d6',
				    	confirmButtonText: 'Ok'
				    })
		    	}
		    }         
	    });
	}

});


function editaModelo(id){
	// Envia o post para deletar o bairro
	$.ajax({
	    url: 'http://'+server+'/_app/class_api/busca-padrao-pdv.class.php',
	    type: "POST",
	    dataType: "json",
	    data:  {buscaModelo: id},
	    success: function(data){
	    	// Exibe o formulario de edição
	    	$('#form_padrao_pdv').hide();
			$('#form-edit-padrao').show();

			// define o nome da marca no select
			document.getElementById('edit_marca').value = data[1];

			$('#edit_modelo').val(data[0]);
			$('#edit_sequencia').val(data[3]);
			
			// define o id do bairro no valor do input
			$('#id_modelo').val(data[2]);
	    }         
	}); 
}

function RemoverPadrao(id){
	swal({
	  title: "Deletar Padrão de PDV?",
	  text: "Ao deletar, a área de PDV poderá ser afetada!",
	  icon: "warning",
	  buttons: {
	  	cancel: "Não",
	  	confirm: "Sim",
	  }
	}).then((willDelete) => {
	  	if (willDelete) {
		 	swal({
			  title: "Aguarde..."
			})

	  	// Envia o post para deletar o bairro
	  	$.ajax({
		    url: 'http://'+server+'/_app/_cadastros/class/deleta-padrao-pdv.class.php',
		    type: "POST",
		    data:  {delPadrao: id},
		    success: function(data){
		    	if (data == 'ok'){
		    		swal({
				      title: "Padrão Deletado!",
				      icon: "success",
				    }).then((value) => {
		             	window.location.reload();
		            });
		    	}else{
		    		swal({
				    	title: 'Verifique as informações e tente novamente!',
				    	icon: 'error',
				    	confirmButtonColor: '#3085d6',
				    	confirmButtonText: 'Ok'
				    })
		    	}
		    }         
	    });
	  } else {
	    swal.close();
	  }
	});
}
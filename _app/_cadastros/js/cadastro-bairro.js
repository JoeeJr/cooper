var host = location.host;
var server = "";
if (host == 'localhost'){
    server = host+'/'+pasta;
}else{
    server = host;
}

function NovaRegiao(valor){

	if (valor == 'nova-regiao'){
		$('#nova-regiao').remove();

		// Cria o campo input da região
		$('<input>').attr({
		    type: 'text',
		    class: 'form-control focus',
		    name: 'cad_regiao'
		}).appendTo('.appendRegiao');

		// Seta o campo
		$('.focus').focus();
	}

}

// Cadastro de bairro
$('#cadastrar-bairro').click(function(){
	var cad_regiao = $('.cad_regiao').val();
	var cad_bairro = $('.cad_bairro').val();

	if (cad_regiao == '' || cad_bairro == ''){
		swal({
	    	title: 'Preencha todos os campos obrigatórios (*)',
	    	icon: 'warning',
	    	confirmButtonColor: '#3085d6',
	    	confirmButtonText: 'Ok'
	    })
	}else{
		swal("Aguarde...!", {
		  closeOnClickOutside: false,
		  buttons: false
		});

		// Serializa o formulario
		var form = $('#form-bairro').serialize();

		$.ajax({
		    url: 'http://'+server+'/_app/_cadastros/class/cadastro-bairro.class.php',
		    type: "POST",
		    data:  form,
		    success: function(data){
		    	if (data == 'ok'){
		    		swal({
				    	title: 'Cadastro Realizado!',
				    	icon: 'success',
				    	confirmButtonColor: '#3085d6',
				    	confirmButtonText: 'Ok'
				    }).then((value) => {
		             	window.location.reload();
		            });
		    	}else{
		    		swal({
				    	title: 'Verifique as informações e tente novamente!',
				    	icon: 'warning',
				    	confirmButtonColor: '#3085d6',
				    	confirmButtonText: 'Ok'
				    })
		    	}
		    }         
	    });
	}
});


function RemoverBairro(id){
	swal({
	  title: "Deletar Bairro?",
	  text: "Ao deletar, cooperados com este bairro serão afetados!",
	  icon: "warning",
	  buttons: {
	  	cancel: "Não",
	  	confirm: "Sim",
	  }
	}).then((willDelete) => {
	  	if (willDelete) {
		 	swal({
			  title: "Aguarde..."
			})

	  	// Envia o post para deletar o bairro
	  	$.ajax({
		    url: 'http://'+server+'/_app/_cadastros/class/deleta-bairro.class.php',
		    type: "POST",
		    data:  {delBairro: id},
		    success: function(data){
		    	if (data == 'ok'){
		    		swal({
				      title: "Bairro Deletado!",
				      icon: "success",
				    }).then((value) => {
		             	window.location.reload();
		            });
		    	}else{
		    		swal({
				    	title: 'Verifique as informações e tente novamente!',
				    	icon: 'error',
				    	confirmButtonColor: '#3085d6',
				    	confirmButtonText: 'Ok'
				    })
		    	}
		    }         
	    });
	  } else {
	    swal.close();
	  }
	});
}




function editaBairro(id){
	// Envia o post para deletar o bairro
	$.ajax({
	    url: 'http://'+server+'/_app/class_api/busca-bairro.class.php',
	    type: "POST",
	    dataType: "json",
	    data:  {buscaBairro: id},
	    success: function(data){
	    	// Exibe o formulario de edição
	    	$('#form-bairro').hide();
			$('#form-edit-bairro').show();

			// define o nome do bairro no campo
			$('.edit_bairro').val(data[0]);

			// define o nome da regiao no select
			document.getElementById('edit-regiao').value = data[1];

			// define o id do bairro no valor do input
			$('.id_bairro').val(data[2]);
	    }         
	}); 
}



$('#atualizar-bairro').click(function(){

	var idBairro = $('.id_bairro').val();
	var regiao = $('#edit-regiao').val();
	var bairro = $('.edit_bairro').val();

	if (idBairro == '' || regiao == '' || bairro == ''){
		swal({
	    	title: 'Preencha todos os campos obrigatórios (*)',
	    	icon: 'warning',
	    	confirmButtonColor: '#3085d6',
	    	confirmButtonText: 'Ok'
	    })
	}else{
		// Serializa o form
		var formEditBairro = $('#form-edit-bairro').serialize();
		$.ajax({
		    url: 'http://'+server+'/_app/_cadastros/class/atualiza-bairro.class.php',
		    type: "POST",
		    data:  formEditBairro,
		    success: function(data){
		    	if (data == 'ok'){
		    		swal({
				      title: "Bairro Atualizado!",
				      icon: "success",
				    }).then((value) => {
		             	window.location.reload();
		            });
		    	}else{
		    		swal({
				    	title: 'Verifique as informações e tente novamente!',
				    	icon: 'error',
				    	confirmButtonColor: '#3085d6',
				    	confirmButtonText: 'Ok'
				    })
		    	}
		    }         
	    });
	}

});
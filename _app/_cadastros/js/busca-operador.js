// autocomplet : this function will be executed every time we change the text
var host = location.host;
var server = "";
if (host == 'localhost'){
    server = host+'/'+pasta;
}else{
    server = host;
}

// Verifica o tipo de proprietario
$('.tipo_prop').change(function(){
	var tipo_prop = this.value;

	if(tipo_prop == 'cooperado'){
		$(".form_tipo_prop").load('http://'+server+'/tipo_coop');
	}else if (tipo_prop == 'terceiro'){
		$(".form_tipo_prop").load('http://'+server+'/tipo_terc');
	}
});

$('.cid_status').change(function(){
	var status = this.value;

	if(status == 'Bloqueada' || status == 'Manuntenção' || status == 'Inutilizada'){
		$('.cad_obs').val('');
		$('.cad_obs').show();
	}else{
		$('.cad_obs').val('');
		$('.cad_obs').hide();
	}
});

// Busca o operador pela matricula
$('#busca_matricula').blur(function(){
	var id = $('#busca_matricula').val();
	if (id != ''){


		swal("Aguarde...!", {
		  closeOnClickOutside: false,
		  buttons: false
		});


		$.ajax({
			url: 'http://'+server+'/_app/class_api/busca-matricula.class.php',
			type: 'POST',
			dataType: 'json',
			data: {matricula:id},
			success:function(data){
				if (data == 'not_find'){
					$('#busca_matricula').val('');

					swal({
				    	title: 'Cooperado não encontrado!',
				    	icon: 'warning',
				    	confirmButtonColor: '#3085d6',
				    	confirmButtonText: 'Ok'
				    })
				}else{
					// Fecha a modal
					swal.close();

					// Seta o nome do operador no campo
					$('#busca_operador').val(data[1]);

					// ID do operador
					$('#matricula').val(data[0]);
					$('#matricula_operador').val(data[0]);

					// Oculta a busca
					$('.cl_operador').hide();

					// Mostra o chipview
					$('#chipview-operador').show();

					// Seta o nome do cooperado no chipview
					var str = "("+data[0]+") - "+data[1];
					// if(str.length > 23) str = str.substring(0,23)+'...';
					$('#txt_name_prepo2').html(str);

					// Seta o valor do ID no button para fechar depois o chipview
					$('#close-chip2').val(data[0]);

					// hide proposition list
					$('#lista_operador').hide();
				}
			}
		});
	}
});


// ########################## BUSCA DE OPERADOR DE PDV ######################


function BuscaOperador() {
	var inpBusca = $('#busca_operador').val();
	

	var min_length = 0; // min caracters to display the autocomplete
	var keyword = $('#busca_operador').val();
	if (keyword.length >= min_length) {

		if (inpBusca == '') {
			$('#lista_operador').hide();
		}else{

			$.ajax({
				url: 'http://'+server+'/_app/class_api/busca-operador.class.php',
				type: 'POST',
				data: {keyword:keyword},
				success:function(data){
					$('#lista_operador').show();
					

					if (data == ''){
						var estrutura = "<li class='listagem_nenhum'>Nenhum resultado encontrado</li>";
						$('#lista_operador').html(estrutura);
					}else{
						$('#lista_operador').html(data);
					}
				}
			});
		}


	} else {
		$('#lista_operador').hide();
	}
}

function verificaOperador(){
	var inpBusca = $('#busca_operador').val();
	if (inpBusca == '') {
		$('.listagem').hide();
	}
}

// set_item : this function will be executed when we select an item
function set_item_operador(item, item2) {
	// Seta o nome do operador no campo
	$('#busca_operador').val(item);

	// ID do operador
	$('#matricula').val(item2);

	// Oculta a busca
	$('.cl_operador').hide();

	// Mostra o chipview
	$('#chipview-operador').show();

	// Seta o nome do cooperado no chipview
	var str = "("+item2+") - "+item;
	// if(str.length > 23) str = str.substring(0,23)+'...';
	$('#txt_name_prepo2').html(str);

	// Seta o valor do ID no button para fechar depois o chipview
	$('#close-chip2').val(item2);

	// hide proposition list
	$('#lista_operador').hide();
}

function closeChip2(id){
	// Oculta o chipview
	$('#chipview-operador').hide();

	// Zera o valor do ID no input
	$('#matricula').val('');
	$('#matricula_operador').val('');
	$('#busca_operador').val('');
	$('#busca_matricula').val('');

	// Exibe o campo de busca vazio
	$('.cl_operador').val('');
	$('.cl_operador').show();
}






// ###################### BUSCA DE COOPERADO PROPRIETARIO DE PDV ##########################


// Busca o operador pela matricula
function BuscaMatriculaCoop(mat){
	var id = $('.buscaMatPropCoop').val();
	if (id != ''){


		swal("Aguarde...!", {
		  closeOnClickOutside: false,
		  buttons: false
		});


		$.ajax({
			url: 'http://'+server+'/_app/class_api/busca-matricula.class.php',
			type: 'POST',
			dataType: 'json',
			data: {matricula:mat},
			success:function(data){
				if (data == 'not_find'){
					$('.buscaMatPropCoop').val('');

					swal({
				    	title: 'Cooperado não encontrado!',
				    	icon: 'warning',
				    	confirmButtonColor: '#3085d6',
				    	confirmButtonText: 'Ok'
				    })
				}else{
					// Fecha a modal
					swal.close();

					// Seta o nome do operador no campo
					$('#busca_prop_cooperado').val(data[1]);

					// ID do operador
					$('#cad_matricula_prop').val(data[0]);					

					// Oculta a busca
					$('.cl_prop_coop').hide();

					// Mostra o chipview
					$('#chipview-prop-cooperado').show();

					// Seta o nome do cooperado no chipview
					var str = "("+data[0]+") - "+data[1];
					// if(str.length > 23) str = str.substring(0,23)+'...';
					$('#txt_name_prop_coop').html(str);

					// Seta o valor do ID no button para fechar depois o chipview
					$('#close-chip-prop-coop').val(data[0]);

					// hide proposition list
					$('#lista_prop_coop').hide();
				}
			}
		});
	}
}

function BuscaPropCoop() {
	var inpBusca = $('#busca_prop_cooperado').val();
	

	var min_length = 0; // min caracters to display the autocomplete
	var keyword = $('#busca_prop_cooperado').val();
	if (keyword.length >= min_length) {

		if (inpBusca == '') {
			$('#lista_prop_coop').hide();
		}else{

			$.ajax({
				url: 'http://'+server+'/_app/class_api/busca-proprietario-cooperado.class.php',
				type: 'POST',
				data: {keyword:keyword},
				success:function(data){
					$('#lista_prop_coop').show();
					

					if (data == ''){
						var estrutura = "<li class='listagem_nenhum'>Nenhum resultado encontrado</li>";
						$('#lista_prop_coop').html(estrutura);
					}else{
						$('#lista_prop_coop').html(data);
					}
				}
			});
		}


	} else {
		$('#lista_operador').hide();
	}
}

function verificaPropCoop(){
	var inpBusca = $('#busca_prop_cooperado').val();
	if (inpBusca == '') {
		$('.listagem_prop_coop').hide();
	}
}

// set_item : this function will be executed when we select an item
function set_prop_cooperado(item, item2) {
	// Seta o nome do operador no campo
	$('#busca_prop_cooperado').val(item);

	// ID do operador
	$('#matricula_prop_cooperado_temp').val(item2);
	$('#cad_matricula_prop').val(item2);

	// Oculta a busca
	$('.cl_prop_coop').hide();

	// Mostra o chipview
	$('#chipview-prop-cooperado').show();

	// Seta o nome do cooperado no chipview
	var str = "("+item2+") - "+item;
	// if(str.length > 23) str = str.substring(0,23)+'...';
	$('#txt_name_prop_coop').html(str);

	// Seta o valor do ID no button para fechar depois o chipview
	$('#close-chip-prop-coop').val(item2);

	// hide proposition list
	$('#lista_prop_coop').hide();
}

function closeChipPropCoop(id){
	// Oculta o chipview
	$('#chipview-prop-cooperado').hide();

	// Zera o valor do ID no input
	$('#cad_matricula_prop').val('');
	$('#busca_prop_cooperado').val('');
	$('#matricula_prop_cooperado_temp').val('');

	// Exibe o campo de busca vazio
	$('.cl_prop_coop').show();
}







// ###################### BUSCA DE TERCEIRO PROPRIETARIO DE PDV ##########################

// Busca o terceiro pela matricula
function BuscaMatriculaTerc(mat){
	var id = $('.buscaMatPropTerc').val();
	if (id != ''){


		swal("Aguarde...!", {
		  closeOnClickOutside: false,
		  buttons: false
		});


		$.ajax({
			url: 'http://'+server+'/_app/class_api/busca-matricula-terceiro.class.php',
			type: 'POST',
			dataType: 'json',
			data: {matricula:mat},
			success:function(data){
				if (data == 'not_find'){
					$('.buscaMatPropTerc').val('');

					swal({
				    	title: 'Terceiro(a) não encontrado!',
				    	icon: 'warning',
				    	confirmButtonColor: '#3085d6',
				    	confirmButtonText: 'Ok'
				    })
				}else{
					// Fecha a modal
					swal.close();

					// Seta o nome do operador no campo
					$('#busca_prop_cooperado').val(data[1]);

					// ID do operador
					$('#cad_matricula_prop').val(data[0]);					

					// Oculta a busca
					$('.cl_prop_coop').hide();

					// Mostra o chipview
					$('#chipview-prop-cooperado').show();

					// Seta o nome do cooperado no chipview
					var str = "("+data[0]+") - "+data[1];
					// if(str.length > 23) str = str.substring(0,23)+'...';
					$('#txt_name_prop_coop').html(str);

					// Seta o valor do ID no button para fechar depois o chipview
					$('#close-chip-prop-coop').val(data[0]);

					// hide proposition list
					$('#lista_prop_coop').hide();
				}
			}
		});
	}
}


function BuscaPropTerc() {
	var inpBusca = $('#busca_prop_terceiro').val();
	

	var min_length = 0; // min caracters to display the autocomplete
	var keyword = $('#busca_prop_terceiro').val();
	if (keyword.length >= min_length) {

		if (inpBusca == '') {
			$('#lista_prop_coop').hide();
		}else{

			$.ajax({
				url: 'http://'+server+'/_app/class_api/busca-proprietario-terceiro.class.php',
				type: 'POST',
				data: {keyword:keyword},
				success:function(data){
					$('#lista_prop_coop').show();
					

					if (data == ''){
						var estrutura = "<li class='listagem_nenhum'>Nenhum resultado encontrado</li>";
						$('#lista_prop_coop').html(estrutura);
					}else{
						$('#lista_prop_coop').html(data);
					}
				}
			});
		}


	} else {
		$('#lista_operador').hide();
	}
}

function verificaPropTerc(){
	var inpBusca = $('#busca_prop_terceiro').val();
	if (inpBusca == '') {
		$('.listagem_prop_coop').hide();
	}
}

// set_item : this function will be executed when we select an item
function set_prop_terceiro(item, item2) {
	// Seta o nome do operador no campo
	$('#busca_prop_terceiro').val(item);

	// ID do operador
	$('#matricula_prop_cooperado_temp').val(item2);
	$('#cad_matricula_prop').val(item2);

	// Oculta a busca
	$('.cl_prop_coop').hide();

	// Mostra o chipview
	$('#chipview-prop-cooperado').show();

	// Seta o nome do cooperado no chipview
	var str = "("+item2+") - "+item;
	// if(str.length > 23) str = str.substring(0,23)+'...';
	$('#txt_name_prop_coop').html(str);

	// Seta o valor do ID no button para fechar depois o chipview
	$('#close-chip-prop-coop').val(item2);

	// hide proposition list
	$('#lista_prop_coop').hide();
}

function closeChipPropTerc(id){
	// Oculta o chipview
	$('#chipview-prop-cooperado').hide();

	// Zera o valor do ID no input
	$('#cad_matricula_prop').val('');
	$('#busca_prop_terceiro').val('');
	$('#matricula_prop_cooperado_temp').val('');

	// Exibe o campo de busca vazio
	$('.cl_prop_coop').show();
}
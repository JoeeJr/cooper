var host = location.host;
var server = "";
if (host == 'localhost'){
    server = host+'/'+pasta;
}else{
    server = host;
}

// Campo de Moeda
$(function() {
	$('.cad_taxa_adm').maskMoney();
	$('.cad_taxa_aluguel').maskMoney();
	$('.valor_cad').maskMoney();
	$('.taxa_aluguel_50').maskMoney();
})


// Mascaras nos campos
$(document).ready(function(){       
   //  CPF
   $('#cpf').mask('000.000.000-00');

   //  Telefone Fixo
   $('.fixo').mask('(00) 0000-0000');

   //  Telefone Celular
   $('.celular').mask('(00) 00000-0000');

   //  CEP
   $('.cep').mask('00000-000');

   // Data de Nascimento
   $('.vencimento_boleto')
    .datepicker({
        format: 'dd/mm/yyyy'
    })
});

// Selecão do tipo de plano
function TipoPlano(valor){
	if (valor == 'pre'){
		$('.taxa_adm').show();
		$('.taxa_aluguel').hide();
		$('.taxa_aluguel').val('');
	}else{
		$('.taxa_adm').hide();
		$('.taxa_aluguel').show();
		$('.taxa_adm').val('');
	}
}

// Selecao do tipo de cooperado
function TipoCooperado(tipo){
	if (tipo == 'preponente'){
		$('.txt_preponente').show();
		$('.txt_preposto').hide();
		$('.txt_preposto').val('');

		// Oculta resultados de busca
		$('#country_list_id').hide();
		$('#lista_preponente').hide();

		// Remove o chipview do preposto
		$('#chipview-preponente').hide();

		// Zera o valor do ID no input
		$('#close-chip2').val('');

		// Zera o ID do preponente
		$('.sec_preponente').val('');

	}else if(tipo == 'preposto'){
		$('.txt_preposto').show();
		$('.txt_preponente').hide();
		$('.txt_preponente').val('');

		// Oculta resultados de busca
		$('#country_list_id').hide();
		$('#lista_preponente').hide();

		// Remove o chipview do preponente
		$('#chipview-preposto').hide();

		// Zera o valor do ID no input
		$('#close-chip').val('');

		// Retira o nome do chipview
		$('#txt_name_prepo').html('');
		$('#txt_name_prepo2').html('');

		// Zera o ID do preponente
		$('.sec_preposto').val('');

	}else if(tipo == 'nenhum'){
		$('.txt_preposto').hide();
		$('.txt_preponente').hide();
		$('.txt_preponente').val('');
		$('.txt_preposto').val('');

		// Oculta resultados de busca
		$('#country_list_id').hide();
		$('#lista_preponente').hide();

		// Remove o chipview do preposto/preponente

		$('#chipview-preponente').hide();
		// Zera o valor do ID no input
		$('#close-chip2').val('');

		// Remove o chipview do preponente
		$('#chipview-preposto').hide();

		// Zera o valor do ID no input
		$('#close-chip').val('');

		// Retira o nome do chipview
		$('#txt_name_prepo').html('');
		$('#txt_name_prepo2').html('');

		// Zera os valores de preponente e preposto (ID)
		$('.sec_preponente').val('');
		$('.sec_preposto').val('');


	}
}

// Preview da imagem
function readURL(input) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      $('.photoCoop').attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
  }
}

$("#foto_cooperado").change(function() {
  readURL(this);
});


// Cadastro de Cooperado
$('#cadastrar-cooperado').click(function(){
	var cad_nome = $('.cad_nome').val();
	var cad_rg = $('.cad_rg').val();
	var cad_emissor = $('.cad_emissor').val();
	var cad_cpf = $('.cad_cpf').val();
	var cad_data_nasc = $('.cad_data_nasc').val();
	var cad_estado_civil = $('.cad_estado_civil').val();
	var cad_profissao = $('.cad_profissao').val();
	var cad_naturalidade = $('.cad_naturalidade').val();
	var cad_cel = $('.cad_cel').val();
	var cad_email = $('.cad_email').val();	
	var cad_preposto = $('.cad_preposto').val();
	var cad_cep = $('.cad_cep').val();
	var cad_endereco = $('.cad_endereco').val();
	var cad_numero = $('.cad_numero').val();
	var cad_bairro = $('.cad_bairro').val();
	var cad_cidade = $('.cad_cidade').val();
	var cad_regiao = $('.cad_regiao').val();
	var cad_bairro_com = $('.cad_bairro_com').val();
	var cad_end_com = $('.cad_end_com').val();
	var cad_numero_com = $('.cad_numero_com').val();
	var cad_taxa_adm = $('.cad_taxa_adm').val();
	var cad_taxa_aluguel = $('.cad_taxa_aluguel').val();
	var valor_cad = $('.valor_cad').val();
	var valor_meta = $('.valor_meta').val();
	var taxa_aluguel_50 =  $('.taxa_aluguel_50').val();
	var tipo_coop = $('.tipo_coop').val();
	
	// Form
	var formulario = document.getElementById('form_cooperado');
    var form = new FormData(formulario);


	if(	cad_nome != '' &&
		cad_rg != '' &&
		cad_emissor != '' &&
		cad_cpf != '' &&
		cad_data_nasc != '' &&
		cad_estado_civil != '' &&
		cad_profissao != '' &&
		cad_naturalidade != '' &&
		cad_cel != '' &&
		cad_email != '' &&
		cad_cep != '' &&
		cad_endereco != '' &&
		cad_numero != '' &&
		cad_bairro != '' &&
		cad_cidade != '' &&
		cad_regiao != '' &&
		cad_bairro_com != '' &&
		cad_end_com != '' &&
		cad_numero_com != '' &&
		valor_cad != '' &&
		valor_meta != '' &&
		taxa_aluguel_50 != '' &&
		tipo_coop != '' &&
		(cad_taxa_adm != '' ||
		cad_taxa_aluguel != '')){

		swal("Aguarde...!", {
		  closeOnClickOutside: false,
		  buttons: false
		});

		$.ajax({
		    url: 'http://'+server+'/_app/_cadastros/class/cadastro-cooperado.class.php',
		    type: "POST",
		    data:  form,
		    contentType: false,
	        cache: false,
	        processData:false,
		    success: function(data){
		    	if (data == 'ok'){
		    		swal({
				    	title: 'Cadastro Realizado!',
				    	icon: 'success',
				    	confirmButtonColor: '#3085d6',
				    	confirmButtonText: 'Ok'
				    }).then((value) => {
		             	window.location.reload();
		            });
		    	}else if(data == 'error_cpf'){
		    		swal({
				    	title: 'Já existe um cadastro com esse CPF!',
				    	icon: 'warning',
				    	confirmButtonColor: '#3085d6',
				    	confirmButtonText: 'Ok'
				    })
		    	}else if(data == 'error_preponente'){
		    		swal({
				    	title: 'Este preponente já está vinculado!',
				    	icon: 'warning',
				    	confirmButtonColor: '#3085d6',
				    	confirmButtonText: 'Ok'
				    })
		    	}
		    	else if(data == 'error_preposto'){
		    		swal({
				    	title: 'Este preposto já está vinculado!',
				    	icon: 'warning',
				    	confirmButtonColor: '#3085d6',
				    	confirmButtonText: 'Ok'
				    })
		    	}else{
		    		swal({
				    	title: 'Tente Novamente!',
				    	icon: 'warning',
				    	confirmButtonColor: '#3085d6',
				    	confirmButtonText: 'Ok'
				    })
		    	}
		    }         
	    });


	}else{
		swal({
	    	title: 'Preencha todos os campos obrigatórios (*)',
	    	icon: 'warning',
	    	confirmButtonColor: '#3085d6',
	    	confirmButtonText: 'Ok'
	    })
	}
});


// Busca os bairros relacionados a região selecionada
$('#cad_regiao').change(function() {

	var regiao = $('#cad_regiao option:selected').val();

	swal("Aguarde...!", {
	  closeOnClickOutside: false,
	  buttons: false
	});

		jQuery.ajax({
		  url: 'http://'+server+'/_app/class_api/busca-bairros.php',
		  type: 'POST',
		  data: {regiao: regiao},
		  success: function(data) {
		  	$('#cad_bairro_com').html(data);
		  	swal.close();
		  },
		});	
});
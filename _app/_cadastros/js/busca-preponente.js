// autocomplet : this function will be executed every time we change the text
var host = location.host;
var server = "";
if (host == 'localhost'){
    server = host+'/'+pasta;
}else{
    server = host;
}

function BuscaPreponente() {
	var inpBusca = $('#busca_preponente').val();
	

	var min_length = 0; // min caracters to display the autocomplete
	var keyword = $('#busca_preponente').val();
	if (keyword.length >= min_length) {

		if (inpBusca == '') {
			$('#lista_preponente').hide();
		}else{

			$.ajax({
				url: 'http://'+server+'/_app/class_api/busca-preponente.class.php',
				type: 'POST',
				data: {keyword:keyword},
				success:function(data){
					$('#lista_preponente').show();
					

					if (data == ''){
						var estrutura = "<li class='listagem_nenhum'>Nenhum resultado encontrado</li>";
						$('#lista_preponente').html(estrutura);
					}else{
						$('#lista_preponente').html(data);
					}
				}
			});
		}


	} else {
		$('#lista_preponente').hide();
	}
}


function verificaPreponente(){
	var inpBusca = $('#busca_preponente').val();
	if (inpBusca == '') {
		$('.listagem').hide();
	}
}





// set_item : this function will be executed when we select an item
function set_item_preponente(item, item2) {
	// Seta o nome do preponente no campo
	$('#busca_preponente').val(item);

	// ID do preponente
	$('.cadastro_preponente').val(item2);

	// Oculta a busca
	$('.sec_preponente').hide();

	// Mostra o chipview
	$('#chipview-preponente').show();

	// Seta o nome do cooperado no chipview
	var str = item;
	if(str.length > 23) str = str.substring(0,23)+'...';
	$('#txt_name_prepo2').html(str);

	// Seta o valor do ID no button para fechar depois o chipview
	$('#close-chip2').val(item2);

	// hide proposition list
	$('#lista_preponente').hide();
}

function closeChip2(id){
	// Oculta o chipview
	$('#chipview-preponente').hide();

	// Zera o valor do ID no input
	$('#close-chip2').val('');

	// Exibe o campo de busca vazio
	$('.sec_preponente').val('');
	$('.sec_preponente').show();
}
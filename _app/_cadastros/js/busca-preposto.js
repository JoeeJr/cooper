// autocomplet : this function will be executed every time we change the text
var host = location.host;
var server = "";
if (host == 'localhost'){
    server = host+'/'+pasta;
}else{
    server = host;
}

function autocomplet() {
	var inpBusca = $('#country_id').val();
	

	var min_length = 0; // min caracters to display the autocomplete
	var keyword = $('#country_id').val();
	if (keyword.length >= min_length) {

		if (inpBusca == '') {
			$('#country_list_id').hide();
		}else{

			$.ajax({
				url: 'http://'+server+'/_app/class_api/busca-preposto.class.php',
				type: 'POST',
				data: {keyword:keyword},
				success:function(data){
					$('#country_list_id').show();
					

					if (data == ''){
						var estrutura = "<li class='listagem_nenhum'>Nenhum resultado encontrado</li>";
						$('#country_list_id').html(estrutura);
					}else{
						$('#country_list_id').html(data);
					}
				}
			});
		}


	} else {
		$('#country_list_id').hide();
	}
}


function verificacampo(){
	var inpBusca = $('#country_id').val();
	if (inpBusca == '') {
		$('.listagem').hide();
	}
}





// set_item : this function will be executed when we select an item
function set_item(item, item2) {
	// change input value
	$('#country_id').val(item);

	// ID do preposto
	$('.cadastro_preposto').val(item2);

	// Oculta o campo de busca de preposto
	$('.sec_preposto').hide();

	// Mostra o chipview
	$('#chipview-preposto').show();

	// Seta o nome do cooperado no chipview
	var str = item;
	if(str.length > 23) str = str.substring(0,23)+'...';
	$('#txt_name_prepo').html(str);

	// Seta o valor do ID no button para fechar depois o chipview
	$('#close-chip').val(item2);

	// hide proposition list
	$('#country_list_id').hide();
}

function closeChip(id){
	// Oculta o chipview
	$('#chipview-preposto').hide();

	// Zera o valor do ID no input
	$('#close-chip').val('');

	// Exibe o campo de busca vazio
	$('.sec_preposto').val('');
	$('.sec_preposto').show();

}


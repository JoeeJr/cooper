var host = location.host;
var server = "";
if (host == 'localhost'){
    server = host+'/'+pasta;
}else{
    server = host;
}

// CPF CNPJ
$('#prop_cpf').keydown(function(){
	var cpf_cnpj = $('#prop_cpf').val();
	var tam = cpf_cnpj.length;

	if (tam <= 13){
		$('#prop_cpf').mask('000.000.000-00');
	}else{
		$('#prop_cpf').mask('00.000.000/0000-00');
	}


	
});

// CPF CNPJ
$('#prop_edit_cpf').keydown(function(){
	var cpf_cnpj2 = $('#prop_edit_cpf').val();
	var tam2 = cpf_cnpj2.length;

	
	if (tam2 <= 13){
		$('#prop_edit_cpf').mask('000.000.000-00');
	}else{
		$('#prop_edit_cpf').mask('00.000.000/0000-00');
	}

	
});

// Cadastro de proprietario de pdv
$('.cad_prop_pdv').click(function(){
	var prop_nome = $('#prop_nome').val();
	var prop_rg = $('#prop_rg').val();
	var prop_cpf = $('#prop_cpf').val();

	if (prop_nome == '' || prop_rg == '' || prop_cpf == ''){
		swal({
	    	title: 'Preencha todos os campos obrigatórios (*)',
	    	icon: 'warning',
	    	confirmButtonColor: '#3085d6',
	    	confirmButtonText: 'Ok'
	    })
	}else{
		swal("Aguarde...!", {
		  closeOnClickOutside: false,
		  buttons: false
		});

		// Serializa o formulario
		var form = $('#proprietario_pdv').serialize();

		$.ajax({
		    url: 'http://'+server+'/_app/_cadastros/class/cadastro-proprietario-pdv.class.php',
		    type: "POST",
		    data:  form,
		    success: function(data){
		    	if (data == 'ok'){
		    		swal({
				    	title: 'Cadastro Realizado!',
				    	icon: 'success',
				    	confirmButtonColor: '#3085d6',
				    	confirmButtonText: 'Ok'
				    }).then((value) => {
		             	window.location.reload();
		            });
		    	}else{
		    		swal({
				    	title: 'Verifique as informações e tente novamente!',
				    	icon: 'warning',
				    	confirmButtonColor: '#3085d6',
				    	confirmButtonText: 'Ok'
				    })
		    	}
		    }         
	    });
	}
});


$('.edit_prop_pdv').click(function(){
	var id_prop = $('.edit_prop').val();
	var prop_nome = $('#prop_edit_nome').val();
	var prop_rg = $('#prop_edit_rg').val();
	var prop_cpf = $('#prop_edit_cpf').val();

	if (id_prop=='' ||prop_nome == '' || prop_rg == '' || prop_cpf == ''){
		swal({
	    	title: 'Preencha todos os campos obrigatórios (*)',
	    	icon: 'warning',
	    	confirmButtonColor: '#3085d6',
	    	confirmButtonText: 'Ok'
	    })
	}else{
		// Serializa o form
		var formEditProp = $('#form-edit-proprietarios').serialize();
		$.ajax({
		    url: 'http://'+server+'/_app/_cadastros/class/atualiza-proprietario-pdv.class.php',
		    type: "POST",
		    data:  formEditProp,
		    success: function(data){
		    	if (data == 'ok'){
		    		swal({
				      title: "Proprietário Atualizado!",
				      icon: "success",
				    }).then((value) => {
		             	window.location.reload();
		            });
		    	}else{
		    		swal({
				    	title: 'Verifique as informações e tente novamente!',
				    	icon: 'error',
				    	confirmButtonColor: '#3085d6',
				    	confirmButtonText: 'Ok'
				    })
		    	}
		    }         
	    });
	}

});


function editaProprietario(id){
	// Envia o post para deletar o bairro
	$.ajax({
	    url: 'http://'+server+'/_app/class_api/busca-proprietario-pdv.class.php',
	    type: "POST",
	    dataType: "json",
	    data:  {buscaPropietario: id},
	    success: function(data){
	    	// Exibe o formulario de edição
	    	$('#proprietario_pdv').hide();
			$('#form-edit-proprietarios').show();

			$('#prop_edit_nome').val(data[1]);
			$('#prop_edit_rg').val(data[2]);
			$('#prop_edit_cpf').val(data[3]);
			
			// define o id do bairro no valor do input
			$('.edit_prop').val(data[0]);
	    }         
	}); 
}

function RemoverProprietario(id){
	swal({
	  title: "Deletar Proprietário?",
	  text: "Ao deletar, a área de PDV poderá ser afetada!",
	  icon: "warning",
	  buttons: {
	  	cancel: "Não",
	  	confirm: "Sim",
	  }
	}).then((willDelete) => {
	  	if (willDelete) {
		 	swal({
			  title: "Aguarde..."
			})

	  	// Envia o post para deletar o bairro
	  	$.ajax({
		    url: 'http://'+server+'/_app/_cadastros/class/deleta-proprietario-pdv.class.php',
		    type: "POST",
		    data:  {delProp: id},
		    success: function(data){
		    	if (data == 'ok'){
		    		swal({
				      title: "Proprietário Deletado!",
				      icon: "success",
				    }).then((value) => {
		             	window.location.reload();
		            });
		    	}else{
		    		swal({
				    	title: 'Verifique as informações e tente novamente!',
				    	icon: 'error',
				    	confirmButtonColor: '#3085d6',
				    	confirmButtonText: 'Ok'
				    })
		    	}
		    }         
	    });
	  } else {
	    swal.close();
	  }
	});
}
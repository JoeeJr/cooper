// autocomplet : this function will be executed every time we change the text
var host = location.host;
var server = "";
if (host == 'localhost'){
    server = host+'/'+pasta;
}else{
    server = host;
}

$('.cad_termo').click(function(){
	var titulo = $('#titulo').val();
	var termo = CKEDITOR.instances.termo.getData();

	if (titulo == '' || termo == ''){
		swal({
	    	title: 'Preencha todos os campos obrigatórios (*)',
	    	icon: 'warning',
	    	confirmButtonColor: '#3085d6',
	    	confirmButtonText: 'Ok'
	    })
	}else{
		swal("Aguarde...!", {
		  closeOnClickOutside: false,
		  buttons: false
		});

		var form = document.getElementById('form_cad_termos');
		var dataForm = new FormData(form);

		// text area
		dataForm.append('cl_termo', termo);

		$.ajax({
		    url: 'http://'+server+'/_app/_cadastros/class/cadastro-termos.class.php',
		    type: "POST",
		    data:  dataForm,
		    contentType: false,
	        cache: false,
	        processData:false,
		    success: function(data){
		    	if (data == 'ok'){
		    		swal({
				    	title: 'Termo Cadastrado!',
				    	icon: 'success',
				    	confirmButtonColor: '#3085d6',
				    	confirmButtonText: 'Ok'
				    }).then((value) => {
		             	window.location.reload();
		            });
		    	}else{
		    		swal({
				    	title: 'Verifique as informações e tente novamente!',
				    	icon: 'warning',
				    	confirmButtonColor: '#3085d6',
				    	confirmButtonText: 'Ok'
				    })
		    	}
		    }         
	    });
	}

});


// Busca os bairros relacionados a região selecionada
$('#cad_regiao').change(function() {

	var regiao = $('#cad_regiao option:selected').val();

	swal("Aguarde...!", {
	  closeOnClickOutside: false,
	  buttons: false
	});

		jQuery.ajax({
		  url: 'http://'+server+'/_app/class_api/busca-bairros.php',
		  type: 'POST',
		  data: {regiao: regiao},
		  success: function(data) {
		  	$('#cad_bairro_com').html(data);
		  	swal.close();
		  },
		});	
});
<?php /*autenticador*/ include('../../admin/autenticador.php'); ?>
<?php /*controlador*/ include('../../admin/controler_sys.php'); ?>
<!DOCTYPE html>
<html lang="pt-br">
   <head>
      <meta charset="utf-8">
      <meta name="robots" content="noindex, nofollow">
      <title><?php include('../../includes/title.php'); ?></title>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- FAV ICON -->
      <link rel="icon" type="image/png" href="http://<?= $server ?>/img/fav.png" />
      <!-- CSS -->
      <link rel="stylesheet" href="http://<?= $server ?>/css/bootstrap3.3.0.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/estilo.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/fontawesome.css">
      <!-- JAVASCRIPTS -->
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/bootstrap330.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.mask.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/sweet-alert.js"></script>
   </head>
   <style type="text/css">
      .cad_obs{
         display: none;
      }
   </style>
   <body>
      <div id="throbber" style="display:none; min-height:120px;"></div>
      <div id="noty-holder"></div>
      <div id="wrapper">
         <!-- Navigation -->
         <?php include('../../includes/menu.php') ?>
         <div id="page-wrapper">
            <div class="container-fluid">
               <form method="post" id="form-pdv">
                  <!-- Page Heading -->
                  <div class="row" id="main" >
                     <div class="col-md-12 well">
                        <div class="col-md-12">
                           <h3 class="rlk">Cadastro de PDV</h3>
                        </div>
                        <div class="col-md-12">
                           <div class="col-md-2">
                              <span>Marca</span><b style="color:red">*</b>
                              <select class="form-control" name="cad_marca" id="cad_marca">
                                 <option value="">Selecione</option>
                                 <?php
                                    // Lista as Regiões cadastradas
                                    $marca = $class->Select("id, marca", "marca_pdv", "", "ORDER BY marca ASC");
                                    while($row = $marca->fetch(PDO::FETCH_OBJ)){
                                       echo "<option value='".$row->id."'>".$row->marca."</option>";
                                    }
                                    ?>
                              </select>
                              <br>
                           </div>
                           <div class="col-md-3">
                              <span>Modelo</span><b style="color:red">*</b>
                              <select disabled class="form-control" name="cad_modelo" id="cad_modelo">
                                 <option value="">Selecione</option>
                              </select>
                              <br>
                           </div>
                           <div class="col-md-5">
                              <span>Sequência</span><b style="color:red">*</b>
                              <input name="cad_sequencia" disabled id="sequencia" class="form-control" type="text">
                              <small><b id="sequencia_bd"></b></small>
                              <br>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-12 well">
                        <div class="col-md-12">
                           <h3 class="rlk">Operador</h3>
                        </div>
                        <div class="col-md-12">
                           <div class="col-md-2 cl_operador">
                              <span>Matrícula</span><b style="color:red">*</b>
                              <input autocomplete="off" id="busca_matricula" class="form-control" type="text">
                              <br>
                           </div>
                           <div class="col-md-6 cl_operador">
                              <span>Nome do Operador</span><b style="color:red">*</b>
                              <input autocomplete="off" class="form-control" type="text" onkeyup="BuscaOperador()" onkeydown="verificaOperador()" id="busca_operador">
                              <br>
                           </div>

                           <div style="margin-top:20px; padding:0" class="col-md-9">
                              <!-- Input com o ID do operador -->
                              <input type="hidden" name="cad_matricula" id="matricula_operador">

                              <!-- Chip View -->
                              <span id="chipview-operador" class="chipview">
                                 <span id="txt_name_prepo2" class="txt_name_prepo"></span>
                                 <button onClick="closeChip2(this.value)" value="" type="button" id="close-chip2" class="close-chip">
                                    <b class="fa fa-close"></b>
                                 </button>
                              </span>


                              <!-- Auto Complete -->
                              <div style="display:none" id="lista_operador" class="history_list">
                                 <div class="history_item_name">         
                                    <li class="listagem">
                                    </li>
                                 </div>
                               </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-12 well">
                        <div class="col-md-12">
                           <h3 class="rlk">Proprietário <b style="color:red">*</b></h3>
                        </div>
                        <div class="col-md-12">
                           <div class="form-inline required">
                              <div class="col-md-3">
                                 <span>Cooperado</span><br
                                 <div class="form-group has-feedback">
                                    <label class="input-group">
                                       <span class="input-group-addon">
                                       <input class="tipo_prop" type="radio" name="cad_proprietario" value="cooperado" />
                                       </span>
                                       <div class="form-control form-control-static">
                                          Cooperado
                                       </div>
                                       <span class="glyphicon form-control-feedback "></span>
                                    </label>
                                 </div>
                              </div>
                              <div class="col-md-3">
                                 <span>Terceiro</span><br>
                                 <div class="form-group has-feedback ">
                                    <label class="input-group">
                                       <span class="input-group-addon">
                                       <input class="tipo_prop" type="radio" name="cad_proprietario" value="terceiro" />
                                       </span>
                                       <div class="form-control form-control-static">
                                          Terceiro 
                                       </div>
                                       <span class="glyphicon form-control-feedback "></span>
                                    </label>
                                 </div>
                              </div>
                           </div>
                           <div class="col-md-12">
                              <hr>
                           </div>
                           <div class="col-md-12 form_tipo_prop">                           
                              <!-- Include dinamico do tipo de proprietario -->
                           </div>
                        </div>
                        <div class="col-md-12 well">
                           <div class="col-md-12">
                              <h3 class="rlk">Status</h3>
                           </div>
                           <div class="col-md-12">
                              <div class="col-md-3">
                                 <span>Status</span><b style="color:red">*</b>
                                 <select id="cid_status" class="form-control cid_status" name="cid_status">
                                    <option value="">Selecione</option>
                                    <option value="Operando">Operando</option>
                                    <option value="Estoque">Estoque</option>
                                    <option value="Bloqueada">Bloqueada</option>
                                    <option value="Manuntenção">Manuntenção</option>
                                    <option value="Inutilizada">Inutilizada</option>
                                 </select>
                              </div>
                              <br><br>
                              <br><br>
                              <div class="col-md-12 cad_obs">
                                 <span>Obs:</span>
                                 <textarea id="cad_obs" style="min-width:100%; max-width:100%; min-height:150px; max-height:150px" name="cad_obs" class="form-control cad_obs"></textarea>
                              </div>
                              <div class="col-md-2 pull-right">
                                 <span>&nbsp;</span>
                                 <button id="cad_pdv" type="button" class="btn btn-warning cad_pdv">CADASTRAR</button>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>
      </div>
      <script type="text/javascript" src="http://<?= $server ?>/admin/_class/caminho_controler.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/menu-mobile.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/_app/_cadastros/js/cadastro-pdv.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/_app/_cadastros/js/busca-operador.js"></script>
   </body>
</html>
<?php 
include('../../controler.php');
?>

<!DOCTYPE html>
<html lang="pt-br">
<html>
<head>
	 <!-- CSS -->
       <link rel="stylesheet" href="http://<?= $server ?>/css/consulta-de-cad.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/bootstrap3.3.0.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/estilo.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/fontawesome.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/responsive.dataTables.min.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/jquery.dataTables.min.css">
      <!-- JAVASCRIPTS --> 


      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.js"></script>
     

      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.dataTables.min.js"></script>
        
      <script type="text/javascript" src="http://<?= $server ?>/js/dataTable.responsive.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/bootstrap330.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/sweet-alert.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.mask.js"></script>
</head>
<body>
<?php
// Caminho da chave de conexao ao banco
	

	if($_POST){
		

		
	// Divisao
	$min = ($_POST['inicio']);
	$dateTime = explode("/", $min);
	$data = $dateTime[2].'-'.$dateTime[1].'-'.$dateTime[0];
	$max = ($_POST['fim']);
	$dataTime = explode("/", $max);
	$date = $dataTime[2].'-'.$dataTime[1].'-'.$dataTime[0];

	

	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	// Busca as Unidades
	$sql = $pdo->prepare("SELECT * FROM repasse_cad WHERE data BETWEEN ? AND ?");
	$sql->bindValue(1,$data);
	$sql->bindValue(2,$date);
	$sql->execute();

 

	$output = '		  <div class="tab-pane active" id="tab1">
						<table id="example"class="display  nowrap" style="width:100%">
                              <thead>
                                 <tr>
                                    <th>Cooperado</th>
                                    <th>N° do IMEI</th>
                                    <th>Placa</th>
                                    <th>Data</th>
                                    <th>Autênticação</th>
                                    <th>Qtd</th>
                                    <th>Valor</th>
                                 </tr>
                              </thead>';
   
	
	while($row = $sql->fetch(PDO::FETCH_OBJ)){
    $format = new DateTime($row->data);
    $dt = $format->format('d/m/Y');
		
			$output .= '		<tbody>
                                 <tr>
                                    <td>'.$row->nome_cooperado.'</td>
                                    <td>'.$row->imei.'</td>
                                    <td>'.$row->placa.'</td>
                                    <td>'.$dt.'</td>
                                    <td>'.$row->auth.'</td>
                                    <td>'.$row->qtd.'</td>
                                    <td>'.$row->valor.'</td>
                                 </tr>
                              </tbody>';
              

		
                          
	}
	$output .= ' <tfoot>
                                 <th>Cooperado</th>
                                 <th>N° do IMEI</th>
                                 <th>Placa</th>
                                 <th>Data</th>
                                 <th>Autênticação</th>
                                 <th>Qtd</th>
                                 <th>Valor</th>
                              </tfoot>
                           </table></div>
                           ';
                            $saida = '<div class="tab-pane" id="tab2"><table id="example2" class="display nowrap" style="width:100%">
                              <thead>
                                 <tr>
                                    <th>Cooperado</th>
                                    <th>Nº do IMEI</th>
                                    <th>Período</th>
                                    <th>Qtd</th>
                                    <th>Valor</th>
                                 </tr>
                              </thead>';

 $sqlH = $pdo->prepare("SELECT * FROM historico_repasse_cad WHERE data BETWEEN ? AND ?");
  $sqlH->bindValue(1,$data);
  $sqlH->bindValue(2,$date);
  $sqlH->execute();

$total = 0;
while ($rowH = $sqlH->fetch(PDO::FETCH_OBJ)) {
 $format = new DateTime($rowH->data);
    $dt2 = $format->format('d/m/Y');
    $total += $rowH->valor;


 $saida .= '<tbody id="teste">
                                 <tr>
                                    <td id="nome0">'.$rowH->nome_cooperado.'</td>
                                    <td>'.$rowH->imei.'</td>
                                    <td>'.$dt2.'</td>
                                    <td>'.$rowH->qtd.'</td>
                                    <td>'.$rowH->valor.'</td>
                                 </tr>
                                 <?php
                                    }
                                    ?>
                              </tbody>';               



}



	$saida .='<tfoot>
                                 <tr>
                                    <th>-</th>
                                    <th>-</th>
                                    <th>-</th>
                                    <th>Total:</th>
                                    <th style="color: #00ab56;">R$:'.$total.'</th>
                                 </tr>
                              </tfoot></div>';
	
	echo ($output);
	echo ($saida);

	

}

	

	

?>
 <script type="text/javascript">
         $(function(){
             $('[data-toggle="tooltip"]').tooltip();
             $(".side-nav .collapse").on("hide.bs.collapse", function() {                   
                 $(this).prev().find(".fa").eq(1).removeClass("fa-angle-right").addClass("fa-angle-down");
             });
             $('.side-nav .collapse').on("show.bs.collapse", function() {                        
                 $(this).prev().find(".fa").eq(1).removeClass("fa-angle-down").addClass("fa-angle-right");        
             });
         })   
         $(document).ready(function(){
            $('.data').mask('00/00/0000');
           });  
          $(document).ready(function() {
           var table = $('#example').DataTable( {
           rowReorder: {
              selector: 'td:nth-child(2)'
           },
           "destroy": true,
           responsive: true,
           
            "language": {
               "url": "https://cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
            }
           });

           var table2 = $('#example2').DataTable( {
           rowReorder: {
              selector: 'td:nth-child(2)'
           },
           "destroy": true,
           responsive: true,
           
            "language": {
               "url": "https://cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
            }
           });
         });
         
         
         
         
         
        
      </script>
</body>
</html>
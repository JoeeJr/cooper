<?php 
	// Caminho da chave de conexao ao banco
	include('../../controler.php');

	
	// Se recebeu os POSTS
	if ($_POST){
			$pdv_edit_operador = strtoupper($_POST['pdv_edit_operador']);
			$pdv_edit_matricula = $_POST['pdv_edit_matricula'];
			$pdv_edit_status = $_POST['pdv_edit_status'];
			$id = $_POST['id_pdv'];

			// Verifica se nao tem um numero de serie igual ja cadastrado
			$sqlV = $pdo->prepare("SELECT id_pdv FROM cadastro_pdv WHERE mmatricula_cooperado = :operador AND matricula_proprietario = :matricula AND sstatus_pdv = :status");
			$sqlV->bindValue(":operador", $pdv_edit_operador);
			$sqlV->bindValue(":matricula", $pdv_edit_matricula);
			$sqlV->bindValue(":status", $pdv_edit_status);
			
			$sqlV->execute();
			// registros encontrados
			$totalV = $sqlV->rowCount();

			if ($totalV > 0){
				// Atualiza Pdv
			$sql = $pdo->prepare("UPDATE cadastro_pdv SET matricula_cooperado = :operador, matricula_proprietario = :matricula, status_pdv = :status WHERE id_pdv = :id");
			$sql->bindValue(":operador", $pdv_edit_operador);
			$sql->bindValue(":matricula", $pdv_edit_matricula);
			$sql->bindValue(":status", $pdv_edit_status);
			$sql->bindValue(":id", $id);
			$sql->execute();

			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$sql = $pdo->prepare("UPDATE historico_busca_pdv SET operador = :operador, proprietario = :matricula, status = :status WHERE id_pdv = :id");
			$sql->bindValue(":operador", $pdv_edit_operador);
			$sql->bindValue(":matricula", $pdv_edit_matricula);
			$sql->bindValue(":status", $pdv_edit_status);
			$sql->bindValue(":id", $id);
			$sql->execute();
			
	
			}else{
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			// Atualiza Pdv
			$sql = $pdo->prepare("UPDATE cadastro_pdv SET matricula_cooperado = :operador, matricula_proprietario = :matricula, status_pdv = :status WHERE id_pdv = :id");
			$sql->bindValue(":operador", $pdv_edit_operador);
			$sql->bindValue(":matricula", $pdv_edit_matricula);
			$sql->bindValue(":status", $pdv_edit_status);
			$sql->bindValue(":id", $id);
			$sql->execute();

			
				$sql = $pdo->prepare("INSERT INTO historico_busca_pdv ( operador, proprietario, status, id_pdv)VALUES (:operador,															:matricula,															:status,
					:id
															 )");

			$sql->bindValue(":operador", $pdv_edit_operador);
			$sql->bindValue(":matricula", $pdv_edit_matricula);
			$sql->bindValue(":status", $pdv_edit_status);
			$sql->bindValue(":id", $id);
			$sql->execute();
				}
		echo "ok";

	
	}else{
		echo "error";
		header("http://$server/");
	}

	
?>
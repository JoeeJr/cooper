 <?php /*autenticador*/ include('../../admin/autenticador.php'); ?>
<?php /*controlador*/ include('../../admin/controler_sys.php'); ?>
<html lang="pt-br">
   <head>
      <meta charset="utf-8">
      <meta name="robots" content="noindex, nofollow">
      <title><?php include('../../includes/title.php'); ?></title>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      
      <!-- FAV ICON -->
      <link rel="icon" type="image/png" href="http://<?= $server ?>/img/fav.png" />

      <!-- CSS -->
      <link rel="stylesheet" href="http://<?= $server ?>/css/bootstrap3.3.0.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/estilo.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/fontawesome.css">
      <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.1/css/responsive.dataTables.min.css">
      <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
      
      <!-- JAVASCRIPTS -->
       <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
      <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js" ></script>
      <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js" ></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/bootstrap330.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.mask.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/sweet-alert.js"></script>
   </head>
   <body>

     <?php
        // Dados do Cooperados
       
        $id = $_GET['pdv'];
        $dados = $class->Select("id_pdv, serie, marca, modelo, matricula_cooperado, proprietario_pdv, matricula_proprietario, status_pdv", "cadastro_pdv", "WHERE id_pdv = '$id'", "");
        $row = $dados->fetch(PDO::FETCH_OBJ);

        // id do copperado
        $id_cooperado = $row->matricula_cooperado;
        $id_proprietario = $row->matricula_proprietario;

       if($row->proprietario_pdv == 'TERCEIRO'){
       $busca_nome_prop = $class->SelectEsp("nome","proprietarios_pdv","WHERE id_prop = '$id_proprietario'");
        }else{
          $busca_nome_prop = $class->SelectEsp("nome","cooperados","WHERE id = '$id_proprietario'");
        }
        if(strlen($busca_nome_prop) > 23){
          $nome_proprietario = substr($busca_nome_prop, 0, 23)."...";
        }else{
         $nome_proprietario  = $busca_nome_prop;
        }
        
        
        $teste = $class->Select("*","marca_pdv","","");
        $testeMarca = $teste->fetch(PDO::FETCH_OBJ);
        if($testeMarca->id = $row->marca ){
          $marca = $class->SelectEsp("marca","marca_pdv","WHERE id = '$row->marca'");
        }
        $teste2 = $class->Select("*","modelo_pdv","","");
        $testeModelo = $teste->fetch(PDO::FETCH_OBJ);
        if($testeModelo->id = $row->modelo ){
          $modelo = $class->SelectEsp("modelo","modelo_pdv","WHERE id = '$row->modelo'");
        }
     
       
        ?>

    <style>

label {
    width: auto;
}
.btn-warning {
    color: #fff;
    background-color: #949494;
    border-color: #929292;
    width: 100%;
    padding: 1rem 1rem;
    margin-top: 2.1rem;
}
    </style>

      <div id="throbber" style="display:none; min-height:120px;"></div>
      <div id="noty-holder"></div>
      <div id="wrapper">
         <!-- Navigation -->
        <?php include('../../includes/menu.php') ?>
             <form id="FormEditPdv" method="POST">
              <div id="page-wrapper"> 


            <div class="container-fluid">
               <!-- Page Heading -->
               
                  <input type="hidden" name="id_pdv" id="id_pdv" value="<?= $row->id_pdv; ?>">
               <div class="row" id="main" >
               
                <div class="col-md-12 well">
                     <div style="margin-bottom: 1rem;" class="col-md-12">
                        <h3  onclick="newDoc()" class="rlk">Dados do PDV</h3>
                     </div>
                     <div class="col-md-12">
                      <div class="col-md-4"><b>Nº de serie: <br></b> <!-- Chip View -->
                           <span style="display: block; background-color: #f3f3f3" id="chipview-serie" class="chipview">
                              <span id="select_serie" style="color: black" class="select_serie"><?= $class->SelectEsp("serie","cadastro_pdv","WHERE id_pdv = '$id'") ?></span>
                           </span> <br><br></div>
                      <div class="col-md-4"><b>Marca: <br></b> <span style="display: block; background-color: #f3f3f3" id="chipview-marca" class="chipview">
                              <span id="select_marca" style="color:black" class="select_marca"><?= $marca ?></span>
                           </span><br><br></div>
                      <div class="col-md-4"><b>Modelo: <br></b> <span style="display: block; background-color: #f3f3f3" id="chipview-modelo" class="chipview">
                              <span id="select_modelo" style="color:black" class="select_modelo"><?= $modelo ?></span></span> <br><br></div></div>

                      <div class="col-md-12">
                      <div class="col-md-4"><b>Operador: <br></b>
                      <input value="<?= $id_cooperado ?>" style="display: none" placeholder="Ex : João Ferreira" class="form-control txt_cooperado cad_cooperado sec_pdv" type="text" onkeyup="BuscaPdv()" onkeydown="verificaPdv()" id="busca_pdv" >
                      <input class="cadastro_preponente sec_pdv" type="hidden" value="<?= $id_cooperado  ?>" name="pdv_edit_operador" id="sec_pdv">
                                   
                      <!-- Chip View -->
                           <span style="display: block" id="chipview-editapdv" class="chipview">
                              <span id="txt_name_pdv2" style="color:white" class="txt_name_operador"><?= $class->SelectEsp("nome","cooperados","WHERE id = '$id_cooperado'") ?></span>
                              <button onClick="closeChip3(this.value)" value="" type="button" id="close-chip3" class="close-chip">
                                 <b class="fa fa-close"></b>
                              </button>
                           </span>
                          
                             <!-- Auto Complete -->
                           <div style="display:none" id="lista_pdv" class="history_list">
                              <div class="history_item_name">         
                                 <li class="listagem">
                                 </li>
                              </div>
                            </div>
                          

                         </div>

                           
                          
                          
                      <div class="col-md-4"><b>Proprietário: <br></b>  
                        
                         <input value="<?= $id_proprietario ?>" style="display: none" placeholder="Ex : João Ferreira" class="form-control txt_cooperado cad_cooperado sec_pdv" type="text" onkeyup="BuscaPdvProp()" onkeydown="verificaPdvProp()" id="busca_pdv_prop" >
                        <input id="pdv_edit_matricula" value="<?= $id_proprietario  ?>" name="pdv_edit_matricula"  class="form-control" type="hidden">
                        <!-- Chip View -->
                           <span style="display: block" id="chipview-editapdv-prop" class="chipview">
                              <span id="txt_name_prop" style="color:white"class="txt_name_cooperado"><?= utf8_encode($nome_proprietario) ?></span>
                              <button onClick="closeChipProp(this.value)" value="" type="button" id="close-chip-prop" class="close-chip">
                                 <b class="fa fa-close"></b>
                              </button>
                           </span>
                             <!-- Auto Complete -->
                           <div style="display:none" id="lista_pdv_prop" class="history_list">
                              <div class="history_item_name">         
                                 <li class="listagem">
                                 </li>
                              </div>
                            </div>
                           <br><br></div>
                          
                    <div class="col-md-4">
                              <span>Estatus Atual:</span><b style="color:red">*</b>
                              <select class="form-control cad_status" id="pdv_edit_status"name="pdv_edit_status">
                                 <option value="">Selecione</option>
                                 <option <?= ($row->status_pdv == "Operando") ? "selected" : "" ?> value="Operando">Operando</option>
                                 <option <?= ($row->status_pdv == "Estoque") ? "selected" : "" ?> value="Estoque">Estoque</option>
                                 <option <?= ($row->status_pdv == "Bloqueada") ? "selected" : "" ?> value="Bloqueada">Bloqueada</option>
                                 <option <?= ($row->status_pdv == "Manutenção") ? "selected" : "" ?> value="Manutenção">Manutenção</option>
                              </select>
                    </div> </div>

                     <div class="pull-right col-md-4">
                             <button type="button" style="background-color: #04723b;" onclick="EditPDV()" class="btn btn-warning">Atualizar PDV</button>
                        </div>
                      </form>

                </div>
               
                  <div class="col-md-12 well">
                     <div class="col-md-12">
                        <h3  class="rlk">Historico</h3>
                     </div>


                     <div class="col-md-12">

                        <div class="col-md-12">
                                <table id="example" class="display nowrap" style="width:100%">
        <thead>
            <tr>
                <th>Operador </th>
                <th>Proprietário </th>
                <th>Status </th>
                <th>Data</th>
            </tr>
        </thead>
        <tbody>
            <?php
                //dados historico
                $mostrar = $class->select("*", "historico_busca_pdv", "WHERE id_pdv = '$id' ","");

                
                  while($rowH = $mostrar->fetch(PDO::FETCH_OBJ)){
                    $matricula_cooperado = $rowH->operador;
                    $matricula_proprietario = $rowH->proprietario;
                  $pdv = $class->SelectEsp("nome","cooperados","WHERE id = '$matricula_cooperado'");//checa proprietario pdv se e cooperado]
                   if($row->proprietario_pdv == 'COOPERADO'){
                  
                  
                    $prop = $class->SelectEsp("nome","cooperados","WHERE id= '$matricula_proprietario'");

                    //se for terceiro executa o else
                   }else{
                    $prop = $class->SelectEsp("nome","proprietarios_pdv","WHERE id_prop = '$matricula_proprietario'");
                  }

                    $stats = $rowH->status;
                    
                    $format = new DateTime($rowH->data_op);
                    $data = $format->format('d/m/Y');  
                     

                    
            ?>

                <tr>
                    <td><?= $pdv ?></td>
                    <td><?= $prop ?></td>
                    <td><?= $stats ?></td>
                    <td><?= $data ?></td>
                </tr>

            <?php
                }
            ?>
        </tbody>
        <tfoot>
            <tr>
                <th>Operador </th>
                <th>Proprietário</th>
                <th>Status</th>
                <th>Data</th>
            </tr>
        </tfoot>
    </table>
                        </div>

                        <br><br><br><br><br><br><br><br><br><br><br>

                        <div class="col-md-4">
                             <button onclick="window.history.go(-1); return false;" class="btn btn-warning">Voltar</button>
                        </div>

                     </div>
                     
                  </div>

                

               </div>
            </div>
         </div>
      </div>
      </div>
      <script type="text/javascript" src="http://<?= $server ?>/admin/_class/caminho_controler.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/_app/_consulta/js/editar-pdv.js"></script>
       <script type="text/javascript" src="http://<?= $server ?>/_app/_consulta/js/view-editar-pdv.js"></script>
         <script type="text/javascript" src="http://<?= $server ?>/_app/_consulta/js/view-editar-prop-pdv.js"></script>

      <script type="text/javascript">
         $(function(){
             $('[data-toggle="tooltip"]').tooltip();
             $(".side-nav .collapse").on("hide.bs.collapse", function() {                   
                 $(this).prev().find(".fa").eq(1).removeClass("fa-angle-right").addClass("fa-angle-down");
             });
             $('.side-nav .collapse').on("rowH.bs.collapse", function() {                        
                 $(this).prev().find(".fa").eq(1).removeClass("fa-angle-down").addClass("fa-angle-right");        
             });
         })    

$(document).ready(function() {
    var table = $('#example').DataTable( {
        rowReorder: {
            selector: 'td:nth-child(2)'
        },
        responsive: true
    } );
} );
      </script>
   </body>
</html>
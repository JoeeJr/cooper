<?php /*autenticador*/ include('../../admin/autenticador.php'); ?>
<?php /*controlador*/ include('../../admin/controler_sys.php'); ?>
<!DOCTYPE html>
<html lang="pt-br">
   <head>
      <meta charset="utf-8">
      <meta name="robots" content="noindex, nofollow">
      <title><?php include('../../includes/title.php'); ?></title>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- FAV ICON -->
      <link rel="icon" type="image/png" href="http://<?= $server ?>/img/fav.png" />
      <!-- CSS -->
      <link rel="stylesheet" href="http://<?= $server ?>/css/bootstrap3.3.0.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/estilo.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/fontawesome.css">
      <!-- JAVASCRIPTS -->
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/bootstrap330.js"></script>
   </head>
   <style type="text/css">
      @page { size: auto;  margin: 0mm; }
   </style>
   <body>
      <script type="text/javascript">
         $(document).ready(function () {
         window.print();
         });
         
      </script>
      <?php 
         //  id do termo e do cooperado
         $id_termo = $_GET['termo'];
         $id_coop = $_GET['coop'];
         
         // Selecao dos dados de termo e cooperado
         $testando = $class->Select("*","termos","WHERE id = '$id_termo'","");
         $testando2 = $class->Select("*","cooperados","WHERE id = '$id_coop'","");
         
         // Array que traz as informacoes do termo
         $show = $testando->fetch(PDO::FETCH_OBJ);
         
         // Array que traz as informacoes do cooperado
         $showCoop = $testando2->fetch(PDO::FETCH_OBJ);
         
         // Data de nascimento formatada
         $format = new DateTime($showCoop->nascimento);
         $nascimento = $format->format('d/m/Y');               
               
         ?>
      <div class="col-md-12 cabecalho">
         <h2><?= $show->titulo ?></h2>
      </div>
      <div class="col-md-12">
         <div class="col-md-12">

            <?php
               if ($show->cooperado == '1'){
            ?>
            <table id="example" class="display nowrap" style="width:100%">
               <thead>
                  <tr>
                     <th>Nome</th>
                     <th>CPF</th>
                     <th>telefone</th>
                     <th>RG</th>
                     <th>Emissor</th>
                     <th>Data de Nascimento</th>
                  </tr>
               </thead>
               <tbody>
                  <tr>
                     <td><?= $showCoop->nome ?></td>
                     <td><?= utf8_encode($showCoop->cpf) ?></td>
                     <td><?= utf8_encode($showCoop->tel_fixo) ?></td>
                     <td><?= utf8_encode($showCoop->rg) ?></td>
                     <td><?= $showCoop->emissor ?></td>
                     <td><?= $nascimento ?></td>
                  </tr>
               </tbody>
            </table>

            <?php
               }
            ?>


            <?php
               if($show->preposto == '1' && $showCoop->preposto != ''){
                    
               $testando3 = $class->Select("*","cooperados","WHERE id = '$showCoop->preposto'","");
               $showPrep = $testando3->fetch(PDO::FETCH_OBJ);
               $format2 = new DateTime($showPrep->nascimento);
               $nascimentoPrep = $format2->format('d/m/Y');
               
            ?>
            <hr>
            <table id="example2" class="display nowrap" style="width:100%;" >
               <thead>
                  <tr>
                     <th>Nome</th>
                     <th>CPF</th>
                     <th>telefone</th>
                     <th>RG</th>
                     <th>Emissor</th>
                     <th>Data de Nascimento</th>
                  </tr>
               </thead>
               <tbody>
                  <tr  >
                     <td><?= $showPrep->nome ?></td>
                     <td><?= utf8_encode($showPrep->cpf) ?></td>
                     <td><?= utf8_encode($showPrep->tel_fixo) ?></td>
                     <td><?= utf8_encode($showPrep->rg) ?></td>
                     <td><?= $showPrep->emissor ?></td>
                     <td><?= $nascimentoPrep ?></td>
                  </tr>
               </tbody>
            </table>
            <?php 
               }
               ?>
         </div>
         <h3><?= $show->termo ?></h3>
      </div>
      <script type="text/javascript" src="http://<?= $server ?>/admin/_class/caminho_controler.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/menu-mobile.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/_app/_consulta/js/consulta-cooperado.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/_app/_consulta/js/detalhe-consulta-cooperado.js"></script>
   </body>
</html>
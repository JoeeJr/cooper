<?php /*autenticador*/ include('../../admin/autenticador.php'); ?>
<?php include('../../admin/controler_sys.php'); ?>
<!DOCTYPE html>
<html lang="pt-br">
   <head>
      <meta charset="utf-8">
      <meta name="robots" content="noindex, nofollow">
      <title><?php include('../../includes/title.php'); ?></title>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- FAV ICON -->
      <link rel="icon" type="image/png" href="http://<?= $server ?>/img/fav.png" />
      <!-- CSS -->
      <link rel="stylesheet" href="http://<?= $server ?>/css/bootstrap3.3.0.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/estilo.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/fontawesome.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/responsive.dataTables.min.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/jquery.dataTables.min.css">
      <!-- JAVASCRIPTS -->
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.dataTables.min.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/dataTable.responsive.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/bootstrap330.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/_app/_consulta/js/view-editar-pdv.js"></script>

   </head>
   <body>

      
    <style>
      label {
        width: auto;
}
    </style>

      <div id="throbber" style="display:none; min-height:120px;"></div>
      <div id="noty-holder"></div>
      <div id="wrapper">
         <!-- Navigation -->
        <?php include('../../includes/menu.php') ?>
         <div id="page-wrapper">
            <div class="container-fluid">
               <!-- Page Heading -->
               <div class="row" id="main" >
               
                  <div class="col-md-12 well">
                     <div class="col-md-12">
                        <h3 class="rlk">DADOS DO PDV</h3>
                     </div>

                      <div style="margin-bottom: 3rem" class="col-md-2">
                            <a href="cadastro-pdv/"> <button class="btn btn-warning">CADASTRAR PDV &nbsp <i class="fas fa-plus-square"></i></button></a>
                        </div>

                     <div class="col-md-12">

                        <div class="col-md-12">
        <table id="example" class="display nowrap" style="width:100%">



        <thead>
            <tr>
                <th>Nº de serie</th>
                <th>Marca</th>
                <th>Modelo</th>
                <th>Operador</th>
                <th>proprietário</th>
                <th>-</th>
            </tr>
        </thead>
        <tbody>
            <?php

                // Dados do Cooperados
                 $dados = $class->Select("*", "cadastro_pdv", "", "");
                 $teste = $class->Select("*","marca_pdv","","");
                 $testeMarca = $teste->fetch(PDO::FETCH_OBJ);
                
                $teste2 = $class->Select("*","modelo_pdv","","");
                $testeModelo = $teste->fetch(PDO::FETCH_OBJ);
                

                 
                while( $row = $dados->fetch(PDO::FETCH_OBJ)){
                  //checa se proprietario pdv e cooperado ou terceiro
                  if($row->proprietario_pdv == 'COOPERADO'){
                  $matricula_cooperado = $row->matricula_cooperado;
                  $pdv = $class->SelectEsp("nome","cooperados","WHERE id = '$matricula_cooperado'");
                  $matricula_proprietario = $row->matricula_proprietario;
                  $prop = $class->SelectEsp("nome","cooperados","WHERE id= '$matricula_proprietario'");
                  //se for terceiro executa o else
                 }else{
                   $matricula_cooperado = $row->matricula_cooperado;
                  $pdv = $class->SelectEsp("nome","cooperados","WHERE id = '$matricula_cooperado'");
                  $matricula_proprietario = $row->matricula_proprietario;
                  $prop = $class->SelectEsp("nome","proprietarios_pdv","WHERE id_prop = '$matricula_proprietario'");
                }
                if($testeMarca->id = $row->marca ){
                $marca = $class->SelectEsp("marca","marca_pdv","WHERE id = '$row->marca'");
                }
                if($testeModelo->id = $row->modelo ){
                $modelo = $class->SelectEsp("modelo","modelo_pdv","WHERE id = '$row->modelo'");
                }


            ?>

                <tr>
                    <td><?= $row->serie ?></td>
                    <td><?= $marca ?></td>
                    <td><?= $modelo ?></td>
                    <td><?= $pdv ?></td>
                    <td><?= $prop ?></td>
                    <td><div onclick="lupz(<?= $row->id_pdv ?>)" class="lupa"></div></td>
                </tr>

            <?php
               }
            ?>
            
        </tbody>
    </table>
                        </div>

                        <br><br><br><br><br><br><br><br><br><br><br>


                     </div>
                     
                  </div>

                

               </div>
            </div>
         </div>
      </div>
    </div>
       <script type="text/javascript" src="http://<?= $server ?>/admin/_class/caminho_controler.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/menu-mobile.js"></script>
      <script type="text/javascript">        
         $(document).ready(function() {
           var table = $('#example').DataTable( {
           rowReorder: {
              selector: 'td:nth-child(2)'
           },
           responsive: true,
            "language": {
               "url": "https://cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
            }
           });
         });
      </script>
      <script type="text/javascript">
        function lupz(id){
          window.location.href = 'detalhe-consulta-de-pdv/'+id;
        }
        </script>
   </body>
</html>
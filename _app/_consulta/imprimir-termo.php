<?php /*autenticador*/ include('../../admin/autenticador.php'); ?>
<?php /*controlador*/ include('../../admin/controler_sys.php'); ?>
<!DOCTYPE html>
<html lang="pt-br">
   <head>
      <meta charset="utf-8">
      <meta name="robots" content="noindex, nofollow">
      <title><?php include('../../includes/title.php'); ?></title>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- FAV ICON -->
      <link rel="icon" type="image/png" href="http://<?= $server ?>/img/fav.png" />
      <!-- CSS -->
      <link rel="stylesheet" href="http://<?= $server ?>/css/bootstrap3.3.0.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/estilo.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/fontawesome.css">
      <!-- <link rel="stylesheet" href="http://<?= $server ?>/css/responsive.dataTables.min.css"> -->
      <!-- <link rel="stylesheet" href="http://<?= $server ?>/css/jquery.dataTables.min.css"> -->
      <!-- JAVASCRIPTS -->
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.js"></script>
      <!-- <script type="text/javascript" src="http://<?= $server ?>/js/jquery.dataTables.min.js"></script> -->
      <!-- <script type="text/javascript" src="http://<?= $server ?>/js/dataTable.responsive.js"></script> -->
      <script type="text/javascript" src="http://<?= $server ?>/js/bootstrap330.js"></script>
   </head>
   <body>
      <?php 
         $id = $_GET['termo'];
         $row = $class->Select("*","termos","WHERE id = '$id'");
         $termos = $row->fetch(PDO::FETCH_OBJ);

      ?>
      <style>
         label {
         width: auto;
         }
      </style>
      <div id="throbber" style="display:none; min-height:120px;"></div>
      <div id="noty-holder"></div>
      <div id="wrapper">
         <!-- Navigation -->
         <?php include('../../includes/menu.php') ?>
         <div id="page-wrapper">
            <div class="container-fluid">
               <!-- Page Heading -->
               <div class="row" id="main" >
                  <div class="col-md-12 well">
                     <div class="col-md-12">
                        <h3 class="rlk">IMPRIMIR TERMOS</h3>
                     </div>
                     <div class="col-md-12">
                        <div class="col-md-12">
                           <?= $termos->titulo  ?>
                                 
                          
                        </div>
                        <?= $termos->termo ?>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      </div>
      <script type="text/javascript" src="http://<?= $server ?>/admin/_class/caminho_controler.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/menu-mobile.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/_app/_consulta/js/consulta-cooperado.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/admin/_class/caminho_controler.js"></script>
      <!-- <script type="text/javascript">        
         $(document).ready(function() {
           var table = $('#example').DataTable( {
           rowReorder: {
              selector: 'td:nth-child(2)'
           },
           responsive: true,
            "language": {
               "url": "https://cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
            }
           });
         });
      </script> -->
   </body>
</html>
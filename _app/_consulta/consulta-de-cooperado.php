<?php /*autenticador*/ include('../../admin/autenticador.php'); ?>
<?php /*controlador*/ include('../../admin/controler_sys.php'); ?>
<!DOCTYPE html>
<html lang="pt-br">
   <head>
      <meta charset="utf-8">
      <meta name="robots" content="noindex, nofollow">
      <title><?php include('../../includes/title.php'); ?></title>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- FAV ICON -->
      <link rel="icon" type="image/png" href="http://<?= $server ?>/img/fav.png" />
      <!-- CSS -->
      <link rel="stylesheet" href="http://<?= $server ?>/css/bootstrap3.3.0.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/estilo.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/fontawesome.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/responsive.dataTables.min.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/jquery.dataTables.min.css">
      <!-- JAVASCRIPTS -->
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.dataTables.min.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/dataTable.responsive.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/bootstrap330.js"></script>
   </head>
   <body>
      <style>
         label {
         width: auto;
         }
      </style>
      <div id="throbber" style="display:none; min-height:120px;"></div>
      <div id="noty-holder"></div>
      <div id="wrapper">
         <!-- Navigation -->
         <?php include('../../includes/menu.php') ?>
         <div id="page-wrapper">
            <div class="container-fluid">
               <!-- Page Heading -->
               <div class="row" id="main" >
                  <div class="col-md-12 well">
                     <div class="col-md-12">
                        <h3 class="rlk">CONSULTA DE COOPERADO</h3>
                     </div>
                     <div class="col-md-12">
                        <div class="col-md-12">
                           <table id="example" class="display nowrap" style="width:100%">
                              <thead>
                                 <tr>
                                    <th>Matricula</th>
                                    <th>Nome</th>
                                    <th>Bairro</th>
                                    <th>Rua</th>
                                    <th>Status</th>
                                    <th>#</th>
                                 </tr>
                              </thead>
                              <tbody>
                                <?php
                                  $coop = $class->Select("id, nome, bairro, endereco, status", "cooperados", "", "");

                                  while($row = $coop->fetch(PDO::FETCH_OBJ)){
                                    if($row->status == 1){
                                      $teste = "Ativo";
                                    }else{
                                      $teste = "Desativado";
                                    }
                                ?>
                                 <tr>
                                    <td><?= $row->id ?></td>
                                    <td><?= utf8_encode($row->nome) ?></td>
                                    <td><?= utf8_encode($row->bairro) ?></td>
                                    <td><?= utf8_encode($row->endereco) ?></td>
                                    <td><?= $teste ?></td>
                                    <td>
                                       <div onclick="viewCooperado(<?= $row->id ?>)" class="lupa"></div>
                                    </td>
                                 </tr>
                                <?php
                                  }
                                ?>
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      </div>
      <script type="text/javascript" src="http://<?= $server ?>/admin/_class/caminho_controler.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/menu-mobile.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/_app/_consulta/js/consulta-cooperado.js"></script>
      <script type="text/javascript">        
         $(document).ready(function() {
           var table = $('#example').DataTable( {
           rowReorder: {
              selector: 'td:nth-child(2)'
           },
           responsive: true,
            "language": {
               "url": "https://cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
            }
           });
         });
      </script>
   </body>
</html>
<?php /*autenticador*/ include('../../admin/autenticador.php'); ?>
<?php /*controlador*/ include('../../admin/controler_sys.php'); ?>
<!DOCTYPE html>
<html lang="pt-br">
   <head>
      <meta charset="utf-8">
      <meta name="robots" content="noindex, nofollow">
      <title><?php include('../../includes/title.php'); ?></title>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- FAV ICON -->
      <link rel="icon" type="image/png" href="http://<?= $server ?>/img/fav.png" />
      <!-- CSS -->
       <link rel="stylesheet" href="http://<?= $server ?>/css/consulta-de-cad.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/bootstrap3.3.0.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/estilo.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/fontawesome.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/responsive.dataTables.min.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/jquery.dataTables.min.css">
      <!-- JAVASCRIPTS --> 


      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.js"></script>
     

      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.dataTables.min.js"></script>
        
      <script type="text/javascript" src="http://<?= $server ?>/js/dataTable.responsive.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/bootstrap330.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/sweet-alert.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.mask.js"></script>
   </head>
   <body>
      <?php
        // Dados do Cooperados
       
        $id = $_GET['coop'];
        $dados = $class->Select("*", "cooperados", "WHERE id = '$id'", "");
        $row = $dados->fetch(PDO::FETCH_OBJ);

        

        // Data de nascimento formatada
        $format = new DateTime($row->nascimento);
        $nascimento = $format->format('d/m/Y');


      ?>
      <style>
         label {
         width: auto;
         }
         .modal-header {
         min-height: 16.43px;
         padding: 15px;
         font-family: 'roboto-bold';
         background: #900505;
         color: white;
         border-bottom: 1px solid #e5e5e5;
         letter-spacing: 4px;
         }
         .modal-content {
         height: -webkit-fill-available;
         }



         @media (min-width: 992px){
         .col-md-6 {
         width: 50%;
         margin-top: 5%;
         }
         }
      </style>

      <!-- css modal coop -->

      <style>
         label {
         width: auto;
         }
         .modal-header-coop {
         min-height: 16.43px;
         padding: 15px;
         font-family: 'roboto-bold';
         background: #900505;
         color: white;
         border-bottom: 1px solid #e5e5e5;
         letter-spacing: 4px;
         }
         .modal-content-coop {
         /*height: -webkit-fill-available;*/
         position: relative;
         background-color: #fff;
         background-clip: padding-box;
         border: 1px solid rgba(0,0,0,.2);
         border-radius: 6px;
         outline: 0;
         }



         @media (min-width: 992px){
         .col-md-6 {
         width: 50%;
         margin-top: 5%;
         }
         }
      </style>

      <!-- CSS Modal TERMOS -->
       <style>
         label {
         width: auto;
         }
         .modal-header-termos {
         min-height: 16.43px;
         padding: 15px;
         font-family: 'roboto-bold';
         background: #900505;
         color: white;
         border-bottom: 1px solid #e5e5e5;
         letter-spacing: 4px;
         }
         .modal-content-termos {
         /*height: -webkit-fill-available;*/
         position: relative;
         background-color: #fff;
         background-clip: padding-box;
         border: 1px solid rgba(0,0,0,.2);
         border-radius: 6px;
         outline: 0;
         }



         @media (min-width: 992px){
         .col-md-6 {
         width: 50%;
         margin-top: 5%;
         }
         }
      </style>


   
      
      <!-- MODAL DE DADOS DO COOPERADO -->

      <!-- Button trigger modal -->


<!-- Button trigger modal -->

<!-- Modal -->

      
      

      <div class="modal fade" id="modalCoop" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
         <div class="modal-dialog">
            <div class="modal-content-coop">
               <div class="modal-header-coop">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                  <h3><img style="  width: 4rem; margin-right: 2rem; margin-left: 2rem;" src="http://<?= $server ?>/img/icones/term.svg" alt=""> DADOS DO COOPERADO</h3>
               </div>
               <form id="FormCoop" method="POST">
                <input type="hidden" name="id_coop" value="<?= $row->id; ?>">
                <div class="modal-body">
                  

                  
                  <div class="col-md-8">
                    <label>Nome do Cooperado</label>
                    <input name="coop_edit_nome" value="<?= utf8_decode($row->nome); ?>" class="form-control" type="text" id="coop_edit_nome">
                  </div> 

                  <div class="col-md-4">
                    <label>Nascimento</label>
                    <input name="coop_edit_nascimento" value="<?= $row->nascimento; ?>"class="form-control" type="date" id="coop_edit_nascimento">
                  </div>

                  <div class="col-md-6">
                        <span>RG</span><b style="color:red">*</b>
                        <input name="coop_edit_rg" value="<?= $row->rg; ?>" placeholder="00.000.000-0"  class="form-control cad_rg" id="coop_edit_rg" name="cad_rg" type="text">
                   </div>

                   <div class="col-md-6">
                        <span>Emissor</span><b style="color:red">*</b>
                        <input name="coop_edit_emissor" value="<?= $row->emissor; ?>" placeholder="SSP" class="form-control cad_emissor" name="cad_emissor" id="coop_edit_emissor" type="text">
                   </div>

                   <div class="col-md-6">
                              <span>CPF</span><b style="color:red">*</b>
                              <input id="coop_edit_cpf" name="coop_edit_cpf" value="<?= $row->cpf; ?>"  placeholder="000.000.000-00" class="form-control cad_cpf"  type="text">
                   </div>

                 

                    

                    
                    <div class="col-md-6">
                              <span>Estado Civil</span><b style="color:red">*</b>
                              <select class="form-control cad_estado_civil" id="coop_edit_estadocivil"name="coop_edit_estadocivil">
                                 <option value="">Selecione</option>
                                 <option <?= ($row->estado_civil == "Solteiro(a)") ? "selected" : "" ?> value="Solteiro(a)">Solteiro(a)</option>
                                 <option <?= ($row->estado_civil == "Casado(a)") ? "selected" : "" ?> value="Casado(a)">Casado(a)</option>
                                 <option <?= ($row->estado_civil == "Separado(a)") ? "selected" : "" ?> value="Separado(a)">Separado(a)</option>
                                 <option <?= ($row->estado_civil == "Divorciado(a)") ? "selected" : "" ?> value="Divorciado(a)">Divorciado(a)</option>
                                 <option <?= ($row->estado_civil == "Viúvo(a)") ? "selected" : "" ?> value="Viúvo(a)">Viúvo(a)</option>
                                 <option <?= ($row->estado_civil == "Uniâo Estável") ? "selected" : "" ?> value="União Estável">União Estável</option>
                              </select>
                    </div> 

                    <div class="col-md-6">
                              <span>Profissão</span><b style="color:red">*</b>
                              <input name="coop_edit_profissao" value="<?= $row->profissao; ?>" placeholder="Ex: Guardador" class="form-control cad_profissao" id="coop_edit_profissao" name="cad_profissao" type="text">
                    </div>

                    <div class="col-md-6">
                              <span>Naturalidade</span><b style="color:red">*</b>
                              <input name="coop_edit_naturalidade" value="<?= $row->naturalidade; ?>" placeholder="Ex: Santo André" class="form-control cad_naturalidade" id="coop_edit_naturalidade" name="cad_naturalidade" type="text">
                           </div> 

                    

                           <div class="col-md-6">
                              <span>Email</span><b style="color:red">*</b>
                              <input name="coop_edit_email" value="<?= $row->email; ?>"placeholder="Ex: contato@email.com.br" id="coop_edit_email" id="coop_edit_telcel"class="form-control cad_email" name="cad_email" type="text">
                            
                           </div>

                           

                             <div class="col-md-6">
                              <span>Tel.  Whatsapp</span><b style="color:red">*</b>
                              <input  id="coop_edit_telcel" name="coop_edit_telcel" value="<?= $row->tel_cel; ?>"placeholder="Ex: (11) 92222-2222" class="form-control celular cad_cel"  type="text">
                             <br>
                             </div>   

                             

                              
                
                </div>

                <div class="modal-footer">      
                <button type="button" id="btnEditarCoop" class="btn btn-primary" onClick="EditCoop()">Salvar Alterações</button>                
                </div>            
            </form>
            </div>
         </div>
      </div>

      <!-- MODAL DE TERMOS -->

      <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
         <div class="modal-dialog">
            <div class="modal-content-termos">
               <div class="modal-header-termos">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                  <h3><img style="  width: 4rem; margin-right: 2rem; margin-left: 2rem;" src="http://<?= $server ?>/img/icones/term.svg" alt=""> TERMOS</h3>
               </div>
              

               <div class="modal-body">
                  <?php
                    
                    $termos = $class->Select("id, titulo", "termos", "", "");
                    while($rowT = $termos->fetch(PDO::FETCH_OBJ)){
                  ?>
                    <div class="col-md-6">
                       <div onclick="imprimeTermo(<?= $rowT->id ?>,<?= $row->id ?>)" class="col-md-3">  
                         <img style="width: 100%; " src="http://<?= $server ?>/img/icones/printer.svg" alt="">
                          <iframe  style="display:none "id="imprimir" name="imprimir" src="http://<?= $server ?>/impressao-termo/<?= $termo ?>/<?= $coop ?>"></iframe>
                        
                       </div>
                       <div style="padding: 0px;" class="col-md-9">
                          <h3 style="padding: 0" class="jhnm"><?= $rowT->titulo ?></h3>
                       </div>
                    </div>
                  <?php
                    }
                  ?>
                   </div>
                   <div class="modal-footer">      
                           
                </div>                           
             
            </div>
         </div>
      </div>

      <div id="throbber" style="display:none; min-height:120px;"></div>
      <div id="noty-holder"></div>
      <div id="wrapper">
         <!-- Navigation -->
         <?php include('../../includes/menu.php') ?>
         <div id="page-wrapper">
            <div class="container-fluid">
               <!-- Page Heading -->
               <div class="row" id="main" >
                  <div class="col-md-12 well">
                     <div style="margin-bottom: 1rem;" class="col-md-12">
                        <h3  onclick="newDoc()" class="rlk">Dados do Cooperado #<?= $row->id; ?></h3>
                     </div>
                     <div class="col-md-12">
                        <div class="col-md-4"><b>Nome:</b><br> <?= $row->nome; ?> <br><br></div>
                        <div class="col-md-4"><b>CPF:</b><br> <?= $row->cpf; ?> <br><br></div>
                        <div class="col-md-4"><b>Telefone:</b><br> <?= $row->tel_fixo; ?> <br><br></div>
                        <div class="col-md-4"><b>RG:</b><br> <?= $row->rg; ?> <br><br></div>
                        <div class="col-md-4"><b>Emissor:</b><br> <?= $row->emissor; ?> <br><br></div>
                        <div class="col-md-4"><b>Data de Nacimento:</b><br> <?= $nascimento; ?> <br><br></div>
                        
                        <div class="col-md-12">
                          <hr>
                        </div>

                        <div class="col-md-1">
                          <a href="#myModal" role="button"  data-toggle="modal">
                            <button class="btn btn-primary btn-sm btnTermo">
                              <i class="far fa-file-alt"></i>&nbsp;
                              Termos 
                            </button>
                          </a><br><br>
                        </div>

                        <div class="col-md-1">
                          <a href="#modalCoop" role="button"  data-toggle="modal">
                          <button type="button" class="btn btn-warning btn-sm">
                            <i class="far fa-edit"></i>&nbsp;
                            Editar
                          </button>
                          </a>
                          <br><br>
                        </div>

                       

                        <?php
                          if($row->status == '1'){
                        ?>

                          <div class="col-md-1">
                            <button onClick="deletaCooperado(<?= $_GET['coop'] ?>)" type="button" class="btn btn-danger btn-sm btnDelete">
                              <i class="fas fa-lock"></i>&nbsp;
                              Desativar
                            </button>
                            <br><br>
                          </div>

                        <?php
                          }else{
                        ?>
                          <div class="col-md-1">
                            <button onClick="ativaCooperado(<?= $_GET['coop'] ?>)" type="button" class="btn btn-success btn-sm btnActive">
                              <i class="fas fa-check"></i>&nbsp;
                              Ativar
                            </button>
                            <br><br>
                          </div>
                        <?php
                          }
                        ?>

                     </div>
                     <div class="col-md-12 well">
                        <div class="col-md-12">
                           <h3 onclick="newDoc()" class="rlk">Historico Financeiro</h3>
                        </div>
                        <div class="col-md-12">
                           <div class="col-md-12">
                              <table id="example" class="display nowrap" style="width:100%">
                                 <thead>
                                    <tr>
                                       <th>Tipo</th>
                                       <th>Valor</th>
                                       <th>Vencimento</th>
                                       <th>Status</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                 <?php
              
                                           
                    $dadosAdm = $class->Join("A.id_adm,A.numero_documento,  A.valor, A.data_vencimento,A.status,A.id_cooperado,M.id_manu AS manu, M.valor AS manu_valor, M.data_vencimento AS manu_vencimento,M.numero_documento AS doc_manu,M.status AS manu_status,M.status_pag AS manu_pag","cadastro_taxa_adm AS A","cadastro_taxa_manu AS M","ON A.id_cooperado = M.id_cooperado","WHERE A.id_cooperado = '$id'","GROUP BY M.id_manu");
                       
                     $dadosVenda = $class->Join("V.id_parcelas AS id_venda, V.numero_documento AS doc_venda, V.valor AS venda_valor, V.data_vencimento AS venda_vencimento, V.status AS venda_status, V.id_cooperado AS venda_coop,  V.id_prod AS prod","parcelas_produtos AS V","cadastro_taxa_adesao AS A","ON A.id_cooperado = V.id_cooperado","WHERE A.id_cooperado = '$id'","GROUP BY V.id_parcelas"); 
                       $dadosExtra = $class->Select("*","parcelas_taxa_extra","WHERE id_cooperado = '$id'","");
                       $dadosAD = $class->Select("*","cadastro_taxa_adesao","WHERE id_cooperado = '$id'","");
                       while($AD = $dadosAD->fetch(PDO::FETCH_OBJ)){

                         //formatacao taxa de adesao
                             $format3 = new DateTime($AD->data_vencimento);
                             $dataAdesao = $format3->format('d/m/Y');

                        ?>
                        <tr>
                          <td><?=$AD->numero_documento?></td>
                          <td><?= $AD->valor?></td>
                          <td><?=$dataAdesao?></td>
                          <td><?php if ($AD->status == 1){echo"Pago";}else{echo"Pendente";} ?></td>
                        </tr>
                        <?php
                          }
                        ?>
                        <?php

                                            $count = 1;
                                            $aux = 1;

                                   while ($adm = $dadosAdm->fetch(PDO::FETCH_OBJ)){

                                        
                                    
                                           
                                         
                                         
                                          //formatacao data taxa adm
                                        $format = new DateTime($adm->data_vencimento);
                                        $dataAdm = $format->format('d/m/Y');
                                        //formatacao data taxa extra
                                        $format2 = new DateTime($adm->manu_vencimento);
                                        $dataManu = $format2->format('d/m/Y');
                                       
                                          
                                        
                                     
                                   
                                       
                                 ?>
                                 <tr>
                                      <td><?php if($count == 1){echo $adm->numero_documento;}?>
                                     </td>

                                      <!-- valor -->
                                      <td><?php if($count == 1){ echo $adm->valor;}?></td>
                                          <!-- data -->
                                      <td><?php if($count == 1){echo $dataAdm;}?>
                                      
                                        </td>
                                      
                <!-- condicao status taxa adm -->
               <td><?php if($count == 1){if ($adm->status == 1){echo "Pago";}else{echo "Pendente";}}
               

               ?></td>
              
                              </tr>
                              <tr>
                                <td> <?=$adm->doc_manu?> </td>
                                <td><?=$adm->manu_valor?></td>
                                <td><?=$dataManu?></td>
                                <td>
                                <?php //condicao taxa manutencao
               if($adm->manu_pag == 1){echo "Pago";}else{echo "Pendente";}?></td>
                              </tr>
                                 <?php
                                    
                            while($venda = $dadosVenda->fetch(PDO::FETCH_OBJ)){
                               
                             
                              //formatacao produtos venda
                             $format4 = new DateTime($venda->venda_vencimento);
                            $dataVenda = $format4->format('d/m/Y');
                           ?>
                           
                            <tr>
                              <td><?= $venda->doc_venda?></td>
                              <td><?=$venda->venda_valor?></td>
                              <td><?= $dataVenda?></td>
                              <td><?php  //condicao status venda produtos
               if($venda->venda_status == 1){echo"Pago";}else{echo"Pendente";}
                ?></td>
                            </tr>     
                        <?php
                  
                       $count++;
                          }  
                              }
                        ?>

                            <?php
                              $numero = 1;
                         while($extra = $dadosExtra->fetch(PDO::FETCH_OBJ)){
                                     $format5 = new DateTime($extra->vencimento);
                                      $dataExtra = $format5->format('d/m/Y');
                                      ?>
                                  <tr>
                                        <td><?= "TAXA EXTRA".$numero?></td>
                                        <td><?= $extra->valor?>  </td>
                                        <td> <?= $dataExtra?></td>
                                        <td><?php  //condicao status taxa extra
               if($extra->status == 1){echo"Pago";}else{echo"Pendente";} ?></td>
                          </tr>         
                                      

                            <?php
                      $numero++;

                                    
                          }     
                                 ?> 
                               </tbody>
                              </table>
                           </div>
                           <br><br><br><br><br><br><br><br><br><br><br>
                           <div class="col-md-1 pull-right">
                            <span>&nbsp;</span><br>
                              <button style="background:#8c1019; border:#8c1019" onclick="window.history.go(-1); return false;" class="btn btn-warning">
                                Voltar
                              </button>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <script type="text/javascript">
        $(document).ready(function() {
           var table = $('#example').DataTable( {
           rowReorder: {
              selector: 'td:nth-child(2)'
           },
           responsive: true,
            "language": {
               "url": "https://cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
            }
           });
         });
      </script>
      <script type="text/javascript" src="http://<?= $server ?>/admin/_class/caminho_controler.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/menu-mobile.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/_app/_consulta/js/detalhe-consulta-cooperado.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/_app/_consulta/js/mask-detalhe-consulta-cooperado.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/_app/_consulta/js/editar-cooperado.js"></script>
      
      
   </body>
</html>
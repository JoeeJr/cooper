var host = location.host;
var server = "";
if (host == 'localhost'){
    server = host+'/'+pasta;
}else{
    server = host;
}



function viewCooperado(id){
  window.location.href = 'http://'+server+'/detalhe-consulta-de-cooperado/'+id;
}

function imprimeTermo(termo, coop){

	var url = "http://"+server+"/impressao-termo/"+termo+"/"+coop;

	$('<iframe id="imprimir" name="imprimir">')                             // create a new iframe element
        .hide()                               // make it invisible
        .attr("src", url) // point the iframe to the page you want to print
        .appendTo("body");    
}

function editaCooperado(id){

}


function deletaCooperado(id){
	swal({
	  title: "Desativar Cooperado?",
	  icon: "warning",
	  buttons: {
	  	cancel: "Não",
	  	confirm: "Sim",
	  }
	}).then((willDelete) => {
	  	if (willDelete) {
		 	swal({
			  title: "Aguarde..."
			})

	  	// Envia o post para deletar o bairro
	  	$.ajax({
		    url: 'http://'+server+'/_app/_consulta/class/deleta-cooperado.class.php',
		    type: "POST",
		    data:  {idCooperado: id},
		    success: function(data){
		    	if (data == 'ok'){
		    		swal({
				      title: "Cooperado Deletado!",
				      icon: "success",
				    }).then((value) => {
		             	window.location.reload();
		            });
		    	}else{
		    		swal({
				    	title: 'Verifique as informações e tente novamente!',
				    	icon: 'error',
				    	confirmButtonColor: '#3085d6',
				    	confirmButtonText: 'Ok'
				    })
		    	}
		    }         
	    });
	  } else {
	    swal.close();
	  }
	});
}


function ativaCooperado(id){
	swal({
	  title: "Ativar Cooperado?",
	  icon: "warning",
	  buttons: {
	  	cancel: "Não",
	  	confirm: "Sim",
	  }
	}).then((willDelete) => {
	  	if (willDelete) {
		 	swal({
			  title: "Aguarde..."
			})

	  	// Envia o post para deletar o bairro
	  	$.ajax({
		    url: 'http://'+server+'/_app/_consulta/class/ativa-cooperado.class.php',
		    type: "POST",
		    data:  {idCooperado: id},
		    success: function(data){
		    	if (data == 'ok'){
		    		swal({
				      title: "Cooperado Ativo!",
				      icon: "success",
				    }).then((value) => {
		             	window.location.reload();
		            });
		    	}else{
		    		swal({
				    	title: 'Verifique as informações e tente novamente!',
				    	icon: 'error',
				    	confirmButtonColor: '#3085d6',
				    	confirmButtonText: 'Ok'
				    })
		    	}
		    }         
	    });
	  } else {
	    swal.close();
	  }
	});

}


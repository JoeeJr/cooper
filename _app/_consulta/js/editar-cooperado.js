var host = location.host;
var server = "";
if (host == 'localhost'){
    server = host+'/'+pasta;
}else{
    server = host;
}


  function EditCoop(){
	var id = $('#id_coop').val();
	var coop_nome = $('#coop_edit_nome').val();
	var coop_nascimento = $('#coop_edit_nascimento').val();
	var coop_emissor = $('#coop_edit_emissor').val();
	var coop_cpf = $('#coop_edit_cpf').val();
	var coop_estadocivil = $('#coop_edit_estadocivil').val();
	var coop_profissao = $('#coop_edit_profissao').val();
	var coop_naturalidade = $('#coop_edit_naturalidade').val();
	var coop_email = $('#coop_edit_email').val();
	var coop_telcel = $('#coop_edit_telcel').val();
	


	if (id == '' ||coop_nome == '' || coop_nascimento == '' || coop_emissor == '' || coop_cpf == '' || coop_estadocivil == '' || coop_profissao == '' || coop_naturalidade == '' || coop_email == '' || coop_telcel == ''){
		swal({
	    	title: 'Preencha todos os campos obrigatórios (*)',
	    	icon: 'warning',
	    	confirmButtonColor: '#3085d6',
	    	confirmButtonText: 'Ok'
	    })
	}else{
		// Serializa o form
		var formEditcoop = $('#FormCoop').serialize();
		$.ajax({
		    url: 'http://'+server+'/_app/_consulta/class/update-cooperado.class.php',
		    type: "POST",
		    data:  formEditcoop,
		    success: function(data){
		    	if (data == 'ok'){
		    		swal({
				      title: "proprietario Atualizado!",
				      icon: "success",
				    }).then((value) => {
		             	window.location.reload();
		            });
		    	}else{

		    		 swal({
				     	title: 'Verifique as informações e tente novamente!',
				     	icon: 'error',
				    	confirmButtonColor: '#3085d6',
				    	confirmButtonText: 'Ok'
				     })
		    	}
		    }         
	    });
	}

};
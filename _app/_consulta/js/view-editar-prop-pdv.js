// autocomplet : this function will be executed every time we change the text
var host = location.host;
var server = "";
if (host == 'localhost'){
    server = host+'/'+pasta;
}else{
    server = host;
}
//busca cooperado
function BuscaPdvProp() {
	var inpBusca = $('#busca_pdv_prop').val();
	

	var min_length = 0; // min caracters to display the autocomplete
	var keyword = $('#busca_pdv_prop').val();
	if (keyword.length >= min_length) {

		if (inpBusca == '') {
			$('#lista_pdv_prop').hide();
		}else{

			$.ajax({
				url: 'http://'+server+'/_app/class_api/busca-editar-prop-pdv.class.php',
				type: 'POST',
				data: {keyword:keyword},
				success:function(data){
					$('#lista_pdv_prop').show();
					

					if (data == ''){
						var estrutura = "<li class='listagem_nenhum'>Nenhum resultado encontrado</li>";
						$('#lista_pdv_prop').html(estrutura);
					}else{
						$('#lista_pdv_prop').html(data);
					}
				}
			});
		}


	} else {
		$('#lista_pdv_prop').hide();
	}
}


function verificaPdvProp(){
	var inpBusca = $('#busca_pdv_prop').val();
	if (inpBusca == '') {
		$('.listagem_prop').hide();
	}
}





// set_item : this function will be executed when we select an item
function set_item_pdv_prop(item, item2) {
	// Seta o nome do preponente no campo
	$('#busca_pdv_prop').val(item);

	// ID do preponente
	$('#pdv_edit_matricula').val(item2);

	// Oculta a busca
	$('#busca_pdv_prop').hide();

	// Mostra o chipview
	$('#chipview-editapdv-prop').show();

	// Seta o nome do cooperado no chipview
	var str = item;
	if(str.length > 23) str = str.substring(0,23)+'...';
	$('#txt_name_prop').html(str);

	// Seta o valor do ID no button para fechar depois o chipview
	$('#close-chip-prop').val(item2);

	// hide proposition list
	$('#lista_pdv_prop').hide();
}
function closeChipProp(id){
	// Oculta o chipview
	$('#chipview-editapdv-prop').hide();

	// Zera o valor do ID no input
	$('#close-chip-prop').val('');

	// Exibe o campo de busca vazio
	$('#busca_pdv_prop').val('');
	$('#busca_pdv_prop').show();
}


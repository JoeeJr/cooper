var host = location.host;
var server = "";
if (host == 'localhost'){
    server = host+'/'+pasta;
}else{
    server = host;
}


  function EditPDV(){
	var id = $('#id_pdv').val();
	var pdv_operador = $('#pdv_edit_operador').val();
	var pdv_matricula = $('#pdv_edit_matricula').val();
	var pdv_status = $('#pdv_edit_status').val();
	
	


	if (id == '' ||pdv_operador == '' || pdv_matricula == '' || pdv_status == '' ){
		swal({
	    	title: 'Preencha todos os campos',
	    	icon: 'warning',
	    	confirmButtonColor: '#3085d6',
	    	confirmButtonText: 'Ok'

	    })
	}else{

		// Serializa o form
		var formEditpdv = $('#FormEditPdv').serialize();
		$.ajax({
		    url: 'http://'+server+'/_app/_consulta/class/update-pdv.class.php',
		    type: "POST",
		    data:  formEditpdv,
		    success: function(data){
		    	// alert(data);
		    	if (data.trim() == 'ok'){
		    		swal({
				      title: "PDV Atualizado!",
				      icon: "success",
				    }).then((value) => {
		             	window.location.reload();
		            });

		    	}else{

		    		 swal({
				     	title: 'Verifique as informações e tente novamente!',
				     	icon: 'error',
				    	confirmButtonColor: '#3085d6',
				    	confirmButtonText: 'Ok'
				     })
		    	}
		    }         
	    });
	}

};
<?php /*autenticador*/ include('../../admin/autenticador.php'); ?>
<?php /*controlador*/ include('../../admin/controler_sys.php'); ?>
<!DOCTYPE html>
<html lang="pt-br">
   <head>
      <meta charset="utf-8">
      <meta name="robots" content="noindex, nofollow">
      <title><?php include('../../includes/title.php'); ?></title>
      <meta name="viewport" content="width=device-width, initial-scale=1">

      <!-- FAV ICON -->
      <link rel="icon" type="image/png" href="http://<?= $server ?>/img/fav.png" />
       <!-- CSS -->
       <link rel="stylesheet" href="http://<?= $server ?>/css/consulta-de-cad.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/bootstrap3.3.0.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/estilo.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/fontawesome.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/responsive.dataTables.min.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/jquery.dataTables.min.css">
      <!-- JAVASCRIPTS --> 


      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.js"></script>
     

      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.dataTables.min.js"></script>
        
      <script type="text/javascript" src="http://<?= $server ?>/js/dataTable.responsive.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/bootstrap330.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/sweet-alert.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.mask.js"></script>


   </head>
   <style id="css">
      .well {
      min-height: 20px;
      padding: 19px;
      margin-bottom: 20px;
      background-color: #ffffff;
      border: 1px solid #e3e3e3;
      border-radius: 4px;
      -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
      box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
      }
      label {
      width: auto;
      height: 75px;;
      border-radius: 1px;
      border-right: 1px solid #e2e2e2;
      }
      @media (max-width: 748px){
      label {
      width: 33%;
      height: 100px;
      border-radius: 1px;
      border-right: 1px solid #c1c1c1;
      }
      label span {
      display: block;
      }
      }
      .row{
      margin-top: 3rem;
      }
      /*Panel tabs*/
.panel-tabs {
    position: relative;
    bottom: 30px;
    clear:both;
    border-bottom: 1px solid transparent;
}

.panel-tabs > li {
    float: left;
    margin-bottom: -1px;
}

.panel-tabs > li > a {
    margin-right: 2px;
    margin-top: 13px;
    line-height: .85;
    border: 1px solid transparent;
    border-radius: 4px 4px 0 0;
    color: #b1b1b1;
}

.panel-tabs > li > a:hover {
    border-color: transparent;
    color: #555564;
    background-color: transparent;
}
.panel-tabs > li > active {
    float: left;
    margin-bottom: -1px;
    border-top: 1px solid #c42c29;
}

.panel-tabs > li.active > a,
.panel-tabs > li.active > a:hover,
.panel-tabs > li.active > a:focus {
    color: #fff;
    cursor: default;
    -webkit-border-radius: 2px;
    -moz-border-radius: 2px;
    border-radius: 2px;
    background-color: rgba(255,255,255, .23);
    border-bottom-color: transparent;
}


.panel-primary>.panel-heading {
    color: #5f5f5f;
    background-color: #ffffff;
    border-color: #e6e6e6;
    padding: 3rem;
}
.panel-tabs > li.active > a, .panel-tabs > li.active > a:hover, .panel-tabs > li.active > a:focus {
    color: #555;
    cursor: default;
    border-top: 2px solid #c42c29;
    -webkit-border-radius: 2px;
    -moz-border-radius: 2px;
    border-radius: 2px;
    background-color: rgba(255,255,255, .23);
    border-bottom-color: transparent;
}
.tab-content>.active {
    display: block;
    visibility: visible;
    padding-top: 1rem;
}
.panel-primary {
    border-color: #ffffff;
}
   </style>
   <body>
      <div id="throbber" style="display:none; min-height:120px;"></div>
      <div id="noty-holder"></div>
      <div id="wrapper">
         <!-- Navigation -->
         <?php include('../../includes/menu.php') ?>



    
 


         <div id="page-wrapper">
            <div class="container-fluid">
               <!-- Page Heading -->
               <div class="row" id="main" >
                  <div class="col-md-12 well">
                     <div class="col-md-12">
                        <h3 class="rlk">Consulta de CAD</h3>
                     </div>
                     <div class="col-md-12">
                        <form method="POST" id="FormPesquisaPeriodo"  >
                       <!--  <div class="col-md-3">
                           <span>Matrícula</span>
                           <input id="matricula" name="matricula" placeholder="" class="form-control" type="text">
                           <br>
                        </div>
                        <div class="col-md-3">
                           <span>Nome</span>
                           <input id="nome_cooperado" name="nome_cooperado" placeholder="" class="form-control" type="text">
                           <br>
                        </div> -->
                        <div class="col-md-2">
                           <span>De</span>
                           <input id="inicio" name="inicio" placeholder="01/01/2018" class="form-control data" type="text">
                           <br>
                        </div>
                        <div class="col-md-2">
                           <span>Até</span>
                           <input id="fim" name="fim" placeholder="01/01/2018" class="form-control data" type="text">
                           <br>
                        </div>
                       <!--  <div class="col-md-2">
                           <span>IMEI</span>
                           <input placeholder="" class="form-control" type="text">
                           <br>
                        </div> -->
                        <div class="col-md-1">
                           <span>&nbsp</span>
                           <button type="button" id="pesquisar" name="pesquisar" class="btn btn-warning" onclick="Pesquisar()">Pesquisar</button>
                        </div>
                        <div class="col-md-12">
                           
                           <button type="button" class="btn btn-warning" onclick="window.location.reload()">Voltar</button>
                        </div>
                      </form>
                     </div>
                  </div>
                  <div class="col-md-12 well">





                    <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <span class="pull-left">
                        <!-- Tabs -->
                        <ul class="nav panel-tabs">
                            <li  class="active"><a href="#tab1" data-toggle="tab">Analítico</a></li>
                            <li><a href="#tab2" data-toggle="tab">Sintético</a></li>
                        </ul>
                    </span>
                </div>
                <div class="panel-body">
                    <div class="tab-content" id="teste" >
                        <div class="tab-pane active" id="tab1"> <table id="example" class="display  nowrap" style="width:100%">
                              <thead>
                                 <tr>
                                    <th>Cooperado</th>
                                    <th>N° do IMEI</th>
                                    <th>Placa</th>
                                    <th>Data</th>
                                    <th>Autênticação</th>
                                    <th>Qtd</th>
                                    <th>Valor</th>
                                 </tr>
                              </thead>
                              <tbody id="aa">
                                 <?php 
                                    $dados = $class->Select("*","repasse_cad","","");
                                    
                                    
                                    
                                    
                                    
                                    while($row = $dados->fetch(PDO::FETCH_OBJ)){
                                             // Data de nascimento formatada
                                            $format = new DateTime($row->data);
                                            $data = $format->format('d/m/Y');
                                    
                                        
                                    
                                    
                                    
                                    
                                    
                                    ?>
                                 <tr>
                                    <td class="bb"><?= $row->nome_cooperado ?></td>
                                    <td class="bb"><?= $row->imei ?></td>
                                    <td class="bb"><?= $row->placa ?></td>
                                    <td class="bb"><?= $data ?></td>
                                    <td class="bb"><?= $row->auth ?></td>
                                    <td class="bb"><?= $row->qtd ?></td>
                                    <td class="bb"><?= $row->valor ?></td>
                                 </tr>
                                 <?php
                                    }
                                    ?>
                              </tbody>
                              <tfoot>
                                 <th>Cooperado</th>
                                 <th>N° do IMEI</th>
                                 <th>Placa</th>
                                 <th>Data</th>
                                 <th>Autênticação</th>
                                 <th>Qtd</th>
                                 <th>Valor</th>
                              </tfoot>
                           </table></div>
                        <div class="tab-pane" id="tab2"><table id="example2" class="display nowrap" style="width:100%">
                              <thead>
                                 <tr>
                                    <th>Cooperado</th>
                                    <th>Nº do IMEI</th>
                                    <th>Período</th>
                                    <th>Qtd</th>
                                    <th>Valor</th>
                                 </tr>
                              </thead>
                              <tbody id="teste">
                                 <?php 
                                    $dados = $class->Select("*","historico_repasse_cad","","");
                                   
                                    
                                    
                                    
                                    $total = 0;
                                    while($row = $dados->fetch(PDO::FETCH_OBJ)){
                                             // Data de nascimento formatada
                                            $format = new DateTime($row->data);
                                            $data = $format->format('d/m/Y');
                                    
                                        $total += $row->valor;
                                    
                                    
                                    
                                    
                                    
                                    ?>
                                 <tr>
                                    <td id="nome0"><?= $row->nome_cooperado ?></td>
                                    <td><?= $row->imei ?></td>
                                    <td><?= $data ?></td>
                                    <td><?= $row->qtd ?></td>
                                    <td><?= $row->valor ?></td>
                                 </tr>
                                 <?php
                                    }
                                    ?>
                              </tbody>
                              <tfoot>
                                 <tr>
                                    <th>-</th>
                                    <th>-</th>
                                    <th>-</th>
                                    <th>Total:</th>
                                    <th style="color: #00ab56;">R$:<?= $total?></th>
                                 </tr>
                              </tfoot>
                           </table></div>
                    </div>
                </div>
            </div>
        </div>












                     
                  </div>
               </div>
            </div>
         </div>
      </div>
      </div>
      <script type="text/javascript"></script>
      <script type="text/javascript">
         $(function(){
             $('[data-toggle="tooltip"]').tooltip();
             $(".side-nav .collapse").on("hide.bs.collapse", function() {                   
                 $(this).prev().find(".fa").eq(1).removeClass("fa-angle-right").addClass("fa-angle-down");
             });
             $('.side-nav .collapse').on("show.bs.collapse", function() {                        
                 $(this).prev().find(".fa").eq(1).removeClass("fa-angle-down").addClass("fa-angle-right");        
             });
         })   
         $(document).ready(function(){
            $('.data').mask('00/00/0000');
           });  
          $(document).ready(function() {
           var table = $('#example').DataTable( {
           rowReorder: {
              selector: 'td:nth-child(2)'
           },
           destroy: true,
retrieve:true,
paging: false,
           responsive: true,
            "language": {
               "url": "https://cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
            }
           });

           var table2 = $('#example2').DataTable( {
           rowReorder: {
              selector: 'td:nth-child(2)'
           },
           destroy: true,
retrieve:true,
paging: false,
           responsive: true,
            "language": {
               "url": "https://cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
            }
           });
         });
         
         
         
         
         
        
      </script>
      <!--  <script>
         $(document).ready(function() {
             $('#datePicker')
                 .datepicker({
                     format: 'mm/dd/yyyy'
                 })
         
         
                 .on('changeDate', function(e) {
                     // Revalidate the date field
                     $('#eventForm').formValidation('revalidateField', 'date');
                 });
         
             $('#eventForm').formValidation({
                 framework: 'bootstrap',
                 icon: {
                     valid: 'glyphicon glyphicon-ok',
                     invalid: 'glyphicon glyphicon-remove',
                     validating: 'glyphicon glyphicon-refresh'
                 },
                 fields: {
                     name: {
                         validators: {
                             notEmpty: {
                                 message: 'The name is required'
                             }
                         }
                     },
                     date: {
                         validators: {
                             notEmpty: {
                                 message: 'The date is required'
                             },
                             date: {
                                 format: 'MM/DD/YYYY',
                                 message: 'The date is not a valid'
                             }
                         }
                     }
                 }
         
         
         
         
         
             });
         });
         
         
         </script> -->
      <script type="text/javascript" src="http://<?= $server ?>/admin/_class/caminho_controler.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/menu-mobile.js"></script>
     <script type="text/javascript" src="http://<?= $server ?>/_app/_consulta/js/consulta-cad.js"></script>     
   </body>
</html>
<?php /*autenticador*/ include('../../admin/autenticador.php'); ?>
<?php /*controlador*/ include('../../admin/controler_sys.php'); ?>
<!DOCTYPE html>
<html lang="pt-br">
   <head>
      <meta charset="utf-8">
      <meta name="robots" content="noindex, nofollow">
      <title><?php include('../../includes/title.php'); ?></title>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- FAV ICON -->
      <link rel="icon" type="image/png" href="http://<?= $server ?>/img/fav.png" />
       <!-- CSS -->
      <link rel="stylesheet" href="http://<?= $server ?>/css/bootstrap3.3.0.css">
      <!-- <link rel="stylesheet" href="http://<?= $server ?>/css/datepicker.css" /> -->
      <link rel="stylesheet" href="http://<?= $server ?>/css/estilo.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/fontawesome.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/responsive.dataTables.min.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/jquery.dataTables.min.css">
      
      
      <!-- JAVASCRIPTS -->
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/cep.js"></script>
      <!-- <script type="text/javascript" src="http://<?= $server ?>/js/datapicker.js"></script> -->
      <script type="text/javascript" src="http://<?= $server ?>/js/bootstrap330.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.mask.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.maskMoney.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/sweet-alert.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.maskMoney.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/dataTable.responsive.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.dataTables.min.js"></script>
      

      <script src="https://code.highcharts.com/highcharts.js"></script>
      <script src="https://code.highcharts.com/modules/exporting.js"></script>
      <script src="https://code.highcharts.com/modules/export-data.js"></script>

   </head>
   <style>
      .well {
      min-height: 20px;
      padding: 19px;
      margin-bottom: 20px;
      background-color: #ffffff;
      border: 1px solid #e3e3e3;
      border-radius: 4px;
      -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
      box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
      }
      label {
      width: auto;
      height: 75px;;
      border-radius: 1px;
      border-right: 1px solid #e2e2e2;
      }
      
      @media (max-width: 748px){
      label {
      width: 33%;
      height: 100px;
      border-radius: 1px;
      border-right: 1px solid #c1c1c1;
      }
      label span {
      display: block;
      }
      }
      .row{
      margin-top: 3rem;
      }
      #radioBtn .notActive{
    color: #3276b1;
    background-color: #fff;
}
   </style>
 <body>
   <?php 
      $id = $_GET["id"];
      // $dados = $class->Select("*","historico_taxa_adm","WHERE id_adm = '$id'","");
      // $mostrar = $dados->fetch(PDO::FETCH_OBJ);

      // $dadosR = $class->Select("*","cadastro_taxa_adm","WHERE id_adm = '$id'","");
      // $mostrarR = $dados->fetch(PDO::FETCH_OBJ);

         // // Data de nascimento formatada
         //        $format = new DateTime($mostrarR->data_vencimento);
         //         $vencimento = $format->format('d/m/Y');



        


    ?>
      <div id="throbber" style="display:none; min-height:120px;"></div>
      <div id="noty-holder"></div>
      <div id="wrapper">
         <!-- Navigation -->
        <?php include('../../includes/menu.php') ?>
         <div id="page-wrapper">
            <div class="container-fluid">
               <!-- Page Heading -->
               <div class="row" id="main">               
                  <div class="col-md-12 well">                    
                     <div class="col-md-12">
                       <h3 class="rlk">DETALHE VENDA DE PRODUTOS</h3>
                     </div>



                    <!-- FORMULARIO DE EDIÇÃO -->

                  </div>

                   <div class="col-md-12 well">
                   <div class="col-md-12">
                        <h3 class="rlk">VENDA DE PRODUTOS</h3>
                     </div>
                     <div class="col-md-12">
                        <div class="col-md-12">
                      <table id="example" class="display nowrap" style="width:100%">
                          <thead>
                               <tr>
                                  <th>Nome</th>
                                  <th>Vencimento</th>
                                  <th>Parcelas</th>
                                  <th>Valor Parcelas</th>
                                  <th>Status</th>
                                  
                              </tr>
                          </thead>
                          <?php 
                   
                    // $id = $_GET['id_apagar'];
                    $dados = $class->Select("*","parcelas_produtos","WHERE id_prod = '$id'","");



                 
                   
                   
                   while($row = $dados->fetch(PDO::FETCH_OBJ)){ 

                    $dadosP = $class->Select("id_cooperado,nome_produto,desc_produto","venda_produtos","WHERE id_prod = '$id'","");
                    $rowP = $dadosP->fetch(PDO::FETCH_OBJ);
                    
                     $dadosCoop = $class->SelectEsp("nome","cooperados","WHERE id = '$rowP->id_cooperado'");
                      // Data de nascimento formatada
                  $format = new DateTime($row->data_vencimento);
                  $vencimento = $format->format('d/m/Y');

                        

                    

                 

            ?>
            <tr>
                <td><?= $dadosCoop ?></td>
                <td><?= $vencimento ?></td>
                <td><?= $row->parcelas ?></td>         
                <td><?= $row->valor_parcelas ?></td>
                <td>
                   <div class="input-group">
                      
                    <div id="radioBtn" class="btn-group">
                    <a <?php if($row->status == 1){echo 'class="btn btn-primary btn-sm active"';}?> class="btn btn-primary btn-sm notActive" data-toggle="status" data-title="1" value="1" onclick="Status(<?= $row->id_parcelas ?>, 1)">Pago</a>
               
                     <a <?php if($row->status == 0){echo 'class="btn btn-primary btn-sm active"';}?>  class="btn btn-primary btn-sm notActive" data-toggle="status" data-title="0" value="0" onclick="Status(<?= $row->id_parcelas ?>, 0)">Aberto</a>
                  </div> 
                </td>
                
            </tr>
            <?php
                }
             ?>
                          <tfoot>
                              <tr>
                                  <th>Nome</th>
                                  <th>Vencimento</th>
                                  <th>Parcelas</th>
                                  <th>Valor Parcelas</th>
                                  <th>Status</th>
                                  
                              </tr>
                          </tfoot>
                      </table>
                  </div>

                  
                  </div>
                  <div class="col-md-2 pull-light">
                            <span>&nbsp</span>
                             <button style="background-color: #777777;" onclick="window.history.go(-1); return false;" class="btn btn-warning">Voltar</button>
                        </div>

            </div>




          </div>
        </div>
      </div>
    </div>
  <script type="text/javascript">        
         $(document).ready(function() {
           var table = $('#example').DataTable( {
           rowReorder: {
              selector: 'td:nth-child(2)'
           },
           responsive: true,
            "language": {
               "url": "https://cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
            }
           });
         });

         $(function(){
             $('[data-toggle="tooltip"]').tooltip();
             $(".side-nav .collapse").on("hide.bs.collapse", function() {                   
                 $(this).prev().find(".fa").eq(1).removeClass("fa-angle-right").addClass("fa-angle-down");
             });
             $('.side-nav .collapse').on("show.bs.collapse", function() {                        
                 $(this).prev().find(".fa").eq(1).removeClass("fa-angle-down").addClass("fa-angle-right");        
             });
         })    
      </script>


        <script type="text/javascript" src="http://<?= $server ?>/admin/_class/caminho_controler.js"></script>
         <script type="text/javascript" src="http://<?= $server ?>/_app/_financeiro/js/update-historico-venda.js"></script>
        <script type="text/javascript" src="http://<?= $server ?>/js/menu-mobile.js"></script>
      
   </body>
   </html>
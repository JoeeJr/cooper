<style type="text/css">
   .list-group-horizontal .list-group-item {
    display: inline-block;
   }
   .list-group-horizontal .list-group-item {
      margin-bottom: 0;
      margin-left:-4px;
      margin-right: 0;
   }
   .list-group-horizontal .list-group-item:first-child {
      border-top-right-radius:0;
      border-bottom-left-radius:4px;
   }
   .list-group-horizontal .list-group-item:last-child {
      border-top-right-radius:4px;
      border-bottom-left-radius:0;
   }
</style>

<?php
   //  vai retornar o arquivo de pagina
   $url = $_SERVER['PHP_SELF'];
   $pagina = basename($url, '.php');
   
?>

<div class="list-group list-group-horizontal">
    <a href="http://<?= $server ?>/contas-a-receber" class="list-group-item <?= ($pagina == 'contas-a-receber') ? "active" : "" ?>">Graficos</a>
    <a href="http://<?= $server ?>/taxa_administracao" class="list-group-item <?= ($pagina == 'include-taxa-adm') ? "active" : "" ?>">Taxa de administração</a>
    <a href="http://<?= $server ?>/taxa_manutencao" class="list-group-item <?= ($pagina == 'include-taxa-manutencao') ? "active" : "" ?>">Taxa de Manutenção</a>
    <a href="http://<?= $server ?>/taxa_adesao" class="list-group-item <?= ($pagina == 'include-taxa-adesao') ? "active" : "" ?>">Taxa de Adesão</a>
    <a href="http://<?= $server ?>/repasse_cad" class="list-group-item <?= ($pagina == 'include-repasse-cad') ? "active" : "" ?>">Repasse de CAD</a>
    <a href="http://<?= $server ?>/taxa_venda" class="list-group-item <?= ($pagina == 'include-venda-produtos') ? "active" : "" ?>">Venda de Produtos</a>
    <a href="http://<?= $server ?>/taxa_extra" class="list-group-item <?= ($pagina == 'include-taxa-extra') ? "active" : "" ?>">Taxa Extra</a>    
</div>



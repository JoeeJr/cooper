<?php /*autenticador*/ include('../../admin/autenticador.php'); ?>
<?php /*controlador*/ include('../../admin/controler_sys.php'); ?>
<!DOCTYPE html>
<html lang="pt-br">
   <head>
      <meta charset="utf-8">
      <meta name="robots" content="noindex, nofollow">
      <title><?php include('../../includes/title.php'); ?></title>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      
      <!-- FAV ICON -->
      <link rel="icon" type="image/png" href="http://<?= $server ?>/img/fav.png" />

      <!-- CSS -->
      <link rel="stylesheet" href="http://<?= $server ?>/css/bootstrap3.3.0.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/estilo.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/fontawesome.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/datepicker.css" />
     <link rel="stylesheet" href="http://<?= $server ?>/css/responsive.dataTables.min.css">
    <link rel="stylesheet" href="http://<?= $server ?>/css/jquery.dataTables.min.css">
      
    

      <!-- JAVASCRIPTS -->
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/cep.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/datapicker.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/bootstrap330.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.mask.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.maskMoney.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/sweet-alert.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.maskMoney.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/dataTable.responsive.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.dataTables.min.js"></script>

      <script src="https://code.highcharts.com/highcharts.js"></script>
      <script src="https://code.highcharts.com/modules/exporting.js"></script>
      <script src="https://code.highcharts.com/modules/export-data.js"></script>

   </head>
   <style>
    .well {
      min-height: 20px;
      padding: 19px;
      margin-bottom: 20px;
      background-color: #ffffff;
      border: 1px solid #e3e3e3;
      border-radius: 4px;
      -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
      box-shadow: inset 0 1px 1px rgba(0,0,0,.05);

    }
    label {
    width: auto;
    }

  #radioBtn .notActive{
    color: #3276b1;
    background-color: #fff;
}
   </style>
   <body>
    <?php 
      $id = $_GET["pagar"];
      $dados = $class->Select("*","historico_contas_a_pagar","WHERE id_apagar = '$id'","");
      $mostrar = $dados->fetch(PDO::FETCH_OBJ);

      $dadosR = $class->Select("*","contas_a_pagar","WHERE id_apagar = '$id'","");
      $mostrarR = $dados->fetch(PDO::FETCH_OBJ);

         // Data de nascimento formatada
                $format = new DateTime($mostrar->vencimento);
                 $vencimento = $format->format('d/m/Y');



        


    ?>
      <div id="throbber" style="display:none; min-height:120px;"></div>
      <div id="noty-holder"></div>
      <div id="wrapper">
         <!-- Navigation -->
        <?php include('../../includes/menu.php') ?>
         <div id="page-wrapper">
            <div class="container-fluid">
               <!-- Page Heading -->
               <div class="row" id="main" >
               
                  <div class="col-md-12 well">
                     <div class="col-md-12">
                        <h3 class="rlk">Detalhe da Conta</h3>
                     </div>
                     <div class="col-md-6">

                      <div class="col-md-12">
                        
                        <div class="col-md-3">
                             <span>Nome:</span>
                             <h4><?= $mostrar->nome ?></h4>
                        </div>

                      <div class="col-md-12">

                        <hr>
                        <!-- <div class="col-md-6 ">
                             <span>Valor:</span>
                             <input id="valor_boleto" name="valor_boleto" placeholder="0,00" data-thousands="." data-decimal="," value="<?= $mostrar->valor ?>" class="form-control valor-boleto" type="text" disabled>
                        </div>
                        <div class="col-md-3">
    
                            <?php 
                              $data = new DateTime();
                              $formatar =$data;
                              $calc = $formatar->format('d/m/Y');
                            ?>
                             
                            <span>Encargos:</span>
                            <input id="encargos" name="encargos" placeholder ="0,00" data-thousands="." data-decimal="," class="form-control" type="text"<?php if($vencimento < $calc){ echo 'disabled';}?>/>
                            <br>
                        </div>
                         -->

                      </div>
                      <div class="col-md-3">
                          <span>Status recorrente</span><br>
                          <div class="form-group has-feedback">
                            <label class="input-group">
                              <span class="input-group-addon">
                                <input <?php if($mostrarR->pagamento_recorrente == 1){echo 'checked="checked"';}?> type="radio" name="recorrente" id="recorrente" value="1"  onclick="UpdateRecorrente(<?= $mostrarR->id_apagar ?>, 1 )" />
                              </span>
                              <div  style="background: #90c1a9;" class="form-control form-control-static">
                                Ativo
                              </div>
                              <span class="input-group-addon">
                                <input <?php if($mostrarR->pagamento_recorrente == 0){echo 'checked="checked"';}?> type="radio" name="recorrente" id="recorrente" value="0" onclick="UpdateRecorrente(<?= $mostrarR->id_apagar ?>, 0 )" />
                              </span>
                              <div  style="background: #90c1a9;" class="form-control form-control-static">
                                Desativado
                              </div>
                              <span class="glyphicon form-control-feedback "></span>
                            </label>
                           
                          </div>

                         
                           
                             
                        </div>
                       

                        
                        

                        
                  
    

                    </div>

                   

                       
                     </div>
  
                  <div class="col-md-6">
                     <div class="col-md-12">
                         <div id="container" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
                     </div>
                    </div>
                  </div>
  



                  <div class="col-md-12 well">
                   <div class="col-md-12">
                        <h3 class="rlk">CONTAS A PAGAR</h3>
                     </div>
                     <div class="col-md-12">
                        <div class="col-md-12">
                      <table id="example" class="display nowrap" style="width:100%">
        <thead>
            <tr>
                <th>#ID</th>
                <th>Nome</th>
                <th>Valor</th>
                <th>Vencimento</th>
                <th>Parcela</th>
                <th>Status</th>
                <th>#</th>
            </tr>
        </thead>
        <tbody>
          <?php 
 $teste = $class->Select("id_his, nome, valor, vencimento, qtd_parcelas, status","historico_contas_a_pagar","WHERE id_apagar = '$id'","");
 
                  
                          
                          foreach ($teste->fetchAll(PDO::FETCH_OBJ) as $row) { 
                          // Data de nascimento formatada
                          $format = new DateTime($row->vencimento);
                            $vencimento = $format->format('d/m/Y');
         
            ?>
            <tr>
                <td><?= $row->id_his ?></td>
                <td><?= $row->nome ?></td>
                <td><?= $row->valor ?></td>
                <td><?= $vencimento ?></td>
                <td><?= $row->qtd_parcelas ?></td>
                <td>
                 
                    <div class="input-group">
                      
                    <div id="radioBtn" class="btn-group">
                    <a <?php if($row->status == 1){echo 'class="btn btn-primary btn-sm active"';}?> class="btn btn-primary btn-sm notActive" data-toggle="status" data-title="1" value="1" onclick="Status(<?= $row->id_his ?>, 1)">Pago</a>
               
                     <a <?php if($row->status == 0){echo 'class="btn btn-primary btn-sm active"';}?>  class="btn btn-primary btn-sm notActive" data-toggle="status" data-title="0" value="0" onclick="Status(<?= $row->id_his ?>, 0)">Aberto</a>
                  </div> 



                 </td>
                <td><div onclick="lupz(<?= $row->id_his ?>)" class="lupa"></div></td>
            </tr>
         <?php
              }
         ?>
        </tbody>
        <tfoot>
            <tr>
                <th>#ID</th>
                <th>Nome</th>
                <th>Valor</th>
                <th>Vencimento</th>
                <th>Parcela</th>
                <th>Status</th>
                <th>#</th>
            </tr>
        </tfoot>
    </table>
                     </div>
                  </div>

                  <div class="col-md-2 pull-light">
                            <span>&nbsp</span>
                             <button style="background-color: #777777;" onclick="window.history.go(-1); return false;" class="btn btn-warning">Voltar</button>
                        </div>

            </div>




          </div>
        </div>
      </div>
    </div>
  </div>
<script type="text/javascript">
function lupz(id){
  window.location.href = 'http://<?= $server ?>/detalhe-da-conta/'+id;
}
</script>

      <script type="text/javascript">
         $(function(){
             $('[data-toggle="tooltip"]').tooltip();
             $(".side-nav .collapse").on("hide.bs.collapse", function() {                   
                 $(this).prev().find(".fa").eq(1).removeClass("fa-angle-right").addClass("fa-angle-down");
             });
             $('.side-nav .collapse').on("show.bs.collapse", function() {                        
                 $(this).prev().find(".fa").eq(1).removeClass("fa-angle-down").addClass("fa-angle-right");        
             });
         })    

$(document).ready(function() {
    var table = $('#example').DataTable( {
        rowReorder: {
            selector: 'td:nth-child(2)'
        },
        responsive: true
    } );
} );
      </script>

          <?php 
        $grafico = $class->Select("*","historico_contas_a_pagar","WHERE id_apagar = '$id'","");
      
        $percent = 0;
        $i = 0;
       

      foreach ($grafico->fetchAll(PDO::FETCH_OBJ) as $graf) {
            
        
        if($graf->status == 1){
         
          
            $percent = $percent + 1;
            
        }else{
            $i = $i + 1;
        }}
       
       
      ?>

      <script>
        Highcharts.setOptions({
    colors: Highcharts.map(Highcharts.getOptions().colors, function (color) {
        return {
            radialGradient: {
                cx: 0.5,
                cy: 0.3,
                r: 0.7
            },
            stops: [
                [0, color],
                [1, Highcharts.Color(color).brighten(-0.3).get('rgb')] // darken
            ]
        };
    })
});

// Build the chart
Highcharts.chart('container', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: <?= "'Contas Pagas x Pendentes de {$mostrar->nome}'" ?>
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.y}</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.y}',
                style: {
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                },
                connectorColor: 'silver'
            }
        }
    },
    series: [{
        name: 'Share',
        data: [
            { name: 'Pagas', y: <?= $percent ?>},
            { name: 'Pendentes', y: <?= $i ?> }
            
        ]
    }]
});
      </script>
      <!--  <script type="text/javascript">        
         $(document).ready(function() {
           var table = $('#example').DataTable( {
           rowReorder: {
              selector: 'td:nth-child(2)'
           },
           responsive: true,
            "language": {
               "url": "https://cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
            }
           });
         });
      </script> -->
      <script type="text/javascript">
//         $('#radioBtn a').on('click', function(){
//     var sel = $(this).data('title');
//     var tog = $(this).data('toggle');
//     $('#'+tog).prop('value', sel);
    
//     $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
//     $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
// })
</script>


       <script type="text/javascript" src="http://<?= $server ?>/admin/_class/caminho_controler.js"></script>
       <script type="text/javascript" src="http://<?= $server ?>/js/menu-mobile.js"></script>
       <script type="text/javascript" src="http://<?= $server ?>/_app/_financeiro/js/detalhe-contas-a-pagar.js"></script>  

   </body>
</html>
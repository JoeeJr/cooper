<?php /*autenticador*/ include('../../admin/autenticador.php'); ?>
<?php /*controlador*/ include('../../admin/controler_sys.php'); ?>
<!DOCTYPE html>
<html lang="pt-br">
   <head>
      <meta charset="utf-8">
      <meta name="robots" content="noindex, nofollow">
      <title><?php include('../../includes/title.php'); ?></title>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- FAV ICON -->
      <link rel="icon" type="image/png" href="http://<?= $server ?>/img/fav.png" />
     <!-- CSS -->
      <link rel="stylesheet" href="http://<?= $server ?>/css/bootstrap3.3.0.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/estilo.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/fontawesome.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/responsive.dataTables.min.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/jquery.dataTables.min.css">
      <!-- JAVASCRIPTS --> 


      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.js"></script>
     

     
      <script type="text/javascript" src="http://<?= $server ?>/js/bootstrap330.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/sweet-alert.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.mask.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.maskMoney.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/datapicker.js"></script>
       <script type="text/javascript" src="http://<?= $server ?>/js/jquery.dataTables.min.js"></script>
        
      <script type="text/javascript" src="http://<?= $server ?>/js/dataTable.responsive.js"></script>
   </head>
  <style>
  label {
    width: auto;
    height: 75px;
    padding-top: 3rem;
    border-radius: 1px;
    border-right: none;
}
      .well {
      min-height: 20px;
      padding: 19px;
      margin-bottom: 20px;
      background-color: #ffffff;
      border: 1px solid #e3e3e3;
      border-radius: 4px;
      -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
      box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
      }
    
      
      @media (max-width: 748px){
      label {
      width: 33%;
      height: 100px;
      border-radius: 1px;
      border-right: 1px solid #c1c1c1;
      }
      label span {
      display: block;
      }
      }
      .row{
      margin-top: 3rem;
      }
   </style>
 <body>
      <div id="throbber" style="display:none; min-height:120px;"></div>
      <div id="noty-holder"></div>
      <div id="wrapper">
         <!-- Navigation -->
        <?php include('../../includes/menu.php') ?>
         <div id="page-wrapper">
            <div class="container-fluid">
               <!-- Page Heading -->
               <div class="row" id="main">               
                  <div class="col-md-12 well">                    
                    <div class="col-md-12">
                       <h3 class="rlk">REPASSE CAD</h3>
                    </div>

                   





                    <!-- FORMULARIO DE EDIÇÃO -->

                  </div>

                  <div class="col-md-12 well">
                     <div class="col-md-6">
                      <form method="POST" id="FormRepasseCad">
                       <table id="example" class=" display  nowrap" style="width:100%">
                          <thead>
                              <tr>
                                  <th>Cooperado</th>
                                  <th>Matricula</th>
                              </tr>
                          </thead>
                          <?php 

                           
                              
                           $dados = $class->Select("nome_cooperado","historico_repasse_cad","","");
                             

                 
                    
                            $count = 0;
                           while($row = $dados->fetch(PDO::FETCH_OBJ)){
                                 
                              
                                   $count++; 
                                $teste = $class->SelectEsp("auth","repasse_cad","WHERE nome_cooperado = '$row->nome_cooperado'");
                       



                                   

                           ?>
                          
                         
                             
                            
                           
                              <tr>
                                    <td><?= $row->nome_cooperado ?></td>
                                    <td><input type="text"id="matricula_cad_<?= $count?>" name="matricula_cad[]" class="form-control matricula_cad" onblur="teste(<?= $count ?>)"/></td>
                                    <td><label  id="matricula_<?= $count ?>" name="matricula[]" class="matricula"></label></td>
                                    <td><input type="hidden" name="auth[]" value="<?= $teste ?>"></td>
                              </tr>
                          <?php
                                
                              }
                             ?>

                    <tfoot>
                        <th>Cooperado</th>
                        <th>Matricula</th>
                          </tfoot>
                      </table>
                      <br> 
                      <input type="hidden" name="contador" value="<?= $count ?>">
                       <div class="col-md-2 pull-right">                           
                              <button type="button" onclick="AtualizaRepasse()" class="btn btn-warning">Atualziar Matrícula</button> 
                             
                      </div>
                       <div class="col-md-1 pull-light">
                           
                             <button style="background-color: #777777;" onclick="window.history.go(-1); return false;" class="btn btn-warning">Voltar</button>
                        </div>
                    </form>
                   </div>

                  </div>
                 
               </div>
            </div>
         </div>
      </div>
      </div>
       <script type="text/javascript">        
         $(document).ready(function() {
           var table = $('#example').DataTable( {
           rowReorder: {
              selector: 'td:nth-child(2)'
           },
           responsive: true,
            "language": {
               "url": "https://cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
            }
           });
         });

          $(document).ready(function(){
            $('input[type="text"]').mask('0000');
          });
      </script>
      <script type="text/javascript">


      </script>
      <script type="text/javascript" src="http://<?= $server ?>/admin/_class/caminho_controler.js"></script>
       <script type="text/javascript" src="http://<?= $server ?>/_app/_financeiro/js/update-repasse-cad.js"></script>
       <script type="text/javascript" src="http://<?= $server ?>/_app/_financeiro/js/onblur-repasse-cad.js"></script>
      
      <script type="text/javascript" src="http://<?= $server ?>/js/menu-mobile.js"></script>
      
   </body>
   </html>
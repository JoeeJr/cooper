<?php /*autenticador*/ include('../../admin/autenticador.php'); ?>
<?php /*controlador*/ include('../../admin/controler_sys.php'); ?>
<!DOCTYPE html>
<html lang="pt-br">
  <head>
      <meta charset="utf-8">
      <meta name="robots" content="noindex, nofollow">
      <title><?php include('../../includes/title.php'); ?></title>
     
      <meta name="viewport" content="width=device-width, initial-scale=1">

      <!-- FAV ICON -->
      <link rel="icon" type="image/png" href="http://<?= $server ?>/img/fav.png" />
       <!-- CSS -->
      <link rel="stylesheet" href="http://<?= $server ?>/css/bootstrap3.3.0.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/estilo.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/fontawesome.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/responsive.dataTables.min.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/jquery.dataTables.min.css">
      <!-- JAVASCRIPTS --> 


      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.js"></script>
     

     
      <script type="text/javascript" src="http://<?= $server ?>/js/bootstrap330.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/sweet-alert.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.mask.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.maskMoney.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/datapicker.js"></script>
       <script type="text/javascript" src="http://<?= $server ?>/js/jquery.dataTables.min.js"></script>
        
      <script type="text/javascript" src="http://<?= $server ?>/js/dataTable.responsive.js"></script>

   </head>
 <style>
      .well {
      min-height: 20px;
      padding: 19px;
      margin-bottom: 20px;
      background-color: #ffffff;
      border: 1px solid #e3e3e3;
      border-radius: 4px;
      -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
      box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
      }
      label {
      width: auto;
      height: 75px;;
      border-radius: 1px;
      border-right: 1px solid #e2e2e2;
      }
      
      @media (max-width: 748px){
      label {
      width: 33%;
      height: 100px;
      border-radius: 1px;
      border-right: 1px solid #c1c1c1;
      }
      label span {
      display: block;
      }
      }
      .row{
      margin-top: 3rem;
      }
   </style>
 <body>
      <div id="throbber" style="display:none; min-height:120px;"></div>
      <div id="noty-holder"></div>
      <div id="wrapper">
         <!-- Navigation -->
        <?php include('../../includes/menu.php') ?>
         <div id="page-wrapper">
            <div class="container-fluid">
               <!-- Page Heading -->
               <div class="row" id="main">               
                  <div class="col-md-12 well">                    
                    <div class="col-md-12">
                       <h3 class="rlk">TAXA MANUTENÇÃO</h3>
                    </div>

                    <div class="col-md-12">
                      <?php
                        // Cabeçalho de navegação
                        include('includes/cabecalho-contas-a-receber.php'); 
                      ?>
                    </div>







                      <div class="col-md-12 well">
                           <form id="FormTaxaManu" enctype="multipart/form-data"  method="POST"  >
                              <input type="hidden" name="cad_matricula_manu" id="matricula_operador_manu">

                              <div class="col-md-2 cl_manu">
                                <span>Matrícula</span><b style="color:red">*</b>
                                <input autocomplete="off" id="busca_matricula_manu" placeholder="000000000" class="form-control buscaMatManu"  type="text">
                                <br>
                              </div>
                            <div class="col-md-4 cl_manu">
                              <span>Cooperado</span><b style="color:red">*</b>
                              <input autocomplete="off" onkeyup="BuscaOperadorManu()" onkeydown="verificaOperadorManu()" id="busca_operador_manu" placeholder="Ex: João da Silva" class="form-control" type="text">
                            </div>
                            <div class="col-md-10">
                              <!-- div vazia pra ficar bunitin -->
                            </div>

                            <div style="margin-top:10px; padding:0" class="col-md-6">
                              <!-- Auto Complete -->
                              <div style="display:none" id="lista_manu" class="history_list">
                                <div class="history_item_name">     
                                  <li class="listagem">    
                                  </li>
                                </div>
                              </div>

                            <!-- Chip View -->
                            <span id="chipview-manu" class="chipview">
                            <span id="txt_name_prepo_manu" class="txt_name_prepo"></span>
                            <button onClick="closeChipManu(this.value)" value="" type="button" id="close-manu" class="close-chip">
                             <b class="fa fa-close"></b>
                            </button>
                            </span>
                            </div>

                            
                            <div  style="margin-top:20px; padding:0" class="col-md-12">
                              <!-- vazio para ficar bonito -->
                            </div>
                              <div  class="col-md-2">
                                 <span>Título</span>
                                 <input  id="titulo_manu" name="titulo_manu" placeholder="Título"value="" class="form-control" type="text">
                                 <br>
                              </div>
                              <div  class="col-md-3">
                                 <span>Valor</span>
                                 <input id="valor_manu" name="valor_manu"  maxlength="10" data-thousands="." data-decimal="," placeholder="0,00" value="" class="form-control" type="text">
                                 <br>
                              </div>
                              <div class="col-md-2">
                                 <span>Data de vencimento</span>
                                 <input id="data_manu" name="data_manu" autocomplete="off" placeholder="01/01/2018" value="" class="form-control" type="text">
                                 <br>
                              </div>
                              
                              <div class="col-md-3">
                                 <span>Retorno da Manuntenção</span>
                                 <input  id="retorno_manu" name="retorno_manu" autocomplete="off" placeholder="01/01/2018" value="" class="form-control" type="text">
                                 <br>
                              </div>
                              <div class="col-md-2">
                                 <span>Status</span>
                                 <select class="form-control" name="status_manu" id="status_manu">
                                    <option value="Operando">Operando</option>
                                    <option value="Estoque">Estoque</option>
                                    <option value="Bloqueada">Bloqueada</option>
                                    <option value="Manutenção">Manutenção</option>
                                    <option value="Inutilizada">Inutilizada</option>
                                 </select>
                              </div>
                              <div class="col-md-3">
                                 <span>Nº do doc de origem</span>
                                 <input  id="origem_manu" name="origem_manu" placeholder="111-222-333" value="" class="form-control" type="text">
                                 <br>
                              </div>
                              <div class="col-md-4" >
                                 <span>Arquivo</span>
                                 <input  id="arquivo" name="arquivo" class="form-control" type="file">
                                 <br>
                              </div>
                              
                              <div class="col-md-12" style="margin-top:20px;">
                                 <button  id="upload" class="btn btn-warning pull-right" onclick="InsereManu()">CADASTRAR</button>
                              </div>
                           </form>
                        </div>



  <!-- FORMULARIO DE EDIÇÃO -->

                  </div>

                  <div class="col-md-12 well">
                   <div class="col-md-12">
                        <h3 class="rlk">TAXA MANUTENÇÃO</h3>
                     </div>
                     <div class="col-md-12">
                        <div class="col-md-12">
                      <table id="example" class="display nowrap" style="width:100%">
                         <thead>
                               <tr>
                                  <th>Título</th>
                                  <th>Nome</th>
                                  <th>Vencimento</th>
                                  <th>Numero Documento</th>
                                  <th>Documento de Origem</th>
                                  <th>Retorno Manutenção</th>
                                  <th>Status</th>
                                  <th>Valor</th>
                                  <th>Status Pagamento</th>
                                  
                              </tr>
                          </thead>
                          <?php 
                   
                    // $id = $_GET['id_apagar'];
                    $dados = $class->Select("*","cadastro_taxa_manu","","");


                 
                   
                   
                   while($row = $dados->fetch(PDO::FETCH_OBJ)){
                     $dadosCoop = $class->SelectEsp("nome","cooperados","WHERE id = '$row->id_cooperado'");
                        
                         // Data de nascimento formatada
                        $format = new DateTime($row->data_vencimento);
                        $vencimento = $format->format('d/m/Y');

                         // Data de nascimento formatada
                        $format = new DateTime($row->retorno_manutencao);
                        $retorno = $format->format('d/m/Y');


                    

                 

            ?>
            <tr>
                <td><?= $row->titulo ?></td>
                <td><?= utf8_encode($dadosCoop) ?></td>
                <td><?= $vencimento ?></td>
                <td><?= $row->numero_documento ?></td>
                <td><?= $row->documento_origem ?></td>
                <td><?= $retorno ?></td> 
                <td><?= $row->status ?></td>             
                <td><?= $row->valor ?></td>
                       <td>
                   <div class="input-group">
                      
                    <div id="radioBtn" class="btn-group">
                    <a <?php if($row->status_pag == 1){echo 'class="btn btn-primary btn-sm active"';}?> class="btn btn-primary btn-sm notActive" data-toggle="status" data-title="1" value="1" onclick="Status(<?= $row->id_manu ?>, 1)">Pago</a>
               
                     <a <?php if($row->status_pag == 0){echo 'class="btn btn-primary btn-sm active"';}?>  class="btn btn-primary btn-sm notActive" data-toggle="status" data-title="0" value="0" onclick="Status(<?= $row->id_manu ?>, 0)">Aberto</a>
                  </div> 
                </td>
                
            </tr>
            <?php
                }
             ?>
                          <tfoot>
                              <tr>
                                  <th>Título</th>
                                  <th>Nome</th>
                                  <th>Vencimento</th>
                                  <th>Numero Documento</th>
                                  <th>Documento de Origem</th>
                                  <th>Retorno Manutenção</th>
                                  <th>Status</th>
                                  <th>Valor</th>
                                  <th>Status Pagamento</th>
                              </tr>
                          </tfoot>
                      </table>
                  </div>
                  </div>

            </div>




          </div>
        </div>
      </div>
    </div>
       <script type="text/javascript">        
         $(document).ready(function() {
           var table = $('#example').DataTable( {
           rowReorder: {
              selector: 'td:nth-child(2)'
           },
           responsive: true,
            "language": {
               "url": "https://cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
            }
           });
         });
      </script>
      <script type="text/javascript" src="http://<?= $server ?>/admin/_class/caminho_controler.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/_app/_financeiro/js/id-taxa-manu.js"></script>
       <script type="text/javascript" src="http://<?= $server ?>/_app/_financeiro/js/update-status-taxa-manu.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/menu-mobile.js"></script>
      
   </body>
   </html>
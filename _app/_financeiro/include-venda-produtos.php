<?php /*autenticador*/ include('../../admin/autenticador.php'); ?>
<?php /*controlador*/ include('../../admin/controler_sys.php'); ?>
<!DOCTYPE html>
<html lang="pt-br">
  <head>
      <meta charset="utf-8">
      <meta name="robots" content="noindex, nofollow">
      <title><?php include('../../includes/title.php'); ?></title>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- FAV ICON -->
      <link rel="icon" type="image/png" href="http://<?= $server ?>/img/fav.png" />
        <!-- CSS -->
      <link rel="stylesheet" href="http://<?= $server ?>/css/bootstrap3.3.0.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/estilo.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/fontawesome.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/responsive.dataTables.min.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/jquery.dataTables.min.css">
      <!-- JAVASCRIPTS --> 


      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.js"></script>
     

      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.dataTables.min.js"></script>
        
      <script type="text/javascript" src="http://<?= $server ?>/js/dataTable.responsive.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/bootstrap330.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/sweet-alert.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.mask.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.maskMoney.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/datapicker.js"></script>
   </head>
 <style>
      .well {
      min-height: 20px;
      padding: 19px;
      margin-bottom: 20px;
      background-color: #ffffff;
      border: 1px solid #e3e3e3;
      border-radius: 4px;
      -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
      box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
      }
      label {
      width: auto;
      height: 75px;;
      border-radius: 1px;
      border-right: 1px solid #e2e2e2;
      }
      
      @media (max-width: 748px){
      label {
      width: 33%;
      height: 100px;
      border-radius: 1px;
      border-right: 1px solid #c1c1c1;
      }
      label span {
      display: block;
      }
      }
      .row{
      margin-top: 3rem;
      }
   </style>
 <body>
      <div id="throbber" style="display:none; min-height:120px;"></div>
      <div id="noty-holder"></div>
      <div id="wrapper">
         <!-- Navigation -->
        <?php include('../../includes/menu.php') ?>
         <div id="page-wrapper">
            <div class="container-fluid">
               <!-- Page Heading -->
               <div class="row" id="main">               
                  <div class="col-md-12 well">                    
                    <div class="col-md-12">
                       <h3 class="rlk">TAXA ADESÃO</h3>
                    </div>

                    <div class="col-md-12">
                      <?php
                        // Cabeçalho de navegação
                        include('includes/cabecalho-contas-a-receber.php'); 
                      ?>
                    </div>



                        <div class="col-md-12 well">
                           <form method="POST" id="FormVendaProd">
                              <input type="hidden" name="cad_matricula_venda" id="matricula_operador_venda">

                              <div class="col-md-2 cl_venda">
                                <span>Matrícula</span><b style="color:red">*</b>
                                  <input autocomplete="off" id="busca_matricula_venda" placeholder="000000000" class="form-control buscaMatVenda" type="text">
                                  <br>
                             </div>
                             <div class="col-md-4 cl_venda">
                                <span>Cooperado</span><b style="color:red">*</b>
                                <input autocomplete="off" onkeyup="BuscaOperadorVenda()" onkeydown="verificaOperadorVenda()" id="busca_operador_venda" placeholder="Ex: João da Silva" class="form-control" type="text">
                              </div>
                              <div class="col-md-10">
                                <!-- layout -->
                              </div>

                              <div style="margin-top:10px; padding:0" class="col-md-6">
                                  <!-- Auto Complete -->
                              <div style="display:none" id="lista_venda" class="history_list">
                                  <div class="history_item_name">     
                                    <li class="listagem">    
                                    </li>
                                  </div>
                              </div>

                              <!-- Chip View -->
                              <span id="chipview-venda" class="chipview">
                              <span id="txt_name_prepo_venda" class="txt_name_prepo"></span>
                              <button onClick="closeChipVenda(this.value)" value="" type="button" id="close-venda" class="close-chip">
                              <b class="fa fa-close"></b>
                              </button>
                              </span>
                              </div>

                              <script type="text/javascript">
                                  // Mascara para aceitar apenas numeros na matricula
                                  $('#busca_matricula_venda').mask('00000000');  
                              </script>

                              <div style="margin-top:20px; padding:0" class="col-md-12">
                                <!-- layout -->
                              </div>
                              <div class="col-md-3">
                                 <span>Nome do produto</span>
                                 <input id="produto" name="produto" value="" placeholder="Nome do Produto" class="form-control" type="text">
                                 <br>
                              </div>
                              <div class="col-md-4">
                                 <span>Descrição</span>
                                 <input id="desc" name="desc" value="" class="form-control" type="text">
                                 <br>
                              </div>
                              <div class="col-md-2">
                                 <span>Valor unitário</span>
                                 <input id="valorU" name="valorU" data-thousands="." data-decimal="," placeholder="0,00" value=""  class="form-control" type="text" maxlength="10">
                                 <br>
                              </div>
                              <div class="col-md-2">
                                 <span>Quantidade</span>
                                 <input id="qtd" name="qtd" value="" class="form-control" type="text">
                                 <br>
                              </div>
                              <div class="col-md-2">
                                 <span>Prazo de pagamento</span>
                                 <input id="prazo" name="prazo" placeholder="01/01/2018" value="" class="form-control" type="text">
                              </div>
                              <div class="col-md-2">
                                 <span>Parcelas</span>
                                 <select onChange="GeraDoc(this.value)" name="parcelaV" class="form-control" id="parcelaV">
                                    <?php
                                       for ($i=1; $i<= 12; $i++){
                                       
                                       ?>
                                    <option value="<?= $i ?>"><?= $i.'x' ?></option>
                                    <?php
                                       }
                                       ?>
                                 </select>
                              </div>
                              <script type="text/javascript">
                                 function GeraDoc(parcela){
                                   
                                   var recebe2 = "";
                                   var recebe3 = "";
                                   for(i=1; i<=parcela; i++){
                                    
                                     recebe2 = recebe2+"<span>Valor</span><input  class='form-control valor-parcelas' name='valorP[]' "+i+"data-thousands='.' data-decimal=','placeholder='0,00' value=''  type='text' maxlength='10' ><br>";
                                      recebe3 = recebe3+"<span>Vencimento</span><input name='vencimentoB[]' class='form-control vencimento-venda' placeholder='01/01/2018'"+i+" value=''  type='text' ><br>";
                                 
                                   }
                                   
                                   $("#recebe_campos2").html(recebe2);
                                   $("#recebe_campos3").html(recebe3);
                                   $('.vencimento-venda').mask('00/00/0000');
                                   $('.vencimento-venda').datepicker({format: 'dd/mm/yyyy'})
                                   
                                   $('.valor-parcelas').maskMoney();
                                   
                                   $('.documento-venda').mask('000-PRO00000');
                                 
                                   
                                 }
                                 
                              </script>
                             
                              <div class="col-md-12">
                                 <br>
                                 <hr>
                                 <br>
                              </div>
                             <!--  <div class="col-md-2"id="recebe_campos" >
                                 <span>Nº do Documento</span>
                                 <input  name="documento_venda" placeholder="999-PRO99999" value="" class="form-control documento-venda" type="text" readonly="readonly">
                              </div> -->
                              <div class="col-md-2" id="recebe_campos2" >
                                 <span>Valor</span>
                                 <input  name="valorP" data-thousands="." data-decimal="," placeholder="0,00" value="" class="form-control valor-parcelas" type="text" maxlength="10">
                              </div>
                              <div class="col-md-2" id="recebe_campos3">
                                 <span>Vencimento</span>
                                 <input  name="vencimentoB" placeholder="01/01/2018" value="" class="form-control vencimento-venda" type="text">
                              </div>
                           </form>
                           <div class="col-md-12">
                              <br>
                              <hr>
                           </div>
                           <div class="col-md-2">
                              <span>&nbsp</span>
                              <button class="btn btn-warning pull-right" onclick="InsereVenda()">CADASTRAR</button>
                           </div>
                        </div>
                        <!-- FORMULARIO DE EDIÇÃO -->

                  </div>
              <div class="col-md-12 well">
                   <div class="col-md-12">
                        <h3 class="rlk">VENDA DE PRODUTOS</h3>
                     </div>
                     <div class="col-md-12">
                        <div class="col-md-12">
                      <table id="example" class="display nowrap" style="width:100%">
                          <thead>
                               <tr>
                                  <th>Nome Produto</th>
                                  <th>Descrição</th>
                                  <th>Nome</th>
                                  <th>Prazo Pagamento</th>
                                  <th>Parcelas</th>
                                  <th>Quantidade</th>
                                  <th>Valor Unitário</th>
                                  <th>#</th>

                                  
                              </tr>
                          </thead>
                          <tbody>
                          <?php 
                   
                    // $id = $_GET['id_apagar'];
                    $dados = $class->Select("*","venda_produtos","","");


                 
                   
                   
                   while($row = $dados->fetch(PDO::FETCH_OBJ)){
                     $dadosCoop = $class->SelectEsp("nome","cooperados","WHERE id = '$row->id_cooperado'");
                         // Data de nascimento formatada
                        $format = new DateTime($row->prazo_pagamento);
                        $prazo = $format->format('d/m/Y');


                    

                 

            ?>
            <tr>
                <td><?= ($row->nome_produto) ?></td>
                <td><?= ($row->desc_produto) ?></td>
                <td><?= utf8_encode($dadosCoop) ?></td>
                <td><?= $prazo ?></td>
                <td><?= $row->parcelas ?></td>
                <td><?= $row->quantidade ?></td>
                <td><?= $row->valor_und ?></td> 
                <td><div onclick="lupz(<?= $row->id_prod ?>)" class="lupa"></div></td>
               
                
            </tr>
            <?php
                }
             ?>
           </tbody>
                          <tfoot>
                              <tr>
                                  <th>Nome Produto</th>
                                  <th>Descrição</th>
                                  <th>Nome</th>
                                  <th>Prazo Pagamento</th>
                                  <th>Parcelas</th>
                                  <th>Quantidade</th>
                                  <th>Valor Unitário</th>
                                  <th>#</th>
                              </tr>
                          </tfoot>
                      </table>
                 </div>
                  </div>

            </div>




          </div>
        </div>
      </div>
    </div>
       <script type="text/javascript">
       function lupz(id){
      window.location.href = 'detalhe-venda/'+id;
      }
    </script>
    
      <script type="text/javascript">
         
         $(document).ready(function() {
           var table = $('#example').DataTable( {
           rowReorder: {
              selector: 'td:nth-child(2)'
           },
           responsive: true,
            "language": {
               "url": "https://cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
            }
           });
         });
      </script>
      <script type="text/javascript" src="http://<?= $server ?>/admin/_class/caminho_controler.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/_app/_financeiro/js/id-venda-produtos.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/menu-mobile.js"></script>
   </body>
   </html>

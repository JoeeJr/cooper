// autocomplet : this function will be executed every time we change the text
var host = location.host;
var server = "";
if (host == 'localhost'){
    server = host+'/'+pasta;
}else{
    server = host;
}


      
     
                            

        // Mascaras nos campos
        $(document).ready(function(){ 
        	$('#busca_matricula_manu').mask('0000');


        	$('#valor_manu').maskMoney();      
        //  ID do campo + sequencia que o campo deve adotar
            $('#documento_manu').mask('000-MAN00000');

        //  Telefone Celular
  
        	$('#origem_manu').mask('000-000-000');
   
          // Data de Nascimento
        $('#data_manu')
          .datepicker({
            format: 'dd/mm/yyyy'
          })
           $('#retorno_manu')
          .datepicker({
            format: 'dd/mm/yyyy'
          })
        });




// ########################## BUSCA DE OPERADOR TAXA manu ######################


// Busca o operador pela matricula
$('#busca_matricula_manu').blur(function(){
  var id = $('#busca_matricula_manu').val();
  if (id != ''){
    
    swal("Aguarde...!", {
      closeOnClickOutside: false,
      buttons: false
    });
  
    $.ajax({
      url: 'http://'+server+'/_app/class_api/busca-matricula.class.php',
      type: 'POST',
      dataType: 'json',
      data: {matricula:id},
      success:function(data){
        //alert(data);
        if (data == 'not_find'){
          $('#busca_matricula_manu').val('');

          swal({
              title: 'Cooperado não encontrado!',
              icon: 'warning',
              confirmButtonColor: '#3085d6',
              confirmButtonText: 'Ok'
            })
        }else{
          // Fecha a modal
          swal.close();

          // Seta o nome do operador no campo
          $('#busca_operador_manu').val(data[1]);

          // ID do operador
         
          $('#matricula_operador_manu').val(data[0]);

          // Oculta a busca
          $('.cl_manu').hide();

          // Mostra o chipview
          $('#chipview-manu').show();

          // Seta o nome do cooperado no chipview
          var str = "("+data[0]+") - "+data[1];
          // if(str.length > 23) str = str.substring(0,23)+'...';
          $('#txt_name_prepo_manu').html(str);

          // Seta o valor do ID no button para fechar depois o chipview
          $('#close-manu').val(data[0]);

          // hide proposition list
          $('#lista_manu').hide();
        }
      }
    });
  }
});


// ########################## BUSCA DE OPERADOR DE PDV ######################


function BuscaOperadorManu() {
  var inpBusca = $('#busca_operador_manu').val();
  

  var min_length = 0; // min caracters to display the autocomplete
  var keyword = $('#busca_operador_manu').val();
  if (keyword.length >= min_length) {

    if (inpBusca == '') {
      $('#lista_manu').hide();
    }else{

      $.ajax({
        url: 'http://'+server+'/_app/class_api/busca-operador.class.php',
        type: 'POST',
        data: {keyword:keyword},
        success:function(data){
          $('#lista_manu').show();
          

          if (data == ''){
            var estrutura = "<li class='listagem_nenhum'>Nenhum resultado encontrado</li>";
            $('#lista_manu').html(estrutura);
          }else{
            $('#lista_manu').html(data);
          }
        }
      });
    }


  } else {
    $('#lista_manu').hide();
  }
}

function verificaOperadorManu(){
  var inpBusca = $('#busca_operador_manu').val();
  if (inpBusca == '') {
    $('.listagem').hide();
  }
}

// set_item : this function will be executed when we select an item
function set_item_operador(item, item2) {
  // Seta o nome do operador no campo
  $('#busca_operador_manu').val(item);

  // ID do operador
  $('#matricula_operador_manu').val(item2);

  // Oculta a busca
  $('.cl_manu').hide();

  // Mostra o chipview
  $('#chipview-manu').show();

  // Seta o nome do cooperado no chipview
  var str = "("+item2+") - "+item;
  // if(str.length > 23) str = str.substring(0,23)+'...';
  $('#txt_name_prepo_manu').html(str);

  // Seta o valor do ID no button para fechar depois o chipview
  $('#close-manu').val(item2);

  // hide proposition list
  $('#lista_manu').hide();
}

function closeChipManu(id){
  // Oculta o chipview
  $('#chipview-manu').hide();

  // Zera o valor do ID no input
  
  $('#matricula_operador_manu').val('');
  $('#busca_operador_manu').val('');
  $('#busca_matricula_manu').val('');

  // Exibe o campo de busca vazio
  $('.cl_manu').val('');
  $('.cl_manu').show();
}


// ########################## INSERT BANCO CADASTRO TAXA manu ######################


function InsereManu(){


  var matricula = $('#busca_matricula_manu2').val();
  var nome = $('#busca_operador_manu2').val();
  var data = $('#data_manu').val();
  
  var valor_manu = $('#valor_manu').val();
  var retorno = $('#retorno_manu').val();
  var status = $('#status_manu').val();
  var origem = $('#origem_manu').val();
  var arquivo = $('#arquivo').val();

  
 
  


  
  
  


  if (matricula != '' && nome != '' && valor_manu != '' && data != '' &&  retorno != '' && status != '' && origem != ''  ){
        swal("Aguarde...!", {
      closeOnClickOutside: false,
      buttons: false});


     // Serializa o form
     var form = $('#FormTaxaManu')[0];
    var formTaxaManu = new FormData(form);
    
  
    $.ajax({

        url: 'http://'+server+'/_app/_financeiro/class/insert-cadastro-taxa-manu.class.php',
        type: "POST",
        data: formTaxaManu,

        contentType: false,
         
        processData: false,
         
        success: function(data){
         //alert(data);
          
          if (data.trim() == 'ok'){
            swal({
              title: "Taxa cadastrada com sucesso!",
              icon: "success",
            }).then((value) => {
                  window.location.reload();
                });

          }else{

             swal({
              title: 'Verifique as informações e tente novamente!',
              icon: 'error',
              confirmButtonColor: '#3085d6',
              confirmButtonText: 'Ok'
             })
          }
        },
                 
      });
  
  }else{
   swal({
        title: 'Preencha todos os campos',
        icon: 'warning',
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Ok'
    });

    

}

};


// $('#upload').click(function(){
//     var form = document.getElementById('FormTaxaManu');
    
//       $.ajax({

//         url: 'http://'+server+'/_app/_financeiro/arquivo-taxa-manutencao.php',
//         type: "POST",
//         data:  new FormData(form),
//         processData:false,
//         contentType: false,
//         cache: false,
//         processData:false,
//         success: function(data){
//          alert(data);
//             swal({
//               title: "Taxa cadastrada com sucesso!",
//               icon: "success",
//             }).then((value) => {
//                   window.location.reload();
//                 });


//           }
//         })         
//       });

// $('#upload').on('click', function() {
//     var file_data = $('#arquivo').prop('files')[0];   
//     var form_data = new FormData();                  
//     form_data.append('file', file_data);
//     alert(form_data);                             
//     $.ajax({
//         url: 'http://'+server+'/_app/_financeiro/arquivo-taxa-manutencao.php', // point to server-side PHP script 
//         dataType: 'text',  // what to expect back from the PHP script, if anything
//         cache: false,
//         contentType: false,
//         processData: false,
//         data: form_data,                         
//         type: 'post',
//         success: function(php_script_response){
//             alert(php_script_response); // display response from the PHP script, if any
//         }
//      });
// });




// autocomplet : this function will be executed every time we change the text
var host = location.host;
var server = "";
if (host == 'localhost'){
    server = host+'/'+pasta;
}else{
    server = host;
}


      
     

        // Mascaras nos campos
        $(document).ready(function(){ 

        	$('#busca_matricula_venda').mask('0000');
        	$('#valorU').maskMoney(); 
        	$('.valor-parcelas').maskMoney();     
        //  ID do campo + sequencia que o campo deve adotar
            $('.documento-venda').mask('000-PRO00000');
            $('.vencimento-venda').mask('00/00/0000');
            $('#qtd').mask('00000');
            $('#prazo').mask('00/00/0000');

        //  Telefone Celular
  
        	
   
          // Data de Nascimento
        $('#prazo')
          .datepicker({
            format: 'dd/mm/yyyy'
          })
          $('.vencimento-venda')
          .datepicker({
            format: 'dd/mm/yyyy'
          })
           
        });


// Busca o operador pela matricula
$('#busca_matricula_venda').blur(function(){
  var id = $('#busca_matricula_venda').val();
  if (id != ''){


    swal("Aguarde...!", {
      closeOnClickOutside: false,
      buttons: false
    });


    $.ajax({
      url: 'http://'+server+'/_app/class_api/busca-matricula.class.php',
      type: 'POST',
      dataType: 'json',
      data: {matricula:id},
      success:function(data){
        if (data == 'not_find'){
          $('#busca_matricula_venda').val('');

          swal({
              title: 'Cooperado não encontrado!',
              icon: 'warning',
              confirmButtonColor: '#3085d6',
              confirmButtonText: 'Ok'
            })
        }else{
          // Fecha a modal
          swal.close();

          // Seta o nome do operador no campo
          $('#busca_operador_venda').val(data[1]);

          // ID do operador
          
          $('#matricula_operador_venda').val(data[0]);

          // Oculta a busca
          $('.cl_venda').hide();

          // Mostra o chipview
          $('#chipview-venda').show();

          // Seta o nome do cooperado no chipview
          var str = "("+data[0]+") - "+data[1];
          // if(str.length > 23) str = str.substring(0,23)+'...';
          $('#txt_name_prepo_venda').html(str);

          // Seta o valor do ID no button para fechar depois o chipview
          $('#close-adesap').val(data[0]);

          // hide proposition list
          $('#lista_venda').hide();
        }
      }
    });
  }
});


// ########################## BUSCA DE OPERADOR DE PDV ######################


function BuscaOperadorVenda() {
  var inpBusca = $('#busca_operador_venda').val();
  

  var min_length = 0; // min caracters to display the autocomplete
  var keyword = $('#busca_operador_venda').val();
  if (keyword.length >= min_length) {

    if (inpBusca == '') {
      $('#lista_venda').hide();
    }else{

      $.ajax({
        url: 'http://'+server+'/_app/class_api/busca-operador.class.php',
        type: 'POST',
        data: {keyword:keyword},
        success:function(data){
          $('#lista_venda').show();
          

          if (data == ''){
            var estrutura = "<li class='listagem_nenhum'>Nenhum resultado encontrado</li>";
            $('#lista_venda').html(estrutura);
          }else{
            $('#lista_venda').html(data);
          }
        }
      });
    }


  } else {
    $('#lista_venda').hide();
  }
}

function verificaOperadorVenda(){
  var inpBusca = $('#busca_operador_venda').val();
  if (inpBusca == '') {
    $('.listagem').hide();
  }
}

// set_item : this function will be executed when we select an item
function set_item_operador(item, item2) {
  // Seta o nome do operador no campo
  $('#busca_operador_venda').val(item);

  // ID do operador
  $('#matricula_operador_venda').val(item2);

  // Oculta a busca
  $('.cl_venda').hide();

  // Mostra o chipview
  $('#chipview-venda').show();

  // Seta o nome do cooperado no chipview
  var str = "("+item2+") - "+item;
  // if(str.length > 23) str = str.substring(0,23)+'...';
  $('#txt_name_prepo_venda').html(str);

  // Seta o valor do ID no button para fechar depois o chipview
  $('#close-venda').val(item2);

  // hide proposition list
  $('#lista_venda').hide();
}

function closeChipVenda(id){
  // Oculta o chipview
  $('#chipview-venda').hide();

  // Zera o valor do ID no input
 
  $('#matricula_operador_venda').val('');
  $('#busca_operador_venda').val('');
  $('#busca_matricula_venda').val('');

  // Exibe o campo de busca vazio
  $('.cl_venda').val('');
  $('.cl_venda').show();
}        

function InsereVenda(){
  var produto = $('#produto').val();
  var desc = $('#desc').val();
  var valorU = $('#valorU').val();
  var qtd = $('#qtd').val();
  var prazo = $('#prazo').val();
  var parcelasV = $('#parcelasV').val();
  var matricula = $('#busca_matricula4').val();
  var nome = $('#busca_operador4').val();
  var valorB = $('.valor-parcelas').val();
  var vencimentoB = $('.vencimento-venda').val();
  
 
  


  
  
  


  if ( vencimentoB != '' && produto != '' && desc != '' && qtd != '' && parcelasV != '' && valorB != '' && matricula != '' && nome != '' && valorU != '' && prazo != ''  ){
        swal("Aguarde...!", {
        closeOnClickOutside: false,
        buttons: false});


     // Serializa o form
    var formVendaProd = $('#FormVendaProd').serialize();
    $.ajax({

        url: 'http://'+server+'/_app/_financeiro/class/insert-cadastro-venda-produtos.class.php',
        type: "POST",
        data:  formVendaProd,
        success: function(data){
         // alert(data);
          
          if (data.trim() == 'ok'){
            swal({
              title: "Produto cadastrado com sucesso!",
              icon: "success",
            }).then((value) => {
                  window.location.reload();
                });

          }else{

             swal({
              title: 'Verifique as informações e tente novamente!',
              icon: 'error',
              confirmButtonColor: '#3085d6',
              confirmButtonText: 'Ok'
             })
          }
        }         
      });
  
  }else{
   swal({
        title: 'Preencha todos os campos',
        icon: 'warning',
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Ok'
    })

    

  }
}


      




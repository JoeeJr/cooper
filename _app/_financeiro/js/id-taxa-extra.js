// autocomplet : this function will be executed every time we change the text
var host = location.host;
var server = "";
if (host == 'localhost'){
    server = host+'/'+pasta;
}else{
    server = host;
}

 $(document).ready(function(){
                              // Mascara para aceitar apenas numeros na matricula
                              $('#busca_matricula_manu').mask('000');
                              $('#valorE').maskMoney();  
                              });




// Busca o operador pela matricula
$('#busca_matriculaE').blur(function(){
	var id = $('#busca_matriculaE').val();
	if (id != ''){


		swal("Aguarde...!", {
		  closeOnClickOutside: false,
		  buttons: false
		});


		$.ajax({
			url: 'http://'+server+'/_app/class_api/busca-matricula.class.php',
			type: 'POST',
			dataType: 'json',
			data: {matricula:id},
			success:function(data){
				if (data == 'not_find'){
					$('#busca_matriculaE').val('');

					swal({
				    	title: 'Cooperado não encontrado!',
				    	icon: 'warning',
				    	confirmButtonColor: '#3085d6',
				    	confirmButtonText: 'Ok'
				    })
				}else{
					// Fecha a modal
					swal.close();

					// Seta o nome do operador no campo
					$('#busca_operadorE').val(data[1]);

					// ID do operador
					$('#cad_matriculaE').val(data[0]);
					$('#matricula_operadorE').val(data[0]);

					// Oculta a busca
					$('.cl_extra').hide();

					// Mostra o chipview
					$('#chipview-extra').show();

					// Seta o nome do cooperado no chipview
					var str = "("+data[0]+") - "+data[1];
					// if(str.length > 23) str = str.substring(0,23)+'...';
					$('#txt_name_prepoE').html(str);

					// Seta o valor do ID no button para fechar depois o chipview
					$('#close-extra').val(data[0]);

					// hide proposition list
					$('#lista_operadorE').hide();
				}
			}
		});
	}
});


// ########################## BUSCA DE OPERADOR DE PDV ######################


function BuscaExtra() {
	var inpBusca = $('#busca_operadorE').val();
	

	var min_length = 0; // min caracters to display the autocomplete
	var keyword = $('#busca_operadorE').val();
	if (keyword.length >= min_length) {

		if (inpBusca == '') {
			$('#lista_operadorE').hide();
		}else{

			$.ajax({
				url: 'http://'+server+'/_app/class_api/busca-operador.class.php',
				type: 'POST',
				data: {keyword:keyword},
				success:function(data){
					$('#lista_operadorE').show();
					

					if (data == ''){
						var estrutura = "<li class='listagem_nenhum'>Nenhum resultado encontrado</li>";
						$('#lista_operadorE').html(estrutura);
					}else{
						$('#lista_operadorE').html(data);
					}
				}
			});
		}


	} else {
		$('#lista_operadorE').hide();
	}
}

function verificaExtra(){
	var inpBusca = $('#busca_operadorE').val();
	if (inpBusca == '') {
		$('.listagem').hide();
	}
}

// set_item : this function will be executed when we select an item
function set_item_operador(item, item2) {
	// Seta o nome do operador no campo
	$('#busca_operadorE').val(item);

	// ID do operador
	$('#matricula_operadorE').val(item2);

	// Oculta a busca
	$('.cl_extra').hide();

	// Mostra o chipview
	$('#chipview-extra').show();

	// Seta o nome do cooperado no chipview
	var str = "("+item2+") - "+item;
	// if(str.length > 23) str = str.substring(0,23)+'...';
	$('#txt_name_prepoE').html(str);

	// Seta o valor do ID no button para fechar depois o chipview
	$('#close-extra').val(item2);

	// hide proposition list
	$('#lista_operadorE').hide();
}

function closeChipE(id){
	// Oculta o chipview
	$('#chipview-extra').hide();

	// Zera o valor do ID no input
	$('#matricula_operadorE').val('');
	$('#matricula_operadorE').val('');
	$('#busca_operadorE').val('');
	$('#busca_matriculaE').val('');

	// Exibe o campo de busca vazio
	$('.cl_extra').val('');
	$('.cl_extra').show();
}


// ########################## INSERT BANCO CADASTRO TAXA ADM ######################


function InsereExtra(){
  var matricula = $('#matricula_operadorE').val();
  var parcelas = $('#parcelasE').val();
  var data = $('#vencimentoE').val();
  var descricao = $('#descricaoE').val();
  var valor = $('#valorE').val();
  
 
  if (matricula != '' &&  valor != '' && data != '' &&  descricao != '' && parcelas != ''  ){
   			
   			swal("Aguarde...!", {
		  	closeOnClickOutside: false,
		  	buttons: false
				});

     // Serializa o form
    var formTaxaExtra = $('#FormTaxaExtra').serialize();
    $.ajax({

        url: 'http://'+server+'/_app/_financeiro/class/insert-cadastro-taxa-extra.class.php',
        type: "POST",
        data:  formTaxaExtra,
        success: function(data){
         // alert(data);
          
          if (data.trim() == 'ok'){
            swal({
              title: "Taxa Extra cadastrada com sucesso!",
              icon: "success",
            }).then((value) => {
                  window.location.reload();
                });

          }else{

             swal({
              title: 'Verifique as informações e tente novamente!',
              icon: 'error',
              confirmButtonColor: '#3085d6',
              confirmButtonText: 'Ok'
             })
          }
        }         
      });
  
  }else{
   swal({
        title: 'Preencha todos os campos',
        icon: 'warning',
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Ok'
    })

    

}
}
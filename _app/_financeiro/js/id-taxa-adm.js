// autocomplet : this function will be executed every time we change the text
var host = location.host;
var server = "";
if (host == 'localhost'){
    server = host+'/'+pasta;
}else{
    server = host;
}



      
     

        // Mascaras nos campos
       $(document).ready(function(){

        	$('#busca_matricula').mask('0000'); 


        	$('#valor_adm').maskMoney();      
        	//  ID do campo + sequencia que o campo deve adotar
            $('#documento_adm').mask('000-000-000');
            $('#data_adm').mask('00/00/0000');

        	//  Telefone Celular
  

   
          	// Data de Nascimento
	        $('#data_adm')
	          .datepicker({
	            format: 'dd/mm/yyyy'
	          })
        });




// Busca o operador pela matricula
$('#busca_matricula').blur(function(){
	var id = $('#busca_matricula').val();
	if (id != ''){


		swal("Aguarde...!", {
		  closeOnClickOutside: false,
		  buttons: false
		});


		$.ajax({
			url: 'http://'+server+'/_app/class_api/busca-matricula.class.php',
			type: 'POST',
			dataType: 'json',
			data: {matricula:id},
			success:function(data){
				if (data == 'not_find'){
					$('#busca_matricula').val('');

					swal({
				    	title: 'Cooperado não encontrado!',
				    	icon: 'warning',
				    	confirmButtonColor: '#3085d6',
				    	confirmButtonText: 'Ok'
				    })
				}else{
					// Fecha a modal
					swal.close();

					// Seta o nome do operador no campo
					$('#busca_operador').val(data[1]);

					// ID do operador
					$('#matricula').val(data[0]);
					$('#matricula_operador').val(data[0]);

					// Oculta a busca
					$('.cl_operador').hide();

					// Mostra o chipview
					$('#chipview-operador').show();

					// Seta o nome do cooperado no chipview
					var str = "("+data[0]+") - "+data[1];
					// if(str.length > 23) str = str.substring(0,23)+'...';
					$('#txt_name_prepo2').html(str);

					// Seta o valor do ID no button para fechar depois o chipview
					$('#close-chip2').val(data[0]);

					// hide proposition list
					$('#lista_operador').hide();
				}
			}
		});
	}
});


// ########################## BUSCA DE OPERADOR DE PDV ######################


function BuscaOperador() {
	var inpBusca = $('#busca_operador').val();
	

	var min_length = 0; // min caracters to display the autocomplete
	var keyword = $('#busca_operador').val();
	if (keyword.length >= min_length) {

		if (inpBusca == '') {
			$('#lista_operador').hide();
		}else{

			$.ajax({
				url: 'http://'+server+'/_app/class_api/busca-operador.class.php',
				type: 'POST',
				data: {keyword:keyword},
				success:function(data){
					$('#lista_operador').show();
					

					if (data == ''){
						var estrutura = "<li class='listagem_nenhum'>Nenhum resultado encontrado</li>";
						$('#lista_operador').html(estrutura);
					}else{
						$('#lista_operador').html(data);
					}
				}
			});
		}


	} else {
		$('#lista_operador').hide();
	}
}

function verificaOperador(){
	var inpBusca = $('#busca_operador').val();
	if (inpBusca == '') {
		$('.listagem').hide();
	}
}

// set_item : this function will be executed when we select an item
function set_item_operador(item, item2) {
	// Seta o nome do operador no campo
	$('#busca_operador').val(item);

	// ID do operador
	$('#matricula_operador').val(item2);

	// Oculta a busca
	$('.cl_operador').hide();

	// Mostra o chipview
	$('#chipview-operador').show();

	// Seta o nome do cooperado no chipview
	var str = "("+item2+") - "+item;
	// if(str.length > 23) str = str.substring(0,23)+'...';
	$('#txt_name_prepo2').html(str);

	// Seta o valor do ID no button para fechar depois o chipview
	$('#close-chip2').val(item2);

	// hide proposition list
	$('#lista_operador').hide();
}

function closeChip2(id){
	// Oculta o chipview
	$('#chipview-operador').hide();

	// Zera o valor do ID no input
	$('#matricula').val('');
	$('#matricula_operador').val('');
	$('#busca_operador').val('');
	$('#busca_matricula').val('');

	// Exibe o campo de busca vazio
	$('.cl_operador').val('');
	$('.cl_operador').show();
}


// ########################## INSERT BANCO CADASTRO TAXA ADM ######################


function InsereAdm(){
  var matricula = $('#matricula_operador').val();
  
  var data = $('#data_adm').val();
  
  var valor_adm = $('#valor_adm').val();
  
 
  


  
  
  


  if (matricula != '' &&    data != '' && valor_adm != ''  ){
   			
   			swal("Aguarde...!", {
		  	closeOnClickOutside: false,
		  	buttons: false
				});
 	
     // Serializa o form
    var formTaxaAdm = $('#FormTaxaAdm').serialize();
    $.ajax({

        url: 'http://'+server+'/_app/_financeiro/class/insert-cadastro-taxa-adm.class.php',
        type: "POST",
        data:  formTaxaAdm,
        success: function(data){
          //alert(data);
          
          if (data.trim() == 'ok'){
            swal({
              title: "Taxa cadastrada com sucesso!",
              icon: "success",
            }).then((value) => {
                 window.location.reload();
                });

          }else{

             swal({
              title: 'Verifique as informações e tente novamente!',
              icon: 'error',
              confirmButtonColor: '#3085d6',
              confirmButtonText: 'Ok'
             })
          }
        }         
      });
  
  }else{
   swal({
        title: 'Preencha todos os campos',
        icon: 'warning',
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Ok'
    })

    

}
}



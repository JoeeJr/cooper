// autocomplet : this function will be executed every time we change the text
var host = location.host;
var server = "";
if (host == 'localhost'){
    server = host+'/'+pasta;
}else{
    server = host;
}


      
     
        // Mascaras nos campos
        $(document).ready(function(){ 
        	$('#busca_matricula_adesao').mask('0000');



        	$('#valor_adesao').maskMoney();      
        //  ID do campo + sequencia que o campo deve adotar
            $('#documento_adesao').mask('000-ADS00000');

        //  Telefone Celular
  
        	$('#origem_adesao').mask('000-000-000');
          $('#data_adesao').mask('00/00/0000');  
          // Data de Nascimento
        $('#data_adesao')
          .datepicker({
            format: 'dd/mm/yyyy'
          })
        
        });


// Busca o operador pela matricula
$('#busca_matricula_adesao').blur(function(){
  var id = $('#busca_matricula_adesao').val();
  if (id != ''){


    swal("Aguarde...!", {
      closeOnClickOutside: false,
      buttons: false
    });


    $.ajax({
      url: 'http://'+server+'/_app/class_api/busca-matricula.class.php',
      type: 'POST',
      dataType: 'json',
      data: {matricula:id},
      success:function(data){
        if (data == 'not_find'){
          $('#busca_matricula_adesao').val('');

          swal({
              title: 'Cooperado não encontrado!',
              icon: 'warning',
              confirmButtonColor: '#3085d6',
              confirmButtonText: 'Ok'
            })
        }else{
          // Fecha a modal
          swal.close();

          // Seta o nome do operador no campo
          $('#busca_operador_adesao').val(data[1]);

          // ID do operador
          $('#matricula').val(data[0]);
          $('#matricula_operador_adesao').val(data[0]);

          // Oculta a busca
          $('.cl_adesao').hide();

          // Mostra o chipview
          $('#chipview-adesao').show();

          // Seta o nome do cooperado no chipview
          var str = "("+data[0]+") - "+data[1];
          // if(str.length > 23) str = str.substring(0,23)+'...';
          $('#txt_name_prepo_adesao').html(str);

          // Seta o valor do ID no button para fechar depois o chipview
          $('#close-adesap').val(data[0]);

          // hide proposition list
          $('#lista_adesao').hide();
        }
      }
    });
  }
});


// ########################## BUSCA DE OPERADOR DE PDV ######################


function BuscaOperadorAdesao() {
  var inpBusca = $('#busca_operador_adesao').val();
  

  var min_length = 0; // min caracters to display the autocomplete
  var keyword = $('#busca_operador_adesao').val();
  if (keyword.length >= min_length) {

    if (inpBusca == '') {
      $('#lista_adesao').hide();
    }else{

      $.ajax({
        url: 'http://'+server+'/_app/class_api/busca-operador.class.php',
        type: 'POST',
        data: {keyword:keyword},
        success:function(data){
          $('#lista_adesao').show();
          

          if (data == ''){
            var estrutura = "<li class='listagem_nenhum'>Nenhum resultado encontrado</li>";
            $('#lista_adesao').html(estrutura);
          }else{
            $('#lista_adesao').html(data);
          }
        }
      });
    }


  } else {
    $('#lista_adesao').hide();
  }
}

function verificaOperadorAdesao(){
  var inpBusca = $('#busca_operador_adesao').val();
  if (inpBusca == '') {
    $('.listagem').hide();
  }
}

// set_item : this function will be executed when we select an item
function set_item_operador(item, item2) {
  // Seta o nome do operador no campo
  $('#busca_operador_adesao').val(item);

  // ID do operador
  $('#matricula_operador_adesao').val(item2);

  // Oculta a busca
  $('.cl_adesao').hide();

  // Mostra o chipview
  $('#chipview-adesao').show();

  // Seta o nome do cooperado no chipview
  var str = "("+item2+") - "+item;
  // if(str.length > 23) str = str.substring(0,23)+'...';
  $('#txt_name_prepo_adesao').html(str);

  // Seta o valor do ID no button para fechar depois o chipview
  $('#close-adesao').val(item2);

  // hide proposition list
  $('#lista_adesao').hide();
}

function closeChipAdesao(id){
  // Oculta o chipview
  $('#chipview-adesao').hide();

  // Zera o valor do ID no input
  $('#matricula').val('');
  $('#matricula_operador_adesao').val('');
  $('#busca_operador_adesao').val('');
  $('#busca_matricula_adesao').val('');

  // Exibe o campo de busca vazio
  $('.cl_adesao').val('');
  $('.cl_adesao').show();
}        

        
// ########################## INSERT BANCO CADASTRO TAXA adesao ######################


function InsereAdesao(){
    var data = $('#data_adesao').val();
    var valor = $('#valor_adesao').val();
    var matricula = $('#busca_matricula_adesao3').val();
    var nome = $('#busca_operador_adesao3').val();
  
 
  


  
  
  


  if (matricula != '' && nome != '' && valor_adesao != '' && data != ''  ){
            swal("Aguarde...!", {
            closeOnClickOutside: false,
            buttons: false});


     // Serializa o form
    var formTaxaAdesao = $('#FormTaxaAdesao').serialize();
    $.ajax({

        url: 'http://'+server+'/_app/_financeiro/class/insert-cadastro-taxa-adesao.class.php',
        type: "POST",
        data:  formTaxaAdesao,
        success: function(data){
         //alert(data);
          
          if (data.trim() == 'ok'){
            swal({
              title: "Taxa cadastrada com sucesso!",
              icon: "success",
            }).then((value) => {
                  window.location.reload();
                });

          }else{

             swal({
              title: 'Verifique as informações e tente novamente!',
              icon: 'error',
              confirmButtonColor: '#3085d6',
              confirmButtonText: 'Ok'
             })
          }
        }         
      });
  
  }else{
   swal({
        title: 'Preencha todos os campos',
        icon: 'warning',
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Ok'
    })

    

}
}





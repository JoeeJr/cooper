function AtualizaRepasse(){
  var matricula_cad = $('.matricula_cad').val();
  

  if (matricula_cad != '' ){
   			
   			swal("Aguarde...!", {
		  	closeOnClickOutside: false,
		  	buttons: false
				});

     // Serializa o form
    var formRepasseCad = $('#FormRepasseCad').serialize();
    $.ajax({

        url: 'http://'+server+'/_app/_financeiro/class/update-repasse-cad.class.php',
        type: "POST",
        data:  formRepasseCad,
        success: function(data){
          //alert(data);
          
          if (data.trim() == 'ok'){
            swal({
              title: "Matrícula atualizada com sucesso!",
              icon: "success",
            }).then((value) => {
                  window.location.reload();
                });

          }else{

             swal({
              title: 'Verifique as informações e tente novamente!',
              icon: 'error',
              confirmButtonColor: '#3085d6',
              confirmButtonText: 'Ok'
             })
          }
        }         
      });
  
  }else{
   swal({
        title: 'Preencha todos os campos',
        icon: 'warning',
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Ok'
    })

    

}
}

<?php /*autenticador*/ include('../../admin/autenticador.php'); ?>
<?php /*controlador*/ include('../../admin/controler_sys.php'); ?>
<!DOCTYPE html>
<html lang="pt-br">
   <head>
      <meta charset="utf-8">
      <meta name="robots" content="noindex, nofollow">
      <title><?php include('../../includes/title.php'); ?></title>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- FAV ICON -->
      <link rel="icon" type="image/png" href="http://<?= $server ?>/img/fav.png" />
      <!-- CSS -->
      <link rel="stylesheet" href="http://<?= $server ?>/css/bootstrap3.3.0.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/estilo.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/fontawesome.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/responsive.dataTables.min.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/jquery.dataTables.min.css">
      <!-- JAVASCRIPTS --> 


      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.js"></script>
     

     
      <script type="text/javascript" src="http://<?= $server ?>/js/bootstrap330.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/sweet-alert.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.mask.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.maskMoney.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/datapicker.js"></script>
       <script type="text/javascript" src="http://<?= $server ?>/js/jquery.dataTables.min.js"></script>
        
      <script type="text/javascript" src="http://<?= $server ?>/js/dataTable.responsive.js"></script>
   </head>
   <style>
      .well {
      min-height: 20px;
      padding: 19px;
      margin-bottom: 20px;
      background-color: #ffffff;
      border: 1px solid #e3e3e3;
      border-radius: 4px;
      -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
      box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
      }
      label {
      width: auto;
      height: 75px;;
      border-radius: 1px;
      border-right: 1px solid #e2e2e2;
      }
      
      @media (max-width: 748px){
      label {
      width: 33%;
      height: 100px;
      border-radius: 1px;
      border-right: 1px solid #c1c1c1;
      }
      label span {
      display: block;
      }
      }
      .row{
      margin-top: 3rem;
      }
   </style>
   <body>
      <div id="throbber" style="display:none; min-height:120px;"></div>
      <div id="noty-holder"></div>
      <div id="wrapper">
         <!-- Navigation -->
         <?php include('../../includes/menu.php') ?>
         <div id="page-wrapper">
            <div class="container-fluid">
               <!-- Page Heading -->
               <div class="row" id="main">
                  <div class="col-md-12 well">
                     <div class="col-md-12">
                        <h3 class="rlk">REPASSE CAD</h3>
                     </div>
                     <div class="col-md-12">
                        <?php
                           // Cabeçalho de navegação
                           include('includes/cabecalho-contas-a-receber.php'); 
                           ?>
                     </div>
                     <div class="col-md-12 well">
                        <form id="FormRepasseCad" enctype="multipart/form-data" method="post"  >
                           <div class="col-md-4" >
                              <span>Arquivo</span>
                              <input id="arquivo_repasse"  name="arquivo_repasse" value="teste" class="form-control" type="file">
                              <br>
                           </div>
                           <div class="col-md-12" style="margin-top:20px;">
                              <button   class="btn btn-warning pull-right" onclick="InsereRepasseCAD()" >CADASTRAR</button>
                           </div>
                        </form>
                        <div class="col-md-12" style="margin-top:20px;">
                           <button  name="passo_2" onclick="passo()" class="btn btn-warning pull-right" >PASSO 2</button>
                        </div>
                     </div>
                     <!-- FORMULARIO DE EDIÇÃO -->
                  </div>
                  <div class="col-md-12 well">
                     <div class="col-md-12">
                        <h3 class="rlk">REPASSE CAD</h3>
                     </div>
                     <div class="col-md-12">
                        <div class="col-md-12">
                           <table id="example" class="display nowrap" style="width:100%">
                        <thead>
                           <tr>
                              <th>Cooperado</th>
                              <th>N° do IMEI</th>
                              <th>Placa</th>
                              <th>Data</th>
                              <th>Autênticação</th>
                              <th>Qtd</th>
                              <th>Valor</th>
                           </tr>
                        </thead>
                        <?php 
                           $dados = $class->Select("*","repasse_cad","","");
                           
                           
                           
                           
                           
                           while($row = $dados->fetch(PDO::FETCH_OBJ)){
                                    $format = new DateTime($row->data);
                                    $data = $format->format('d/m/Y');
                               
                           
                           
                           
                           
                           
                           ?>
                        <tr>
                           <td><?= utf8_encode($row->nome_cooperado) ?></td>
                           <td><?= $row->imei ?></td>
                           <td><?= $row->placa ?></td>
                           <td><?= $data ?></td>
                           <td><?= $row->auth ?></td>
                           <td><?= $row->qtd ?></td>
                           <td><?= $row->valor ?></td>
                        </tr>
                        <?php
                           }
                           ?>
                        <tfoot>
                           <th>Cooperado</th>
                           <th>N° do IMEI</th>
                           <th>Placa</th>
                           <th>Data</th>
                           <th>Autênticação</th>
                           <th>Qtd</th>
                           <th>Valor</th>
                        </tfoot>
                     </table>
                   </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <script type="text/javascript">        
         $(document).ready(function() {
           var table = $('#example').DataTable( {
           rowReorder: {
              selector: 'td:nth-child(2)'
           },
           responsive: true,
            "language": {
               "url": "https://cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
            }
           });
         });
      </script>
      <script type="text/javascript">
         function passo(){
            window.location.href = 'http://'+server+ '/repasse_cad_matricula/';
           }
         
      </script>
      <script type="text/javascript" src="http://<?= $server ?>/admin/_class/caminho_controler.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/_app/_financeiro/js/importar-planilha.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/menu-mobile.js"></script>
   </body>
</html>
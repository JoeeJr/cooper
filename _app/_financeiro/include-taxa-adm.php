 <?php /*autenticador*/ include('../../admin/autenticador.php'); ?>
<?php /*controlador*/ include('../../admin/controler_sys.php'); ?>
<!DOCTYPE html>
<html lang="pt-br">
   <head>
      <meta charset="utf-8">
      <meta name="robots" content="noindex, nofollow">
      <title><?php include('../../includes/title.php'); ?></title>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- FAV ICON -->
      <link rel="icon" type="image/png" href="http://<?= $server ?>/img/fav.png" />
      <!-- CSS -->
      <link rel="stylesheet" href="http://<?= $server ?>/css/bootstrap3.3.0.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/estilo.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/fontawesome.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/responsive.dataTables.min.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/jquery.dataTables.min.css">
      <!-- JAVASCRIPTS --> 
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.dataTables.min.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/dataTable.responsive.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/bootstrap330.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/sweet-alert.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.mask.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.maskMoney.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/datapicker.js"></script>
   </head>
   <style>
      .well {
      min-height: 20px;
      padding: 19px;
      margin-bottom: 20px;
      background-color: #ffffff;
      border: 1px solid #e3e3e3;
      border-radius: 4px;
      -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
      box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
      }
      label {
      width: auto;
      height: 75px;;
      border-radius: 1px;
      border-right: 1px solid #e2e2e2;
      }
      @media (max-width: 748px){
      label {
      width: 33%;
      height: 100px;
      border-radius: 1px;
      border-right: 1px solid #c1c1c1;
      }
      label span {
      display: block;
      }
      }
      .row{
      margin-top: 3rem;
      }
   </style>
   <body>
      <div id="throbber" style="display:none; min-height:120px;"></div>
      <div id="noty-holder"></div>
      <div id="wrapper">
         <!-- Navigation -->
         <?php include('../../includes/menu.php') ?>
         <div id="page-wrapper">
            <div class="container-fluid">
               <!-- Page Heading -->
               <div class="row" id="main">
                  <div class="col-md-12 well">
                     <div class="col-md-12">
                        <h3 class="rlk">TAXA ADMINISTRATIVA</h3>
                     </div>
                     <div class="col-md-12">
                        <?php
                           // Cabeçalho de navegação
                           include('includes/cabecalho-contas-a-receber.php'); 
                           ?>
                     </div>
                     <div class="col-md-12">
                        <form method="POST" id="FormTaxaAdm">
                           <div style="margin-bottom: 5px;" class="col-md-12 well">
                              <div class="col-md-1 cl_operador">
                                 <span>Matrícula</span><b style="color:red">*</b>
                                 <input autocomplete="off" id="busca_matricula" class="form-control" type="text">
                                 <br>
                              </div>
                              <div class="col-md-4 cl_operador">
                                 <span>Nome do Operador</span><b style="color:red">*</b>
                                 <input autocomplete="off" class="form-control" type="text" onkeyup="BuscaOperador()" onkeydown="verificaOperador()" id="busca_operador">
                                 <br>
                              </div>
                              <div style="margin-top:10px; padding:0" class="col-md-8">
                                 <!-- Input com o ID do operador -->
                                 <input type="hidden" name="cad_matricula" id="matricula_operador">
                                 <!-- Chip View -->
                                 <span id="chipview-operador" class="chipview">
                                 <span id="txt_name_prepo2" class="txt_name_prepo"></span>
                                 <button onClick="closeChip2(this.value)" value="" type="button" id="close-chip2" class="close-chip">
                                 <b class="fa fa-close"></b>
                                 </button>
                                 </span>
                                 <!-- Auto Complete -->
                                 <div style="display:none" id="lista_operador" class="history_list">
                                    <div class="history_item_name">
                                       <li class="listagem">
                                       </li>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-md-12">
                                 <!-- div vazia para alinhamento dos inputs-->
                              </div>
                              <div class="col-md-2" style="margin-top:20px;">
                                 <span>Valor</span>
                                 <input id="valor_adm" name="valor_adm" maxlength="10" data-thousands="." data-decimal="," value="" placeholder="0,00" class="form-control"  type="text">
                                 <br>
                              </div>
                              <div class="col-md-2" style="margin-top:20px;">
                                 <span>Data de vencimento</span>
                                 <input  id="data_adm" name="data_adm" autocomplete="off" placeholder="01/01/2018"value=""  class="form-control" type="text">
                                 <br>
                              </div>
                              <!--    <div class="col-md-2" style="margin-top:20px;">
                                 <span>Nº do Documento</span>
                                 <input  id="documento_adm" name="documento_adm" value="" placeholder="111-222-333" class="form-control" type="text">
                                 <br>
                                 </div> -->
                              <div class="col-md-2" style="margin-top:20px;">
                                 <br>
                                 <button  class="btn btn-warning pull-right" onclick="InsereAdm()">CADASTRAR</button>
                              </div>
                           </div>
                        </form>
                     </div>
                     <!-- FORMULARIO DE EDIÇÃO -->
                  </div>
                  <div class="col-md-12 well">
                     <div class="col-md-12">
                        <h3 class="rlk">TAXA ADMINISTRATIVA</h3>
                     </div>
                     <div class="col-md-12">
                        <div class="col-md-12">
                           <table id="example" class="display nowrap" style="width:100%">
                              <thead>
                                 <tr>
                                    <th>Nome</th>
                                    <th>Numero Documento</th>
                                    <th>Valor</th>
                                    <th>Status</th>
                                 </tr>
                              </thead>
                              <?php 
                                 // $id = $_GET['id_apagar'];
                                 $dados = $class->Select("*","cadastro_taxa_adm","","");
                                 
                                 
                                 
                                 
                                 
                                 while($row = $dados->fetch(PDO::FETCH_OBJ)){
                                  $dadosCoop = $class->SelectEsp("nome","cooperados","WHERE id = '$row->id_cooperado'");
                                 
                                     
                                 
                                 
                                 
                                 
                                 
                                 ?>
                              <tr>
                                 <td><?= utf8_encode($dadosCoop) ?></td>
                                 <td><?= $row->numero_documento ?></td>
                                 <td><?= $row->valor ?></td>
                                 <td>
                   <div class="input-group">
                      
                    <div id="radioBtn" class="btn-group">
                    <a <?php if($row->status == 1){echo 'class="btn btn-primary btn-sm active"';}?> class="btn btn-primary btn-sm notActive" data-toggle="status" data-title="1" value="1" onclick="Status(<?= $row->id_adm ?>, 1)">Pago</a>
               
                     <a <?php if($row->status == 0){echo 'class="btn btn-primary btn-sm active"';}?>  class="btn btn-primary btn-sm notActive" data-toggle="status" data-title="0" value="0" onclick="Status(<?= $row->id_adm ?>, 0)">Aberto</a>
                  </div> 
                </td>
                              </tr>
                              <?php
                                 }
                                 ?>
                              <tfoot>
                                 <tr>
                                    <th>Nome</th>
                                    <th>Numero Documento</th>
                                    <th>Valor</th>
                                    <th>Status</th>
                                 </tr>
                              </tfoot>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      
      <script type="text/javascript">        
         $(document).ready(function() {
           var table = $('#example').DataTable( {
           rowReorder: {
              selector: 'td:nth-child(2)'
           },
           responsive: true,
            "language": {
               "url": "https://cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
            }
           });
            $(function(){
             $('[data-toggle="tooltip"]').tooltip();
             $(".side-nav .collapse").on("hide.bs.collapse", function() {                   
                 $(this).prev().find(".fa").eq(1).removeClass("fa-angle-right").addClass("fa-angle-down");
             });
             $('.side-nav .collapse').on("show.bs.collapse", function() {                        
                 $(this).prev().find(".fa").eq(1).removeClass("fa-angle-down").addClass("fa-angle-right");        
             });
         });
         });   
      </script>
      <script type="text/javascript" src="http://<?= $server ?>/admin/_class/caminho_controler.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/_app/_financeiro/js/id-taxa-adm.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/_app/_financeiro/js/update-status-taxa-adm.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/menu-mobile.js"></script>
   </body>
</html>
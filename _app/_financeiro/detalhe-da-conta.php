<?php /*autenticador*/ include('../../admin/autenticador.php'); ?>
<?php /*controlador*/ include('../../admin/controler_sys.php'); ?>
<!DOCTYPE html>
<html lang="pt-br">
   <head>
     <meta charset="utf-8">
      <meta name="robots" content="noindex, nofollow">
      <title><?php include('../../includes/title.php'); ?></title>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      
      <!-- FAV ICON -->
      <link rel="icon" type="image/png" href="http://<?= $server ?>/img/fav.png" />

      <!-- CSS -->
      <link rel="stylesheet" href="http://<?= $server ?>/css/bootstrap3.3.0.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/estilo.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/fontawesome.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/datepicker.css" />
     <link rel="stylesheet" href="http://<?= $server ?>/css/responsive.dataTables.min.css">
    <link rel="stylesheet" href="http://<?= $server ?>/css/jquery.dataTables.min.css">
      
    

      <!-- JAVASCRIPTS -->
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/cep.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/datapicker.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/bootstrap330.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.mask.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.maskMoney.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/sweet-alert.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.maskMoney.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/dataTable.responsive.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.dataTables.min.js"></script>

      <script src="https://code.highcharts.com/highcharts.js"></script>
      <script src="https://code.highcharts.com/modules/exporting.js"></script>
      <script src="https://code.highcharts.com/modules/export-data.js"></script>
   </head>
   <style>
    .well {
      min-height: 20px;
      padding: 19px;
      margin-bottom: 20px;
      background-color: #ffffff;
      border: 1px solid #e3e3e3;
      border-radius: 4px;
      -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
      box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
    }
    label {
    width: auto;
}
   </style>
   <body>
    <?php  
        $id = $_GET['his'];
        $dados = $class->Select("*","historico_contas_a_pagar","WHERE id_his = '$id'","");
        $editar = $dados->fetch(PDO::FETCH_OBJ);

         // Data de nascimento formatada
                 $format = new DateTime($editar->vencimento);
                            $vencimento = $format->format('d/m/Y');
                            //poem , no lugar de ponto ao puxar valor do banco tava bugando
                            $valor = str_replace(".", ",",$editar->valor);
                            $total = str_replace(".", ",",$editar->valor_total);
    ?>
      <div id="throbber" style="display:none; min-height:120px;"></div>
      <div id="noty-holder"></div>
      <div id="wrapper">
         <!-- Navigation -->
        <?php include('../../includes/menu.php') ?>
         <div id="page-wrapper">
            <div class="container-fluid">
               <!-- Page Heading -->
               <div class="row" id="main" >
               
                  <div class="col-md-12 well">
                     <div class="col-md-12">
                        <h3 class="rlk">Detalhe da conta</h3>
                     </div>
                     <div class="col-md-12">
                      <form method="POST" id="FormEncargos">
                        <div class="col-md-3">
                             <span>Nome:</span>
                              <input id="nome" name="nome"  class="form-control" type="text" value="<?= $editar->nome ?>">
                        </div>
                        <div >
                             
                              <input id="id" name="id"  class="form-control" type="hidden" value="<?= $editar->id_his ?>">
                        </div>
                        <div class="col-md-2">
                             <span>Valor:</span>
                             <input  id="valor_boleto" name="valor_boleto" placeholder ="0,00" data-thousands="." data-decimal="," class="form-control valor-boleto" type="text" value="<?= $valor ?>">
                        </div>

                        <div class="col-md-3">
                           
                             <?php 
                              $data = date('Y-m-d');

                              
                              
                              $editar->vencimento;

                              
                        
                             
                             
                            
                            ?>
                             
                            <span>Encargos:</span>
                            <input <?php if($editar->vencimento > $data){ echo 'readonly value="0,00"';}?> <?php if($editar->encargos > 0.00){ echo 'value="'.$editar->encargos.'"';} ?> id="encargos" name="encargos" placeholder ="0,00" data-thousands="." data-decimal="," class="form-control" type="text" maxlenght="10" />
                            <br>
                          
                        </div>
                        <div class="col-md-2">
                             <span>Total:</span>
                             <input  id="valor_total" name="valor_total" placeholder ="0,00" data-thousands="." data-decimal="," class="form-control type="text" value="<?= $total ?>" readonly="readonly">
                        </div>


                        <div class="col-md-2">
                           
                              <span>Vencimento:</span>
                              <input id="vencimento_boleto" name="vencimento_boleto" class="form-control vencimento-boleto" type="text" value="<?= $vencimento ?>">
                              <br>
                           
                        </div>


                          <div class="col-md-2 pull-right">
                            <span>&nbsp</span>
                            <button type="button" class="btn btn-warning" onclick="UpdateEncargos()">Atualizar conta</button>
                        </div>
<br>

                         <div class="col-md-2 pull-light">
                            <span>&nbsp</span>
                             <button style="background-color: #777777;" onclick="window.history.go(-1); return false;" class="btn btn-warning">Voltar</button>
                        </div>
                    </form>
                       
                  </div>
</div>

          </div>
        </div>
      </div>
    </div>
  </div>
 
      <script type="text/javascript">
         $(function(){
             $('[data-toggle="tooltip"]').tooltip();
             $(".side-nav .collapse").on("hide.bs.collapse", function() {                   
                 $(this).prev().find(".fa").eq(1).removeClass("fa-angle-right").addClass("fa-angle-down");
             });
             $('.side-nav .collapse').on("show.bs.collapse", function() {                        
                 $(this).prev().find(".fa").eq(1).removeClass("fa-angle-down").addClass("fa-angle-right");        
             });
         })    

         $(document).ready(function() {
    $('#example').DataTable();
} );
      </script>
      <script>
        Highcharts.setOptions({
    colors: Highcharts.map(Highcharts.getOptions().colors, function (color) {
        return {
            radialGradient: {
                cx: 0.5,
                cy: 0.3,
                r: 0.7
            },
            stops: [
                [0, color],
                [1, Highcharts.Color(color).brighten(-0.3).get('rgb')] // darken
            ]
        };
    })
});

// Build the chart
Highcharts.chart('container', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: 'Browser market shares in January, 2018'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                style: {
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                },
                connectorColor: 'silver'
            }
        }
    },
    series: [{
        name: 'Share',
        data: [
            { name: 'Chrome', y: 61.41 },
            { name: 'Internet Explorer', y: 11.84 },
            { name: 'Firefox', y: 10.85 },
            { name: 'Edge', y: 4.67 },
            { name: 'Safari', y: 4.18 },
            { name: 'Other', y: 7.05 }
        ]
    }]
});
      </script>
     

       
      </script>
       <script type="text/javascript" src="http://<?= $server ?>/admin/_class/caminho_controler.js"></script>
       <script type="text/javascript" src="http://<?= $server ?>/js/menu-mobile.js"></script>
        <script type="text/javascript" src="http://<?= $server ?>/_app/_financeiro/js/detalhe-da-conta.js"></script>  

   </body>
</html>
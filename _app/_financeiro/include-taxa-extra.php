<?php /*autenticador*/ include('../../admin/autenticador.php'); ?>
<?php /*controlador*/ include('../../admin/controler_sys.php'); ?>
<!DOCTYPE html>
<html lang="pt-br">
   <head>
      <meta charset="utf-8">
      <meta name="robots" content="noindex, nofollow">
      <title><?php include('../../includes/title.php'); ?></title>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- FAV ICON -->
      <link rel="icon" type="image/png" href="http://<?= $server ?>/img/fav.png" />
      <!-- CSS -->
      <link rel="stylesheet" href="http://<?= $server ?>/css/bootstrap3.3.0.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/estilo.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/fontawesome.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/responsive.dataTables.min.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/jquery.dataTables.min.css">
      <!-- JAVASCRIPTS --> 
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.dataTables.min.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/dataTable.responsive.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/bootstrap330.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/sweet-alert.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.mask.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.maskMoney.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/datapicker.js"></script>
   </head>
   <style>
      .well {
      min-height: 20px;
      padding: 19px;
      margin-bottom: 20px;
      background-color: #ffffff;
      border: 1px solid #e3e3e3;
      border-radius: 4px;
      -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
      box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
      }
      label {
      width: auto;
      height: 75px;;
      border-radius: 1px;
      border-right: 1px solid #e2e2e2;
      }
      @media (max-width: 748px){
      label {
      width: 33%;
      height: 100px;
      border-radius: 1px;
      border-right: 1px solid #c1c1c1;
      }
      label span {
      display: block;
      }
      }
      .row{
      margin-top: 3rem;
      }
   </style>
   <body>
      <div id="throbber" style="display:none; min-height:120px;"></div>
      <div id="noty-holder"></div>
      <div id="wrapper">
         <!-- Navigation -->
         <?php include('../../includes/menu.php') ?>
         <div id="page-wrapper">
            <div class="container-fluid">
               <!-- Page Heading -->
               <div class="row" id="main">
                  <div class="col-md-12 well">
                     <div class="col-md-12">
                        <h3 class="rlk">TAXA EXTRA</h3>
                     </div>
                     <div class="col-md-12">
                        <?php
                           // Cabeçalho de navegação
                           include('includes/cabecalho-contas-a-receber.php'); 
                           ?>
                     </div>
                     <div  class="col-md-12 well">
                        <form method="POST" id="FormTaxaExtra">
                           <div class="col-md-1 cl_extra" >
                              <span>Matrícula</span><b style="color:red">*</b>
                              <input autocomplete="off" id="busca_matriculaE" name="busca_matriculaE" class="form-control" type="text">
                              <br>
                           </div>
                           <div class="col-md-4 cl_extra">
                              <span>Nome do Operador</span><b style="color:red">*</b>
                              <input autocomplete="off" class="form-control" type="text" onkeyup="BuscaExtra()" onkeydown="verificaExtra()" id="busca_operadorE" name="busca_operadorE">
                           </div>
                           <div class="col-md-10">
                              <!-- div vazia pra deixar bunitin -->
                           </div>
                           <div style="margin-top:10px; padding:0" class="col-md-6" >
                              <!-- Input com o ID do operador -->
                              <input type="hidden" name="cad_matriculaE" id="matricula_operadorE">
                              <!-- Chip View -->
                              <span id="chipview-extra" class="chipview">
                              <span id="txt_name_prepoE" class="txt_name_prepo"></span>
                              <button onClick="closeChipE(this.value)" value="" type="button" id="close-extra" class="close-chip">
                              <b class="fa fa-close"></b>
                              </button>
                              </span>
                              <br>
                              <!-- Auto Complete -->
                              <div style="display:none" id="lista_operadorE" class="history_list">
                                 <div class="history_item_name">
                                    <li class="listagem">
                                    </li>
                                 </div>
                              </div>
                           </div>
                           <div class="col-md-12">
                              <br>
                              <hr>
                           </div>
                           <div class="col-md-2">
                              <span>Parcelas</span>
                              <select onChange="GeraExtra(this.value)" class="form-control" name="parcelaE" id="parcelaE">
                                 <?php
                                    for ($i=1; $i<= 12; $i++){
                                    
                                    ?>
                                 <option value="<?= $i ?>"><?= $i.'x' ?></option>
                                 <?php
                                    }
                                    ?>
                              </select>
                              <br>
                           </div>
                           <script type="text/javascript">
                              function GeraExtra(parcela){
                                var extra = "";
                                var extra2 = "";
                                var extra3 = "";
                                var extra4 = "";
                                for(i=1; i<=parcela; i++){
                                  
                                  extra2 = extra2+"<span>Valor</span><input  class='form-control valor-extra'  name='valorE[]' data-thousands='.' data-decimal=','' "+i+"placeholder='0,00' value=''  type='text' maxlength='10' ><br>";
                                   extra3 = extra3+"<span>Vencimento</span><input name='vencimentoE[]' class='form-control data-extra' placeholder='01/01/2018' autocomplete='off'"+i+" value=''  type='text' ><br>";
                                   
                              
                                }
                                
                                $("#extra2").html(extra2);
                                $("#extra3").html(extra3);
                                
                                $('.data-extra').mask('00/00/0000');
                                $('.data-extra').datepicker({format: 'dd/mm/yyyy'})
                                
                                $('.valor-extra').maskMoney();
                                
                                
                              
                                
                              }
                              
                           </script>
                           <div class="col-md-2" id="extra2">
                              <span>Valor</span>
                              <input name="valorE" id="valorE" data-thousands="." data-decimal="," placeholder="0,00" maxlength="10" value="" class="form-control valor-extra" type="text">
                              <br>
                           </div>
                           <div class="col-md-2" id="extra3">
                              <span>Data de vencimento</span>
                              <input id="vencimentoE" name="vencimentoE" placeholder="01/01/2018" autocomplete="off" value="" class="form-control data-extra" type="text">
                              <br>
                           </div>
                           <div class="col-md-12">
                              <span>Descrição do Motivo</span>
                              <textarea class="form-control" name="descE" id="descE" cols="30" rows="10"></textarea>
                              <br>
                           </div>
                           <div class="col-md-2">
                              <span>&nbsp</span>
                              <button class="btn btn-warning pull-right" onclick="InsereExtra()">CADASTRAR CONTA</button>
                           </div>
                           <!-- <div class="col-md-3">
                              <span>&nbsp</span>
                              <button style="background: #3F51B5" class="btn btn-warning pull-right">CADASTRAR E GERAR BOLETO</button>
                              </div> -->
                        </form>
                     </div>
                     <!-- FORMULARIO DE EDIÇÃO -->
                  </div>
                  <div class="col-md-12 well">
                     <div class="col-md-12">
                        <h3 class="rlk">TAXA EXTRA</h3>
                     </div>
                     <div class="col-md-12">
                        <div class="col-md-12">
                           <table id="example" class="display nowrap" style="width:100%">
                              <thead>
                                 <tr>
                                    <th>Cooperado</th>
                                    <th>Parcelas</th>
                                    <th>Descrição</th>
                                    <th>#</th>
                                 </tr>
                              </thead>
                              <?php 
                                 // $id = $_GET['id_apagar'];
                                 $dados = $class->Select("*","cadastro_taxa_extra","","");
                                 
                                 
                                 
                                 
                                 
                                 while($row = $dados->fetch(PDO::FETCH_OBJ)){
                                  $dadosCoop = $class->SelectEsp("nome","cooperados","WHERE id = '$row->id_cooperado'");
                                     
                                 
                                 
                                 
                                 
                                 
                                 ?>
                              <tr>
                                 <td><?= utf8_encode($dadosCoop) ?></td>
                                 <td><?= $row->parcelas ?></td>
                                 <td><?= $row->descricao_motivo ?></td>
                                 <td>
                                    <div onclick="lupz(<?= $row->id_extra ?>)" class="lupa"></div>
                                 </td>
                              </tr>
                              <?php
                                 }
                                 ?>
                              <tfoot>
                                 <th>Cooperado</th>
                                 <th>Parcelas</th>
                                 <th>Descrição</th>
                                 <th>#</th>
                              </tfoot>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <script type="text/javascript">
         function lupz(id){
           window.location.href = 'http://'+server+'/detalhe-taxa-extra/'+id;
         }
         
      </script>
      <script type="text/javascript">        
         $(document).ready(function() {
           var table = $('#example').DataTable( {
           rowReorder: {
              selector: 'td:nth-child(2)'
           },
           responsive: true,
            "language": {
               "url": "https://cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
            }
           });
         });
      </script>
      <script type="text/javascript" src="http://<?= $server ?>/admin/_class/caminho_controler.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/_app/_financeiro/js/id-taxa-extra.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/menu-mobile.js"></script>
   </body>
</html>
<?php /*autenticador*/ include('../../admin/autenticador.php'); ?>
<?php /*controlador*/ include('../../admin/controler_sys.php'); ?>
<!DOCTYPE html>
<html lang="pt-br">
   <head>
      <meta charset="utf-8">
      <meta name="robots" content="noindex, nofollow">
      <title><?php include('../../includes/title.php'); ?></title>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- FAV ICON -->
      <link rel="icon" type="image/png" href="http://<?= $server ?>/img/fav.png" />
      <!-- CSS -->
      <link rel="stylesheet" href="http://<?= $server ?>/css/bootstrap3.3.0.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/estilo.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/fontawesome.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/responsive.dataTables.min.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/jquery.dataTables.min.css">
      <!-- JAVASCRIPTS --> 
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.dataTables.min.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/dataTable.responsive.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/bootstrap330.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/sweet-alert.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.mask.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.maskMoney.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/datapicker.js"></script>
   </head>
   <style>
      .well {
      min-height: 20px;
      padding: 19px;
      margin-bottom: 20px;
      background-color: #ffffff;
      border: 1px solid #e3e3e3;
      border-radius: 4px;
      -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
      box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
      }
      label {
      width: auto;
      height: 75px;;
      border-radius: 1px;
      border-right: 1px solid #e2e2e2;
      }
      @media (max-width: 748px){
      label {
      width: 33%;
      height: 100px;
      border-radius: 1px;
      border-right: 1px solid #c1c1c1;
      }
      label span {
      display: block;
      }
      }
      .row{
      margin-top: 3rem;
      }
   </style>
   <body>
      <div id="throbber" style="display:none; min-height:120px;"></div>
      <div id="noty-holder"></div>
      <div id="wrapper">
         <!-- Navigation -->
         <?php include('../../includes/menu.php') ?>
         <div id="page-wrapper">
            <div class="container-fluid">
               <!-- Page Heading -->
               <div class="row" id="main">
                  <div class="col-md-12 well">
                     <div class="col-md-12">
                        <h3 class="rlk">TAXA ADESÃO</h3>
                     </div>
                     <div class="col-md-12">
                        <?php
                           // Cabeçalho de navegação
                           include('includes/cabecalho-contas-a-receber.php'); 
                           ?>
                     </div>
                     <div class="col-md-12 well">
                        <form method="POST" id="FormTaxaAdesao">
                           <input type="hidden" name="cad_matricula_adesao" id="matricula_operador_adesao">
                           <div class="col-md-2 cl_adesao">
                              <span>Matrícula</span><b style="color:red">*</b>
                              <input autocomplete="off" id="busca_matricula_adesao" placeholder="000000000" class="form-control buscaMatAdesao" onBlur="buscarMatriculaAdesao(this.value)" type="text">
                              <br>
                           </div>
                           <div class="col-md-4 cl_adesao">
                              <span>Cooperado</span><b style="color:red">*</b>
                              <input autocomplete="off" onkeyup="BuscaOperadorAdesao()" onkeydown="verificaOperadorAdesao()" id="busca_operador_adesao" placeholder="Ex: João da Silva" class="form-control" type="text">
                           </div>
                           <div class="col-md-10">
                              <!-- diva vazia para ficar bonito -->
                           </div>
                           <div style="margin-top:10px; padding:0" class="col-md-6">
                              <!-- Auto Complete -->
                              <div style="display:none" id="lista_adesao" class="history_list">
                                 <div class="history_item_name">
                                    <li class="listagem">    
                                    </li>
                                 </div>
                              </div>
                              <!-- Chip View -->
                              <span id="chipview-adesao" class="chipview">
                              <span id="txt_name_prepo_adesao" class="txt_name_prepo"></span>
                              <button onClick="closeChipAdesao(this.value)" value="" type="button" id="close-adesao" class="close-chip">
                              <b class="fa fa-close"></b>
                              </button>
                              </span>
                           </div>
                           <div style="margin-top:20px; padding:0" class="col-md-12">
                              <!-- div vazia para ficar bonito -->
                           </div>
                           <div class="col-md-2">
                              <span>Data de vencimento</span>
                              <input id="data_adesao" name="data_adesao" autocomplete="off" placeholder="01/01/2018" value=""  class="form-control" type="text">
                              <br>
                           </div>
                           <div class="col-md-2">
                              <span>Valor</span>
                              <input  id="valor_adesao" name="valor_adesao"  maxlength="10" value="" class="form-control" data-thousands="." data-decimal="," placeholder="0,00" type="text">
                              <br>
                           </div>
                           <div class="col-md-12" style="margin-top:20px">
                              <button class="btn btn-warning pull-right" onclick="InsereAdesao()">CADASTRAR</button>
                           </div>
                        </form>
                     </div>
                     <!-- FORMULARIO DE EDIÇÃO -->
                  </div>
                  <div class="col-md-12 well">
                     <div class="col-md-12">
                        <h3 class="rlk">TAXA ADESÃO</h3>
                     </div>
                     <div class="col-md-12">
                        <div class="col-md-12">
                           <table id="example" class="display nowrap" style="width:100%">
                              <thead>
                                 <tr>
                                    <th>Nome</th>
                                    <th>Vencimento</th>
                                    <th>Numero Documento</th>
                                    <th>Valor</th>
                                    <th>Status</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <?php 
                                    // $id = $_GET['id_apagar'];
                                    $dados = $class->Select("*","cadastro_taxa_adesao","","");
                                    
                                    
                                    
                                    
                                    
                                    while($row = $dados->fetch(PDO::FETCH_OBJ)){
                                     $dadosCoop = $class->SelectEsp("nome","cooperados","WHERE id = '$row->id_cooperado'");
                                    
                                      // Data de nascimento formatada
                                     $format = new DateTime($row->data_vencimento);
                                     $vencimento = $format->format('d/m/Y');
                                    
                                        
                                    
                                    
                                    
                                    
                                    
                                    ?>
                                 <tr>
                                    <td><?= utf8_encode($dadosCoop) ?></td>
                                    <td><?= $vencimento ?></td>
                                    <td><?= $row->numero_documento ?></td>
                                    <td><?= $row->valor ?></td>
                                    <td>
                   <div class="input-group">
                      
                    <div id="radioBtn" class="btn-group">
                    <a <?php if($row->status == 1){echo 'class="btn btn-primary btn-sm active"';}?> class="btn btn-primary btn-sm notActive" data-toggle="status" data-title="1" value="1" onclick="Status(<?= $row->id_adesao ?>, 1)">Pago</a>
               
                     <a <?php if($row->status == 0){echo 'class="btn btn-primary btn-sm active"';}?>  class="btn btn-primary btn-sm notActive" data-toggle="status" data-title="0" value="0" onclick="Status(<?= $row->id_adesao ?>, 0)">Aberto</a>
                  </div> 
                </td>
                                 </tr>
                                 <?php
                                    }
                                    ?>
                              </tbody>
                              <tfoot>
                                 <th>Nome</th>
                                 <th>Vencimento</th>
                                 <th>Numero Documento</th>
                                 <th>Valor</th>
                                 <TH>Status</TH>
                              </tfoot>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <script type="text/javascript">        
         $(document).ready(function() {
           var table = $('#example').DataTable( {
           rowReorder: {
              selector: 'td:nth-child(2)'
           },
           responsive: true,
            "language": {
               "url": "https://cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
            }
           });
         });
      </script>
      <script type="text/javascript" src="http://<?= $server ?>/admin/_class/caminho_controler.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/_app/_financeiro/js/id-taxa-adesao.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/_app/_financeiro/js/update-status-taxa-adesao.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/menu-mobile.js"></script>
   </body>
</html>
<?php /*autenticador*/ include('../../admin/autenticador.php'); ?>
<?php /*controlador*/ include('../../admin/controler_sys.php'); ?>
<!DOCTYPE html>
<html lang="pt-br">
   <head>
      <meta charset="utf-8">
      <meta name="robots" content="noindex, nofollow">
      <title><?php include('../../includes/title.php'); ?></title>
      <meta name="viewport" content="width=device-width, initial-scale=1">

      <!-- FAV ICON -->
      <link rel="icon" type="image/png" href="http://<?= $server ?>/img/fav.png" />

      <!-- CSS -->
      <link rel="stylesheet" href="http://<?= $server ?>/css/bootstrap3.3.0.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/estilo.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/fontawesome.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/responsive.dataTables.min.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/jquery.dataTables.min.css">
      <!-- JAVASCRIPTS --> 


      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.js"></script>
     

      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.dataTables.min.js"></script>
        
      <script type="text/javascript" src="http://<?= $server ?>/js/dataTable.responsive.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/bootstrap330.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/sweet-alert.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.mask.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.maskMoney.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/datapicker.js"></script>
      

      <script src="https://code.highcharts.com/highcharts.js"></script>
      <script src="https://code.highcharts.com/modules/exporting.js"></script>
      <script src="https://code.highcharts.com/modules/export-data.js"></script>


   </head>
   <style>
    .well {
      min-height: 20px;
      padding: 19px;
      margin-bottom: 20px;
      background-color: #ffffff;
      border: 1px solid #e3e3e3;
      border-radius: 4px;
      -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
      box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
    }
    label {
    width: auto;
}
   </style>
   
   <body>


    
   
      <div id="throbber" style="display:none; min-height:120px;"></div>
      <div id="noty-holder"></div>
      <div id="wrapper">
         <!-- Navigation -->
        <?php include('../../includes/menu.php') ?>
         <div id="page-wrapper">
            <div class="container-fluid">
               <!-- Page Heading -->
               <div class="row" id="main" >
               
                  <div class="col-md-6 well">
                     <div class="col-md-12">
                        <h3 class="rlk">Contas a pagar</h3>
                     </div>
                     <div class="col-md-12">
                     
                      <form method="post" id="FormContaPagar">
                        <div class="col-md-5">
                             <span>Nome:</span>
                              <input id="nome" name="nome" placeholder ="Ex:água,luz,etc.." class="form-control" type="text">
                        </div>
                        <div class="col-md-3 ">
                             <span>Valor:</span>
                             <input id="valor_boleto" name="valor_boleto" placeholder="0,00" data-thousands="." data-decimal="," class="form-control valor-boleto" type="text" maxlength="10">
                        </div>

                        <div class="col-md-3 ">
                         
                              <span>Vencimento</span>
                              <input id="vencimento_boleto" name="vencimento_boleto" placeholder="01/01/2018"  class="form-control vencimento-boleto" type="text">
                              <br>
                          
                        </div>


                        <div class="col-md-6 ">
                      
                              <span>Centro de Custo</span>
                              <input  id="centro_custo" name="centro_custo" value="" class="form-control centro_custo" type="text">
                              <br>
                         
                        </div>

                         <div class="col-md-6 ">
                          
                              <span>Nº do documento de origem</span>
                              <input id="documento_origem" name="documento_origem" value="" class="form-control documento-origem" type="text">
                              <br>
                         
                        </div>
                         <div class="col-md-5">
                       
                              <span>Forma de Pagamento</span>
                              <select class="form-control" name="forma_pagamento" id="forma_pagamento" >
                                <option value="" selected="">Selecione</option>
                                <option value="boleto">boleto</option>
                                <option value="cartao">cartao</option>
                                <option value="debito">debito</option>
                              </select>
                              <br>
                         
                         </div>


                          <div class="col-md-5">
                         
                              <div style="padding:0px" class="col-md-2">
                                <span>Pagamento</span><br>
                                <div class="form-group has-feedback ">
                                  <label class="input-group">
                                    <span class="input-group-addon">
                                      <input id="recorrente" name="recorrente" type="checkbox"  value="0" />
                                    </span>
                                    <div class="form-control form-control-static">
                                      Pagamento recorrente
                                    </div>
                                    <span class="glyphicon form-control-feedback "></span>
                                  </label>
                                </div>
                              </div>
                              <br>
                         
                         </div>


                         <div class="col-md-6">
                        
                              <span>Parcelas</span>
                              <select class="form-control" name="parcelas" id="parcelas" disabled="disabled">
                                <option value="1" selected="">1x</option>
                                <option value="2">2x</option>
                                <option value="3">3x</option>
                                <option value="4">4x</option>
                                <option value="5">5x</option>
                                <option value="6">6x</option>
                                <option value="7">7x</option>
                                <option value="8">8x</option>
                                <option value="9">9x</option>
                                <option value="10">10x</option>
                                <option value="11">11x</option>
                                <option value="12">12x</option>
                              </select>
                              <br>
                        
                         </div>


                         <div class="col-md-5">
                           
                              <span>Tipo de Conta</span>
                              <select class="form-control" name="tipo_conta" id="tipo_conta">
                                <option value="" selected="">Selecione</option>
                                <option value="teste">teste</option>
                                <option value="teste2">teste2</option>
                                <option value="teste3">teste3</option>
                              </select>
                              <br>
                           
                         </div>



                       
                        <div class="col-md-3">
                          <span>Tipo de conta</span><br>
                          <div class="form-group has-feedback">
                            <label class="input-group">
                              <span class="input-group-addon">
                                <input type="radio" name="radio" id="radio" value="Real" />
                              </span>
                              <div class="form-control form-control-static">
                                Real
                              </div>
                              <span class="glyphicon form-control-feedback "></span>
                            </label>
                          </div>
                        </div>

                        <div class="col-md-3">
                          <span>Previsional</span><br>
                          <div class="form-group has-feedback ">
                            <label class="input-group">
                              <span class="input-group-addon">
                                <input type="radio" name="radio" id="radio" value="Preposto" />
                              </span>
                              <div class="form-control form-control-static">
                                Preposto 
                              </div>
                              <span class="glyphicon form-control-feedback "></span>
                            </label>
                          </div>   
                           
                          <div class="col-md-1">                           
                              <button  onclick="InsereConta()" class="btn btn-warning">Adicionar</button> 
                             
                            </div>

                        </div>

                        
                    </form>   
                  </div>
</div>


                  <div class="col-md-6 well">
                     <div class="col-md-12">
                        <h3 class="rlk">Gráfico</h3>
                     </div>
                     <div class="col-md-12">
                         <div id="container" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
                     </div>
                  </div>


                   

                  <div class="col-md-12 well">
                   <div class="col-md-12">
                        <h3 class="rlk">CONTAS A PAGAR</h3>
                     </div>
                     <div class="col-md-12">
                        <div class="col-md-12">
                      <table id="example" class="display nowrap" style="width:100%">
        <thead>
            <tr>
                <th>#ID</th>
                <th>Nome</th>
                <th>Centro de Custo</th>
                <th>Recorrente</th>
                <th>Tipo Conta</th>
                <th>#</th>
            </tr>
        </thead>
        <tbody>
           <?php 
                   
                    // $id = $_GET['id_apagar'];
                    $dados = $class->Select("*","contas_a_pagar","","");
                 
                   
                   
                   while($row = $dados->fetch(PDO::FETCH_OBJ)){
                    

                    if($row->pagamento_recorrente == 1){
                      $recorrente = "Recorrente";
                    }else{
                      $recorrente = "Não Recorrente";
                    }

                 

            ?>
            <tr>
                <td><?= $row->id_apagar ?></td>
                <td><?= utf8_encode($row->nome) ?></td>
                <td><?= $row->centro_custo ?></td>
                <td><?= $recorrente ?></td>              
                <td><?= $row->real_preposto ?></td>
                <td><div onclick="lupz(<?= $row->id_apagar?>)" class="lupa"></div></td>
            </tr>
            <?php
                }
             ?>
        </tbody>
        <tfoot>
            <tr>
                <th>#ID</th>
                <th>Nome</th>
                <th>Centro de Custo</th>
                <th>Recorrente</th>
                <th>Tipo Conta</th>
                <th>#</th>
            </tr>
        </tfoot>
    </table>
                     </div>
                  </div>



            </div>




          </div>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript">
function lupz(id){
  window.location.href = 'detalhe-contas-a-pagar/'+id;
}

</script>
<script type="text/javascript">
  $(document).ready(function(){
     $('#documento_origem').mask('000-000-000');
   });
</script>
      <!-- <script type="text/javascript">
         $(function(){
             $('[data-toggle="tooltip"]').tooltip();
             $(".side-nav .collapse").on("hide.bs.collapse", function() {                   
                 $(this).prev().find(".fa").eq(1).removeClass("fa-angle-right").addClass("fa-angle-down");
             });
             $('.side-nav .collapse').on("show.bs.collapse", function() {                        
                 $(this).prev().find(".fa").eq(1).removeClass("fa-angle-down").addClass("fa-angle-right");        
             });
         })    

$(document).ready(function() {
    var table = $('#example').DataTable( {
        rowReorder: {
            selector: 'td:nth-child(2)'
        },
        responsive: true
    } );
} );
      </script> -->
        <script type="text/javascript">        
         $(document).ready(function() {
           var table = $('#example').DataTable( {
           rowReorder: {
              selector: 'td:nth-child(2)'
           },
           responsive: true,
            "language": {
               "url": "https://cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
            }
           });
         });
      </script>

      <!-- <script>
         $(document).ready(function() {
             $('#datePicker')
                 .datepicker({
                     format: 'mm/dd/yyyy'
                 })
         
         
                 .on('changeDate', function(e) {
                     // Revalidate the date field
                     $('#eventForm').formValidation('revalidateField', 'date');
                 });
         
             $('#eventForm').formValidation({
                 framework: 'bootstrap',
                 icon: {
                     valid: 'glyphicon glyphicon-ok',
                     invalid: 'glyphicon glyphicon-remove',
                     validating: 'glyphicon glyphicon-refresh'
                 },
                 fields: {
                     name: {
                         validators: {
                             notEmpty: {
                                 message: 'The name is required'
                             }
                         }
                     },
                     date: {
                         validators: {
                             notEmpty: {
                                 message: 'The date is required'
                             },
                             date: {
                                 format: 'MM/DD/YYYY',
                                 message: 'The date is not a valid'
                             }
                         }
                     }
                 }
         
         
         
         
         
             });
         });

        $(document).ready(function(){
            //  ID do campo + sequencia que o campo deve adotar
            $('#id_do_campo').mask('000-000-000');

            //  CPF
            $('#id_do_campo_cpf').mask('000.000.000-00');
        });
      </script> -->
      <?php 
        $grafico = $class->Select("*","historico_contas_a_pagar","","");
        
        
        $percent = 0;
        $i = 0;
       

      foreach ($grafico->fetchAll(PDO::FETCH_OBJ) as $graf) {
            
        
        if($graf->status == 1){
         
          
            $percent = $percent + 1;
            
        }else{
            $i = $i + 1;
        }}
       
       
      ?>
    
      <script>
        Highcharts.setOptions({
    colors: Highcharts.map(Highcharts.getOptions().colors, function (color) {
        return {
            radialGradient: {
                cx: 0.5,
                cy: 0.3,
                r: 0.7
            },
            stops: [
                [0, color],
                [1, Highcharts.Color(color).brighten(-0.3).get('rgb')] // darken
            ]
        };
    })
});

// Build the chart
Highcharts.chart('container', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: 'Contas Pagas x Pendentes'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.y}</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.y} ',
                style: {
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                },
                connectorColor: 'silver'
            }
        }
    },
    series: [{
        name: 'Contas',
        data: [
            { name: 'Pagas',y: <?= $percent ?>  },
            { name: 'Pendentes', y: <?= $i ?> },
        ]
    }]
});
      </script>
      <script>
            $('#recorrente').click(function(e) {
                if($(this).prop('checked') == true)$('#parcelas').removeAttr("disabled");
                else $('#parcelas').attr("disabled","disabled");
            });
        </script>
      <script type="text/javascript" src="http://<?= $server ?>/admin/_class/caminho_controler.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/_app/_financeiro/js/conta-a-pagar.js"></script>
<script type="text/javascript" src="http://<?= $server ?>/js/menu-mobile.js"></script>  
   </body>
</html>
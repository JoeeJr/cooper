<?php /*autenticador*/ include('../../admin/autenticador.php'); ?>
<?php /*controlador*/ include('../../admin/controler_sys.php'); ?>
<!DOCTYPE html>
<html lang="pt-br">
   <head>
        <meta charset="utf-8">
      <meta name="robots" content="noindex, nofollow">
      <title><?php include('../../includes/title.php'); ?></title>
      <meta name="viewport" content="width=device-width, initial-scale=1">

      <!-- FAV ICON -->
      <link rel="icon" type="image/png" href="http://<?= $server ?>/img/fav.png" />

      <!-- CSS -->
      <link rel="stylesheet" href="http://<?= $server ?>/css/bootstrap3.3.0.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/datepicker.css" />
      <link rel="stylesheet" href="http://<?= $server ?>/css/estilo.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/fontawesome.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/responsive.dataTables.min.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/jquery.dataTables.min.css">
      
      
      <!-- JAVASCRIPTS -->
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/cep.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/datapicker.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/bootstrap330.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.mask.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.maskMoney.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/sweet-alert.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.maskMoney.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/dataTable.responsive.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.dataTables.min.js"></script>
      

      <script src="https://code.highcharts.com/highcharts.js"></script>
      <script src="https://code.highcharts.com/modules/exporting.js"></script>
      <script src="https://code.highcharts.com/modules/export-data.js"></script>
   </head>
   <style>
      .well {
      min-height: 20px;
      padding: 19px;
      margin-bottom: 20px;
      background-color: #ffffff;
      border: 1px solid #e3e3e3;
      border-radius: 4px;
      -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
      box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
      }
      label {
      width: auto;
      height: 75px;;
      border-radius: 1px;
      border-right: 1px solid #e2e2e2;
      }
      .dataTables_wrapper .dataTables_filter {
      float: right;
      text-align: right;
      display: none;
      }
      .dataTables_wrapper .dataTables_length {
      float: left;
      display: none;
      }
      @media (max-width: 748px){
      label {
      width: 33%;
      height: 100px;
      border-radius: 1px;
      border-right: 1px solid #c1c1c1;
      }
      label span {
      display: block;
      }
      }
      .row{
      margin-top: 3rem;
      }
   </style>

   <body>
      <div id="throbber" style="display:none; min-height:120px;"></div>
      <div id="noty-holder"></div>
      <div id="wrapper">
         <!-- Navigation -->
        <?php include('../../includes/menu.php') ?>
         <div id="page-wrapper">
            <div class="container-fluid">
               <!-- Page Heading -->
               <div class="row" id="main">               
                  <div class="col-md-12 well">                    
                    <div class="col-md-12">
                       <h3 class="rlk">GRAFICOS</h3>
                    </div>

                    <div class="col-md-12">
                      <?php
                        // Cabeçalho de navegação
                        include('includes/cabecalho-contas-a-receber.php'); 
                      ?>
                    </div>
                    <div class="col-md-12" id="container" >
                      
                    </div> 
                    <div class="col-md-12" id="dxcdcxcxddfghbbbgghttDsasfffPkdsfLlllllllllllllllllSAsssaew"></div> 

                      



                    <!-- FORMULARIO DE EDIÇÃO -->

                  </div>

               
               </div>
            </div>
         </div>
      </div>
      </div>
       <script>
         // Create the chart
         Highcharts.chart('container', {
             chart: {
                 type: 'column'
             },
             title: {
                 text: 'Contas a receber 2018'
             },
             subtitle: {
                 text: 'Contas a receber neste ano'
             },
             xAxis: {
                 type: 'category'
             },
             yAxis: {
                 title: {
                     text: ''
                 }
         
             },
             legend: {
                 enabled: false
             },
             plotOptions: {
                 series: {
                     borderWidth: 0,
                     dataLabels: {
                         enabled: true,
                         format: '{point.y}'
                     }
                 }
             },
         
             tooltip: {
                 headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                 pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
             },
            

         
             "series": [
                 {
                     "name": "Mês",
                     "colorByPoint": true,
                     "data": [
                         {
                             "name": "Jan",
                             "y": <?= $teste = $class->SelectQtd("vencimento","historico_contas_a_pagar","WHERE MONTH(vencimento) = '1' AND YEAR(vencimento) = YEAR(CURDATE()) AND status = '0'   "); ?>, 
                             "drilldown": "Jan"
                         },
                         {
                             "name": "Fev",
                             "y": <?= $teste = $class->SelectQtd("vencimento","historico_contas_a_pagar","WHERE MONTH(vencimento) ='2' AND YEAR(vencimento) = YEAR(CURDATE()) AND status = '0'"); ?>,
                             "drilldown": "Fev"
                         },
                         {
                             "name": "Mar",
                             "y": <?= $teste = $class->SelectQtd("vencimento","historico_contas_a_pagar","WHERE MONTH(vencimento) ='3' AND YEAR(vencimento) = YEAR(CURDATE()) AND status = '0'"); ?>,
                             "drilldown": "Mar"
                         },
                         {
                             "name": "Abr",
                             "y": <?= $teste = $class->SelectQtd("vencimento","historico_contas_a_pagar","WHERE MONTH(vencimento) ='4' AND YEAR(vencimento) = YEAR(CURDATE()) AND status = '0'"); ?>,
                             "drilldown": "Abr"
                         },
                         {
                             "name": "Mai",
                             "y": <?= $teste = $class->SelectQtd("vencimento","historico_contas_a_pagar","WHERE MONTH(vencimento) ='5' AND YEAR(vencimento) = YEAR(CURDATE()) AND status = '0'"); ?>,
                             "drilldown": "Mai"
                         },
                         {
                             "name": "Jun",
                             "y": <?= $teste = $class->SelectQtd("vencimento","historico_contas_a_pagar","WHERE MONTH(vencimento) ='6' AND YEAR(vencimento) = YEAR(CURDATE()) AND status = '0'"); ?>,
                             "drilldown": "Jun"
                         },
                         {
                             "name": "Jul",
                             "y": <?= $teste = $class->SelectQtd("vencimento","historico_contas_a_pagar","WHERE MONTH(vencimento) ='7' AND YEAR(vencimento) = YEAR(CURDATE()) AND status = '0'"); ?>,
                             "drilldown": "Jul"
                         },
                         {
                             "name": "Ago",
                             "y": <?= $teste = $class->SelectQtd("vencimento","historico_contas_a_pagar","WHERE MONTH(vencimento) ='8' AND YEAR(vencimento) = YEAR(CURDATE()) AND status = '0'"); ?>,
                             "drilldown": "Ago"
                         },
                         {
                             "name": "Set",
                             "y": <?= $teste = $class->SelectQtd("vencimento","historico_contas_a_pagar","WHERE MONTH(vencimento) ='9' AND YEAR(vencimento) = YEAR(CURDATE()) AND status = '0'"); ?>,
                             "drilldown": "Set"
                         },
                         {
                             "name": "Out",
                             "y": <?= $teste = $class->SelectQtd("vencimento","historico_contas_a_pagar","WHERE MONTH(vencimento) ='10' AND YEAR(vencimento) = YEAR(CURDATE()) AND status = '0'"); ?>,
                             "drilldown": "Out"
                         },
                         {
                             "name": "Nov",
                             "y": <?= $teste = $class->SelectQtd("vencimento","historico_contas_a_pagar","WHERE MONTH(vencimento) ='11' AND YEAR(vencimento) = YEAR(CURDATE()) AND status = '0'"); ?>,
                             "drilldown": "Nov"
                         },
                         {
                             "name": "Dez",
                             "y": <?= $teste = $class->SelectQtd("vencimento","historico_contas_a_pagar","WHERE MONTH(vencimento) ='12' AND YEAR(vencimento) = YEAR(CURDATE()) AND status = '0'"); ?>,
                             "drilldown": "Dez"
                         }
                     ]
                 }
             ],
                "drilldown": {
                 "series": [
                     {
                         "name": "Chrome",
                         "id": "Chrome",
                         "data": [
                             [
                                 "v65.0",
                                 0.1
                             ],
                             [
                                 "v64.0",
                                 1.3
                             ],
                             [
                                 "v63.0",
                                 53.02
                             ],
                             [
                                 "v62.0",
                                 1.4
                             ],
                             [
                                 "v61.0",
                                 0.88
                             ],
                             [
                                 "v60.0",
                                 0.56
                             ],
                             [
                                 "v59.0",
                                 0.45
                             ],
                             [
                                 "v58.0",
                                 0.49
                             ],
                             [
                                 "v57.0",
                                 0.32
                             ],
                             [
                                 "v56.0",
                                 0.29
                             ],
                             [
                                 "v55.0",
                                 0.79
                             ],
                             [
                                 "v54.0",
                                 0.18
                             ],
                             [
                                 "v51.0",
                                 0.13
                             ],
                             [
                                 "v49.0",
                                 2.16
                             ],
                             [
                                 "v48.0",
                                 0.13
                             ],
                             [
                                 "v47.0",
                                 0.11
                             ],
                             [
                                 "v43.0",
                                 0.17
                             ],
                             [
                                 "v29.0",
                                 0.26
                             ]
                         ]
                     },
                     {
                         "name": "Firefox",
                         "id": "Firefox",
                         "data": [
                             [
                                 "v58.0",
                                 1.02
                             ],
                             [
                                 "v57.0",
                                 7.36
                             ],
                             [
                                 "v56.0",
                                 0.35
                             ],
                             [
                                 "v55.0",
                                 0.11
                             ],
                             [
                                 "v54.0",
                                 0.1
                             ],
                             [
                                 "v52.0",
                                 0.95
                             ],
                             [
                                 "v51.0",
                                 0.15
                             ],
                             [
                                 "v50.0",
                                 0.1
                             ],
                             [
                                 "v48.0",
                                 0.31
                             ],
                             [
                                 "v47.0",
                                 0.12
                             ]
                         ]
                     },
                     {
                         "name": "Internet Explorer",
                         "id": "Internet Explorer",
                         "data": [
                             [
                                 "v11.0",
                                 6.2
                             ],
                             [
                                 "v10.0",
                                 0.29
                             ],
                             [
                                 "v9.0",
                                 0.27
                             ],
                             [
                                 "v8.0",
                                 0.47
                             ]
                         ]
                     },
                     {
                         "name": "Safari",
                         "id": "Safari",
                         "data": [
                             [
                                 "v11.0",
                                 3.39
                             ],
                             [
                                 "v10.1",
                                 0.96
                             ],
                             [
                                 "v10.0",
                                 0.36
                             ],
                             [
                                 "v9.1",
                                 0.54
                             ],
                             [
                                 "v9.0",
                                 0.13
                             ],
                             [
                                 "v5.1",
                                 0.2
                             ]
                         ]
                     },
                     {
                         "name": "Edge",
                         "id": "Edge",
                         "data": [
                             [
                                 "v16",
                                 2.6
                             ],
                             [
                                 "v15",
                                 0.92
                             ],
                             [
                                 "v14",
                                 0.4
                             ],
                             [
                                 "v13",
                                 0.1
                             ]
                         ]
                     },
                     {
                         "name": "Opera",
                         "id": "Opera",
                         "data": [
                             [
                                 "v50.0",
                                 0.96
                             ],
                             [
                                 "v49.0",
                                 0.82
                             ],
                             [
                                 "v12.1",
                                 0.14
                             ]
                         ]
                     }
                 ]
             }
         });
               
      </script>
      <script type="text/javascript" src="http://<?= $server ?>/admin/_class/caminho_controler.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/menu-mobile.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/_app/_financeiro/js/cadastro-padrao-pdv.js"></script>
   </body>























   <body>


                     <?php
                      /*
                     ?>
                     <section id="content3" class="tab-content">
                        
                     </section>
                     <section id="content4" class="tab-content">
                        
                           <table id="example" class="display nowrap" style="width:100%">
                              <thead>
                                 <tr  style=" background: #dadada;">
                                    <th>Cooperado</th>
                                    <th>Nº do IMEI</th>
                                    <th>Placa</th>
                                    <th>Data</th>
                                    <th>Autenticação</th>
                                    <th>Qtd</th>
                                    <th>Valor</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <tr>
                                    <td>Tiger Nixon</td>
                                    <td>System Architect</td>
                                    <td>Edinburgh</td>
                                    <td>61</td>
                                    <td>61</td>
                                    <td>61</td>
                                    <td>
                                       <div onclick="lupz()" class="lupa"></div>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>Garrett Winters</td>
                                    <td>Accountant</td>
                                    <td>Tokyo</td>
                                    <td>61</td>
                                    <td>61</td>
                                    <td>61</td>
                                    <td>
                                       <div onclick="lupz()" class="lupa"></div>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>Ashton Cox</td>
                                    <td>Junior Technical Author</td>
                                    <td>San Francisco</td>
                                    <td>61</td>
                                    <td>61</td>
                                    <td>61</td>
                                    <td>
                                       <div onclick="lupz()" class="lupa"></div>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>Cedric Kelly</td>
                                    <td>Senior Javascript Developer</td>
                                    <td>Edinburgh</td>
                                    <td>61</td>
                                    <td>22</td>
                                    <td>61</td>
                                    <td>
                                       <div onclick="lupz()" class="lupa"></div>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>Airi Satou</td>
                                    <td>Accountant</td>
                                    <td>Tokyo</td>
                                    <td>33</td>
                                    <td>61</td>
                                    <td>61</td>
                                    <td>
                                       <div onclick="lupz()" class="lupa"></div>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>Brielle Williamson</td>
                                    <td>Integration Specialist</td>
                                    <td>New York</td>
                                    <td>61</td>
                                    <td>61</td>
                                    <td>61</td>
                                    <td>
                                       <div onclick="lupz()" class="lupa"></div>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>Herrod Chandler</td>
                                    <td>Sales Assistant</td>
                                    <td>San Francisco</td>
                                    <td>59</td>
                                    <td>55</td>
                                    <td>61</td>
                                    <td>
                                       <div onclick="lupz()" class="lupa"></div>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>Rhona Davidson</td>
                                    <td>Integration Specialist</td>
                                    <td>Tokyo</td>
                                    <td>55</td>
                                    <td>61</td>
                                    <td>61</td>
                                    <td>
                                       <div onclick="lupz()" class="lupa"></div>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>Colleen Hurst</td>
                                    <td>Javascript Developer</td>
                                    <td>San Francisco</td>
                                    <td>39</td>
                                    <td>61</td>
                                    <td>61</td>
                                    <td>
                                       <div onclick="lupz()" class="lupa"></div>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>Sonya Frost</td>
                                    <td>Software Engineer</td>
                                    <td>Edinburgh</td>
                                    <td>23</td>
                                    <td>61</td>
                                    <td>61</td>
                                    <td>
                                       <div onclick="lupz()" class="lupa"></div>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>Jena Gaines</td>
                                    <td>Office Manager</td>
                                    <td>London</td>
                                    <td>30</td>
                                    <td>61</td>
                                    <td>61</td>
                                    <td>
                                       <div onclick="lupz()" class="lupa"></div>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>Quinn Flynn</td>
                                    <td>Support Lead</td>
                                    <td>Edinburgh</td>
                                    <td>22</td>
                                    <td>61</td>
                                    <td>61</td>
                                    <td>
                                       <div onclick="lupz()" class="lupa"></div>
                                    </td>
                                 </tr>
                              </tbody>
                              <tfoot>
                                 <tr>
                                    <th>Cooperado</th>
                                    <th>Nº do IMEI</th>
                                    <th>Placa</th>
                                    <th>Data</th>
                                    <th>Autenticação</th>
                                    <th>Qtd</th>
                                    <th>Valor</th>
                                 </tr>
                              </tfoot>
                           </table>
                        </div>
                        <div class="col-md-2 pull-right">
                           <button class="btn btn-warning">PASSO 2</button>
                        </div>
                        <br><br>
                     </section>
                     <section id="content6" class="tab-content">
                        
                     </section>
                     <section id="content7" class="tab-content">
                        
                     </section>
                  </div>
                  <?php
        */
      ?>
               </div>
            </div>
         </div>
      </div>
      </div>

      
      <script type="text/javascript">
         function lupz(){
           window.location.href = 'detalhe-contas-a-pagar';
         }
      </script>
      <script type="text/javascript">
         $(function(){
             $('[data-toggle="tooltip"]').tooltip();
             $(".side-nav .collapse").on("hide.bs.collapse", function() {                   
                 $(this).prev().find(".fa").eq(1).removeClass("fa-angle-right").addClass("fa-angle-down");
             });
             $('.side-nav .collapse').on("show.bs.collapse", function() {                        
                 $(this).prev().find(".fa").eq(1).removeClass("fa-angle-down").addClass("fa-angle-right");        
             });
         })    
         
         $(document).ready(function() {
         var table = $('#example').DataTable( {
         rowReorder: {
            selector: 'td:nth-child(2)'
         },
         responsive: true
         } );
         } );
      </script>
      <?php 
         $grafico = $class->Select("*","historico_contas_a_pagar","","");
         
         
         $percent = 0;
         $i = 0;
         
         
         foreach ($grafico->fetchAll(PDO::FETCH_OBJ) as $graf) {
             
         
         if($graf->status == 1){
          
           
             $percent = $percent + 1;
             
         }else{
             $i = $i + 1;
         }}
         
         
         ?>
      <script>
         Highcharts.chart('dxcdcxcxddfghbbbgghttDsasfffPkdsfLlllllllllllllllllSAsssaew', {
             chart: {
                 plotBackgroundColor: null,
                 plotBorderWidth: 0,
                 plotShadow: false
             },
             title: {
                 text: 'Status contas <br> a receber',
                 align: 'center',
                 verticalAlign: 'middle',
                 y: 40
             },
             tooltip: {
                 pointFormat: '{series.name}: <b>{point.y}</b>'
             },
             plotOptions: {
                 pie: {
                     dataLabels: {
                         enabled: true,
                         distance: -50,
                         style: {
                             fontWeight: 'bold',
                             color: 'white'
                         }
                     },
                     startAngle: -90,
                     endAngle: 90,
                     center: ['50%', '75%']
                 }
             },
             series: [{
                 type: 'pie',
                 name: 'Contas',
                 innerSize: '50%',
                 data: [
                     ['Pagas', <?= $percent ?>],
                     ['Pendentes', <?= $i ?>],
                     {
                       
                     }
                 ]
             }]
         });
         
         
               
      </script>
      <!-- <?php 
         $mes = 1;
         while( $mes <= 12){
            $teste = $class->SelectQtd("vencimento","historico_contas_a_pagar","WHERE MONTH(vencimento) ='$mes'");
         
                 $mes ++;
               }
         ?> -->
     
      <script type="text/javascript" src="http://<?= $server ?>/admin/_class/caminho_controler.js"></script>
      
      
      <script type="text/javascript" src="http://<?= $server ?>/js/menu-mobile.js"></script> 
   </body>
</html>
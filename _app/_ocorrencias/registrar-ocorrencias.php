<?php include('../../admin/controler_sys.php'); ?>
<!DOCTYPE html>
<html lang="pt-br">
   <head>
      <meta charset="utf-8">
      <meta name="robots" content="noindex, nofollow">
      <title><?php include('../../includes/title.php'); ?></title>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      
      <!-- FAV ICON -->
      <link rel="icon" type="image/png" href="http://<?= $server ?>/img/fav.png" />

      <!-- CSS -->
      <link rel="stylesheet" href="http://<?= $server ?>/css/bootstrap3.3.0.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/estilo.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/fontawesome.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/jquery.dataTables.min.css">
      

      <!-- JAVASCRIPTS -->
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.dataTables.min.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/bootstrap330.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.mask.js"></script>
   </head>
   <body>

        <style>
      label {
        width: auto;
}
    </style>


      <div id="throbber" style="display:none; min-height:120px;"></div>
      <div id="noty-holder"></div>
      <div id="wrapper">
         <!-- Navigation -->
        <?php include('../../includes/menu.php') ?>
         <div id="page-wrapper">
            <div class="container-fluid">
               <!-- Page Heading -->
               <div class="row" id="main" >
               
                  <div class="col-md-12 well">
                     <div class="col-md-12">
                        <h3 class="rlk">Registro de Ocorrência</h3>
                        <h4 style="color: #038e48;">&nbsp <b>Chamado:</b> #0001</h4> <br><br>
                     </div>
                     <div class="col-md-12">
                        <div class="col-md-3">
                           <form action="">
                              <span>Título:</span>
                              <input  value=""  class="form-control" type="text">
                              <br>
                           </form>
                        </div>
                        <div class="col-md-3">
                           <form action="">
                              <span>Assunto:</span>
                              <input  value="" class="form-control" type="text">
                              <br>
                           </form>
                        </div>
                        <div class="col-md-3">
                           <form action="">
                              <span>Para:</span>
                              <input  value="" class="form-control" type="text">
                              <br>
                           </form>
                        </div>
                        
                        <div class="col-md-3">
                           <form action="">
                              <span>Status:</span>
                              <select name="" class="form-control" id="">
                                <option value="">Selecione</option>
                                <option value="">Selecione</option>
                              </select>
                              <br>
                           </form>
                        </div>

                        <div class="col-md-12">
                           <form action="">
                              <span>Descrição:</span>
                              <textarea name="" class="form-control" id="" cols="30" rows="10"></textarea>
                              <br>
                           </form>
                        </div>

                        <div class="col-md-2">
                           <form action="">
                              <button class="btn btn-warning">Pesquisar</button>
                              <br>
                           </form>
                        </div>
                     </div>
                  </div>


            </div>




          </div>
        </div>
      </div>
    </div>
  </div>
      <script type="text/javascript">
         $(function(){
             $('[data-toggle="tooltip"]').tooltip();
             $(".side-nav .collapse").on("hide.bs.collapse", function() {                   
                 $(this).prev().find(".fa").eq(1).removeClass("fa-angle-right").addClass("fa-angle-down");
             });
             $('.side-nav .collapse').on("show.bs.collapse", function() {                        
                 $(this).prev().find(".fa").eq(1).removeClass("fa-angle-down").addClass("fa-angle-right");        
             });
         })    

      $(document).ready(function() {
      $('#example').DataTable();
  } );
      </script>
      <script>
         $(document).ready(function() {
             $('#datePicker')
                 .datepicker({
                     format: 'mm/dd/yyyy'
                 })
         
         
                 .on('changeDate', function(e) {
                     // Revalidate the date field
                     $('#eventForm').formValidation('revalidateField', 'date');
                 });
         
             $('#eventForm').formValidation({
                 framework: 'bootstrap',
                 icon: {
                     valid: 'glyphicon glyphicon-ok',
                     invalid: 'glyphicon glyphicon-remove',
                     validating: 'glyphicon glyphicon-refresh'
                 },
                 fields: {
                     name: {
                         validators: {
                             notEmpty: {
                                 message: 'The name is required'
                             }
                         }
                     },
                     date: {
                         validators: {
                             notEmpty: {
                                 message: 'The date is required'
                             },
                             date: {
                                 format: 'MM/DD/YYYY',
                                 message: 'The date is not a valid'
                             }
                         }
                     }
                 }
         
         
         
         
         
             });
         });

        $(document).ready(function(){
            //  ID do campo + sequencia que o campo deve adotar
            $('#id_do_campo').mask('000-000-000');

            //  CPF
            $('#id_do_campo_cpf').mask('000.000.000-00');
        });
      </script>
   </body>
</html>
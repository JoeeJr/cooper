<?php include('../../admin/controler_sys.php'); ?>
<!DOCTYPE html>
<html lang="pt-br">
   <head>
      <meta charset="utf-8">
      <meta name="robots" content="noindex, nofollow">
      <title><?php include('../../includes/title.php'); ?></title>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      
      <!-- FAV ICON -->
      <link rel="icon" type="image/png" href="http://<?= $server ?>/img/fav.png" />

      <!-- CSS -->
      <link rel="stylesheet" href="http://<?= $server ?>/css/bootstrap3.3.0.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/estilo.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/fontawesome.css">
      <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.1/css/responsive.dataTables.min.css">
      <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
      
    

      <!-- JAVASCRIPTS -->
      <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
      <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js" ></script>
      <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js" ></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.dataTables.min.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/bootstrap330.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.mask.js"></script>
   </head>
   <body>

        <style>
      label {
        width: auto;
}
@media (max-width: 748px){
.lupa {
    height: 20px;
    width: 20px;
    margin-top: 2px;
    cursor: pointer;
    margin-right: 30px;
    float: right;
}
}
    </style>


      <div id="throbber" style="display:none; min-height:120px;"></div>
      <div id="noty-holder"></div>
      <div id="wrapper">
         <!-- Navigation -->
        <?php include('../../includes/menu.php') ?>
         <div id="page-wrapper">
            <div class="container-fluid">
               <!-- Page Heading -->
               <div class="row" id="main" >
               
                  <div class="col-md-12 well">
                     <div class="col-md-12">
                        <h3 class="rlk">Registro de Ocorrência</h3>
                        <h4 style="color: #038e48;">&nbsp <b>Chamado:</b> #0001</h4> <br><br>
                     </div>
                     <div class="col-md-12">
                        <div class="col-md-3">
                           <form action="">
                              <span>Solicitante:</span>
                              <input  value=""  class="form-control" type="text">
                              <br>
                           </form>
                        </div>
                        <div class="col-md-3">
                           <form action="">
                              <span>Tempo em aberto:</span>
                              <input  placeholder="Dias" class="form-control" type="text">
                              <br>
                           </form>
                        </div>
                        <div class="col-md-3">
                           <form action="">
                              <span>Data de abertura:</span>
                              <input  value="" class="form-control" type="text">
                              <br>
                           </form>
                        </div>
                        
                        <div class="col-md-3">
                           <form action="">
                              <span>Status:</span>
                              <select name="" class="form-control" id="">
                                <option value="">Selecione</option>
                                <option value="">Selecione</option>
                              </select>
                              <br>
                           </form>
                        </div>

                        <div class="col-md-3">
                           <form action="">
                              <span>Encaminhado:</span>
                              <input  value="" class="form-control" type="text">
                              <br>
                           </form>
                        </div>
                          
                        <div class="col-md-1">
                           <form action="">
                              <span>&nbsp</span>
                              <button class="btn btn-warning">Buscar</button>
                              <br>
                           </form>
                        </div>

                     </div>
                  </div>



                   <div class="col-md-12 well">
                     <div class="col-md-12">
                        <table id="example" class="display nowrap" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Chamado</th>
                                            <th>Assunto</th>
                                            <th>Abertura</th>
                                            <th>Status</th>
                                            <th>#</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>001</td>
                                            <td>Joel</td>
                                            <td>Edinburgh</td>
                                            <td>Em aberto</td>
                                            <td><div onclick="lupz()" class="lupa"></div></td>
                                        </tr>
                                        <tr>
                                            <td>001</td>
                                            <td>Joel</td>
                                            <td>Edinburgh</td>
                                            <td>Em aberto</td>
                                            <td><div onclick="lupz()" class="lupa"></div></td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Chamado</th>
                                            <th>Assunto</th>
                                            <th>Abertura</th>
                                            <th>Status</th>
                                            <th>#</th>
                                        </tr>
                                    </tfoot>
                                </table>
                     </div>
                  </div>


            </div>




          </div>
        </div>
      </div>
    </div>
  </div>
      <script type="text/javascript">
         $(function(){
             $('[data-toggle="tooltip"]').tooltip();
             $(".side-nav .collapse").on("hide.bs.collapse", function() {                   
                 $(this).prev().find(".fa").eq(1).removeClass("fa-angle-right").addClass("fa-angle-down");
             });
             $('.side-nav .collapse').on("show.bs.collapse", function() {                        
                 $(this).prev().find(".fa").eq(1).removeClass("fa-angle-down").addClass("fa-angle-right");        
             });
         })    

$(document).ready(function() {
    var table = $('#example').DataTable( {
        rowReorder: {
            selector: 'td:nth-child(2)'
        },
        responsive: true
    } );
} );
      </script>

<script type="text/javascript">
function lupz(){
  window.location.href = 'detalhe-de-ocorrencias';
}


</script>



      <script>
         $(document).ready(function() {
             $('#datePicker')
                 .datepicker({
                     format: 'mm/dd/yyyy'
                 })
         
         
                 .on('changeDate', function(e) {
                     // Revalidate the date field
                     $('#eventForm').formValidation('revalidateField', 'date');
                 });
         
             $('#eventForm').formValidation({
                 framework: 'bootstrap',
                 icon: {
                     valid: 'glyphicon glyphicon-ok',
                     invalid: 'glyphicon glyphicon-remove',
                     validating: 'glyphicon glyphicon-refresh'
                 },
                 fields: {
                     name: {
                         validators: {
                             notEmpty: {
                                 message: 'The name is required'
                             }
                         }
                     },
                     date: {
                         validators: {
                             notEmpty: {
                                 message: 'The date is required'
                             },
                             date: {
                                 format: 'MM/DD/YYYY',
                                 message: 'The date is not a valid'
                             }
                         }
                     }
                 }
         
         
         
         
         
             });
         });

        $(document).ready(function(){
            //  ID do campo + sequencia que o campo deve adotar
            $('#id_do_campo').mask('000-000-000');

            //  CPF
            $('#id_do_campo_cpf').mask('000.000.000-00');
        });
      </script>
   </body>
</html>
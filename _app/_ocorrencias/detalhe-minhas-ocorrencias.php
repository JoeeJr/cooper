<?php include('../../admin/controler_sys.php'); ?>
<!DOCTYPE html>
<html lang="pt-br">
   <head>
      <meta charset="utf-8">
      <meta name="robots" content="noindex, nofollow">
      <title><?php include('../../includes/title.php'); ?></title>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      
      <!-- FAV ICON -->
      <link rel="icon" type="image/png" href="http://<?= $server ?>/img/fav.png" />

      <!-- CSS -->
      <link rel="stylesheet" href="http://<?= $server ?>/css/bootstrap3.3.0.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/estilo.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/fontawesome.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/jquery.dataTables.min.css">
      <link rel="stylesheet" href="http://<?= $server ?>/css/chats.css">
      

      <!-- JAVASCRIPTS -->
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.dataTables.min.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/bootstrap330.js"></script>
      <script type="text/javascript" src="http://<?= $server ?>/js/jquery.mask.js"></script>
   </head>
   <body>

        <style>
      label {
        width: auto;
}
@media (min-width: 992px){}
.col-md-3 {
    margin-bottom: 3rem;
}
.col-md-6 {
    margin-bottom: 3rem;
}
}
    </style>


      <div id="throbber" style="display:none; min-height:120px;"></div>
      <div id="noty-holder"></div>
      <div id="wrapper">
         <!-- Navigation -->
        <?php include('../../includes/menu.php') ?>
         <div id="page-wrapper">
            <div class="container-fluid">
               <!-- Page Heading -->
               <div class="row" id="main" >
               
                  <div class="col-md-12 well">
                     <div class="col-md-12">
                        <h3 class="rlk">Minhas ocorrencias</h3>
                        <h4 style="color: #038e48;">&nbsp <b>Chamado:</b> #0001</h4> <br><br>
                     </div>
                     <div class="col-md-12">
                        <div class="col-md-3">
                           <form action="">
                              <span>Status:</span> <br>   
                              <b>Em aberto</b>
                              <br>
                           </form>
                        </div>
                        <div class="col-md-3">
                           <form action="">
                              <span>Aberto:</span> <br>
                              <b>10/10/2018</b>
                              <br>
                           </form>
                        </div>
                        
                        <div class="col-md-6">
                           <form action="">
                              <span>Finalizado:</span> <br>
                              <b>10/05/2018</b>
                              <br>
                           </form>
                        </div>

                        <div class="col-md-3">
                               <form action="">
                                  <span>De:</span> <br>
                                  <b style="color: #3F51B5;" >Jonathas Narevicius</b>
                                  <br>
                               </form>
                            </div>


                            <div class="col-md-3">
                               <form action="">
                                  <span>Para:</span> <br>
                                  <b style="color: #3F51B5;" >Jonathas Narevicius</b>
                                  <br>
                               </form>
                            </div>

                        </div>

                     <div class="col-md-12">
                            
                             <div class="panel panel-primary">
                <div class="panel-heading">
                 
                <div class="panel-body">
                    <ul class="chat">
                        <li class="left clearfix"><span class="chat-img pull-left">
                            <img src="http://placehold.it/50/55C1E7/fff&text=U" alt="User Avatar" class="img-circle" />
                        </span>
                            <div class="chat-body clearfix">
                                <div class="header">
                                    <strong class="primary-font">Jack Sparrow</strong> <small class="pull-right text-muted">
                                        <span class="glyphicon glyphicon-time"></span>12 mins ago</small>
                                </div>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur bibendum ornare
                                    dolor, quis ullamcorper ligula sodales.
                                </p>
                            </div>
                        </li>
                        <li class="left clearfix"><span class="chat-img pull-left">
                            <img src="http://placehold.it/50/FA6F57/fff&text=" alt="User Avatar" class="img-circle" />
                        </span>
                            <div class="chat-body clearfix">
                                <div class="header">
                                    <small class="pull-right text-muted"><span class="glyphicon glyphicon-time"></span>13 mins ago</small>
                                    <strong class="pull-left primary-font">Bhaumik Patel</strong> <br>
                                </div>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur bibendum ornare
                                    dolor, quis ullamcorper ligula sodales.
                                </p>
                            </div>
                        </li>
                        <li class="left clearfix"><span class="chat-img pull-left">
                            <img src="http://placehold.it/50/55C1E7/fff&text=U" alt="User Avatar" class="img-circle" />
                        </span>
                            <div class="chat-body clearfix">
                                <div class="header">
                                    <strong class="primary-font">Jack Sparrow</strong> <small class="pull-right text-muted">
                                        <span class="glyphicon glyphicon-time"></span>14 mins ago</small>
                                </div>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur bibendum ornare
                                    dolor, quis ullamcorper ligula sodales.
                                </p>
                            </div>
                        </li>
                        <li class="left clearfix"><span class="chat-img pull-left">
                            <img src="http://placehold.it/50/55C1E7/fff&text=U" alt="User Avatar" class="img-circle" />
                        </span>
                            <div class="chat-body clearfix">
                                <div class="header">
                                    <strong class="primary-font">Jack Sparrow</strong> <small class="pull-right text-muted">
                                        <span class="glyphicon glyphicon-time"></span>14 mins ago</small>
                                </div>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur bibendum ornare
                                    dolor, quis ullamcorper ligula sodales.
                                </p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>

            <div style="padding: 0px;margin-top: 1%;" class="col-md-2 pull-right">
                <button class="btn btn-primary">Voltar</button>
            </div>

                     </div>
                   </div>






          </div>
        </div>
      </div>
    </div>
  </div>
      <script type="text/javascript">
         $(function(){
             $('[data-toggle="tooltip"]').tooltip();
             $(".side-nav .collapse").on("hide.bs.collapse", function() {                   
                 $(this).prev().find(".fa").eq(1).removeClass("fa-angle-right").addClass("fa-angle-down");
             });
             $('.side-nav .collapse').on("show.bs.collapse", function() {                        
                 $(this).prev().find(".fa").eq(1).removeClass("fa-angle-down").addClass("fa-angle-right");        
             });
         })    

      $(document).ready(function() {
      $('#example').DataTable();
  } );
      </script>

<script type="text/javascript">
function lupz(){
  window.location.href = 'detalhe-de-ocorrencias';
}
</script>



      <script>
         $(document).ready(function() {
             $('#datePicker')
                 .datepicker({
                     format: 'mm/dd/yyyy'
                 })
         
         
                 .on('changeDate', function(e) {
                     // Revalidate the date field
                     $('#eventForm').formValidation('revalidateField', 'date');
                 });
         
             $('#eventForm').formValidation({
                 framework: 'bootstrap',
                 icon: {
                     valid: 'glyphicon glyphicon-ok',
                     invalid: 'glyphicon glyphicon-remove',
                     validating: 'glyphicon glyphicon-refresh'
                 },
                 fields: {
                     name: {
                         validators: {
                             notEmpty: {
                                 message: 'The name is required'
                             }
                         }
                     },
                     date: {
                         validators: {
                             notEmpty: {
                                 message: 'The date is required'
                             },
                             date: {
                                 format: 'MM/DD/YYYY',
                                 message: 'The date is not a valid'
                             }
                         }
                     }
                 }
         
         
         
         
         
             });
         });

        $(document).ready(function(){
            //  ID do campo + sequencia que o campo deve adotar
            $('#id_do_campo').mask('000-000-000');

            //  CPF
            $('#id_do_campo_cpf').mask('000.000.000-00');
        });
      </script>
   </body>
</html>
	var host = location.host;
	var server = "";
	if (host == 'localhost'){
	  server = host+'/'+pasta;
	}else{
	  server = host;
	}
	//botao enter para ativar o btnlogar
	$('.btnValida').keypress(function validarTecla(e){
		//valida o botao enter
		if(e.which == 13){
			// Pega os dados digitados
		var user = $('#user').val();
		var pass = $('#pass').val();

		if(user == '' || pass == ''){
			// Mostra o alerta de "preencha todos os campos"
			$('.userNull').fadeIn().promise().done(function(){
		  		swal({
				    	title: 'Preencha os campos corretamente!',
				    	text: 'Insira Usuário e Senha',
				    	icon: 'error',
				    	confirmButtonColor: '#3085d6',
				    	confirmButtonText: 'Ok'
				    }).then((value) => {
		             	window.location.reload();
		            });
		  	});;
			
		}else{
			// Bloqueia os campos
			$('#user').attr("disabled", "disabled");
			$('#pass').attr("disabled", "disabled");

			// Mostra o loading
			document.getElementById("LBLlogin").innerHTML = "Aguarde";
			$('.loadL').show();

			$.ajax({
		        url: 'http://'+server+'/autenticacao',
		        type: "POST",
		        data:  {username: user, password: pass},
		        success: function(data){
		        	alert(data);
		        	if (data.trim() == 'authenticated'){
		        		window.location.href="http://"+server+"/cadastro-cooperado";
		        	}else if(data.trim() == 'not_find'){
		        		//mostra alerta de falha ao logar
		        		swal({
				    	title: 'Erro ao efetuar login!',
				    	text: 'Usuário e senha nâo existe',
				    	icon: 'error',
				    	confirmButtonColor: '#3085d6',
				    	confirmButtonText: 'Ok'
				    }).then((value) => {
		             	window.location.reload();
		            });

		        	}
		        }         
	      	});
		}
		}


	});
	// Login
	$('.BTlogin').click(function(){

		// Pega os dados digitados
		var user = $('#user').val();
		var pass = $('#pass').val();

		if(user == '' || pass == ''){
			// Mostra o alerta de "preencha todos os campos"
			$('.userNull').fadeIn().promise().done(function(){
		  		swal({
				    	title: 'Preencha os campos corretamente!',
				    	text: 'Insira Usuário e Senha',
				    	icon: 'error',
				    	confirmButtonColor: '#3085d6',
				    	confirmButtonText: 'Ok'
				    }).then((value) => {
		             	window.location.reload();
		            });
		  	});;
			
		}else{
			// Bloqueia os campos
			$('#user').attr("disabled", "disabled");
			$('#pass').attr("disabled", "disabled");

			// Mostra o loading
			document.getElementById("LBLlogin").innerHTML = "Aguarde";
			$('.loadL').show();

			$.ajax({
		        url: 'http://'+server+'/autenticacao',
		        type: "POST",
		        data:  {username: user, password: pass},
		        success: function(data){
		        	alert(data);
		        	if (data.trim() == 'authenticated'){
		        		window.location.href="http://"+server+"/cadastro-cooperado";
		        	}else if(data.trim() == 'not_find'){
		        		//mostra alerta de falha ao logar
		        		swal({
				    	title: 'Erro ao efetuar login!',
				    	text: 'Usuário e senha nâo existe',
				    	icon: 'error',
				    	confirmButtonColor: '#3085d6',
				    	confirmButtonText: 'Ok'
				    }).then((value) => {
		             	window.location.reload();
		            });

		        	}
		        }         
	      	});
		}

	});
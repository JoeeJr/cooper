<?php
   // Dados do usuário logado
   $idUsuario = $_SESSION['autenticador'];
   $usuarioLogado = $class->SelectEsp("nome", "usuarios", "WHERE hash = '".$idUsuario."'");
?>

<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
   <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="">
         <img src="http://<?= $server ?>/img/logo.svg" alt="LOGO">
      </a>
   </div>
   <!-- Top Menu Items -->
   <ul class="hidden-xs nav navbar-right top-nav">
      <li><a href="#" data-placement="bottom" data-toggle="tooltip" href="#" data-original-title="Stats"><i <i class="fas fa-user-tie"></i>
         </a>
      </li>
      <li class="dropdown">
         <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <?= $usuarioLogado ?> <b class="fa fa-angle-down"></b>
         </a>
         
         <ul class="dropdown-menu">
            <li>
               <a href="#">
                  <i style="color: #8c1019;" class="fa fa-fw fa-user"></i> Perfil
               </a>
            </li>

            <li class="divider"></li>

            <li>
               <a href="#">
                  <i style="color: #8c1019;" class="fa fa-fw fa-cog"></i> Senha
               </a>
            </li>

            <li class="divider"></li>

            <li>
               <a href="http://<?= $server ?>/logout">
                  <i style="color: #8c1019;" class="fa fa-fw fa-power-off"></i> Sair
               </a>
            </li>
         </ul>
      </li>
   </ul>


   <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
   <div class="collapse navbar-collapse navbar-ex1-collapse">
      <h3 class="hidden-xs terll">Nome do modulo aqui</h3>
      <ul class="nav navbar-nav side-nav">
         <li>
            <a style="text-align: center;" href="http://<?= $server ?>/cadastro-cooperado">
               <img class="tamanho-logo" src="http://<?= $server ?>/img/logo.svg" alt=""><br>
               <h3 class="slk"><small style="color:#a56166">Sistema de Gerenciamento</small><br>SYSCOOP</h3>
            </a>
         </li>
         
         <?php
            // Listagem dos módulos
            $cont = 0;
            $modulos = $class->Select("id_modulo, modulo, icone", "modulos", "", "ORDER BY id_modulo ASC");
            while($rowMOD = $modulos->fetch(PDO::FETCH_OBJ)){
         ?>

         <li class="li-bd">
            <a onClick="openSub(<?= $rowMOD->id_modulo ?>)" href="#" data-toggle="collapse" data-target="#submenu-<?= $rowMOD->id_modulo ?>">
               <img class="ic-ic" src="http://<?= $server ?>/img/<?= $rowMOD->icone ?>" alt=""> 
               <?php 
                  // Nome do Módulo
                  echo $rowMOD->modulo; 
               ?> 
               &nbsp;
               <i class="fas fa-angle-down"></i>
            </a>

            <ul id="submenu-<?= $rowMOD->id_modulo ?>" class="submenucol collapse <?= ($cont == 0) ? "in" : "" ?>">
               <?php
                  // Listagem de Funções do Módulo
                  $modulo = $rowMOD->id_modulo;
                  $funcoes = $class->Select("id_funcao, funcao, icone_funcao, url", "modulos_funcoes", "WHERE id_modulo = '$modulo'", "");
                  while($rowFUNC = $funcoes->fetch(PDO::FETCH_OBJ)){
               ?>
                  <li class="ativado li-bk">
                     <a href="http://<?= $server ?>/<?= $rowFUNC->url ?>">
                        <img class="ic-ic" src="http://<?= $server ?>/img/<?= $rowFUNC->icone_funcao ?>" alt="">
                        <?= $rowFUNC->funcao ?>
                     </a>
                  </li>
               <?php
                  }
               ?>
            </ul>
         </li>

         <?php
            $cont++;
            }
         ?>
         <li class="li-bd blt">
            <a style="text-align: center;" href="http://<?= $server ?>/logout">
               <img width="18%;" class="ic-ic" src="http://<?= $server ?>/img/icones/off.svg" alt="">
               <br>Sair
            </a>
         </li>
      </ul>
   </div>
   <!-- /.navbar-collapse -->
</nav>

<script type="text/javascript">   
   // Menu lateral
   function openSub(modulo){
      $('.submenucol').removeClass('collapse in');
      $('.submenucol').addClass('collapse');
   }
</script>
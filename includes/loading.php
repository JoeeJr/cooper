<style type="text/css">
	.opaca{
		width: 100%;
		height: 100%;
		background: rgba(0,0,0,.6);
		position: fixed;
		top:0;
		left:0;
		z-index: 9;
		display: none;
	}

	.gif{
		margin: 45vh auto;
		width: 47px;
    	height: 49px;
		background: white;
	}
</style>

<div id='loading' class="opaca">
	<div class="gif">
		<img src="http://<?= $server ?>/img/load.svg" alt="">
	</div>	
</div>
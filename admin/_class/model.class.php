<?php 
	//classe que efetua as funcoes do crud
	class Action{

		// Efetua o login do usuario no sistema
		function Login($user, $pass){
			// Chave de conexao
			include('key.php');

			// remete as exceções
			// $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

			// Busca o usuario
			$sql = $pdo->prepare("SELECT id FROM usuarios WHERE user = ? AND password = ? AND status = '1'");
			$sql->bindParam(1, $user);
			$sql->bindParam(2, $pass);

			// Executa
			$sql->execute();

			// Retorna a quantidade de registros encontrados
			$total = $sql->rowCount();

			// Se encontrou o usuario, incializa a sessão
			if($total > 0){
				// Inicia a sessão caso nao tenha sido iniciada
				if (!isset($_SESSION)){	session_start(); }

				// Faz o array da query para pegar o ID
				$array = $sql->fetch(PDO::FETCH_OBJ);

				// Pega o ID
				$id = $array->id;

				// Seta a sessao como logado
				$_SESSION['autenticador'] = md5($id);

				// Indica que o processo foi finalizado com sucesso
				echo "authenticated";

			}else{
				// Se não encontrar usuários, remete o erro de nao encontrado
				echo "not_find";
			}
		}

		// efetua o select e tras as informacoes na pagina followup.
		function Select($selecao,$tabela,$where,$limite){
			
			//conexao com o banco
			include ('key.php');

			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			//query de select	
			$sql = $pdo->prepare("SELECT $selecao FROM $tabela $where $limite");
			
			// Executa
			$sql->execute();
			//retorna o sql
			return $sql;
		}

		function SelectEsp($selecao,$tabela,$where){
			
			//conexao com o banco
			include ('key.php');

			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			//query de select	
			$sql = $pdo->prepare("SELECT $selecao FROM $tabela $where");
			
			// Executa
			$sql->execute();

			// Faz um Array
			$array = $sql->fetch(PDO::FETCH_OBJ);

			// Retorna a informacao desejada
			$info = $array->$selecao;

			//retorna o sql
			return $info;
		}

		function SelectQtd($selecao,$tabela,$where){
			
			//conexao com o banco
			include ('key.php');

			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			//query de select	
			$sql = $pdo->prepare("SELECT $selecao FROM $tabela $where");
			
			// Executa
			$sql->execute();

			// Qtd retornada
			$qtd = $sql->rowCount();

			//retorna o sql
			return $qtd;
		}


		public function Update($tabela, $itens, $where){
			//conexao com o banco
			include ('key.php');

			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			//query de select	
			$sql = $pdo->prepare("UPDATE $tabela SET $itens $where");
			
			// Executa
			$sql->execute();
		}


		function Insert($tabela, $col, $itens){
			//conexao com o banco
			include ('key.php');

			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			//query de select	
			$sql = $pdo->prepare("INSERT INTO $tabela ($col) VALUES ($itens)");
			
			// Executa
			$sql->execute();
		}


		function Limita($texto, $ate){
			$tamanho = strlen($texto);
			$rets = "";

			if ($tamanho > $ate){
				$rets = "...";
			}

			$txt_limit = substr($texto, 0, $ate).$rets;
			return $txt_limit;
		}
		 function Join($selecao,$tabelaA,$tabelaB,$condicao,$where,$grupo){

        	//conexao com o banco
			include ('key.php');

                $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $sql = $pdo->prepare("SELECT $selecao FROM $tabelaA INNER JOIN $tabelaB $condicao $where  $grupo");

                $sql->execute();

                return $sql;

        }
    



		function NomeMes($mes){
			// NOME DOS MESES
			$nomeMes = "";
			($mes=='01') ? $nomeMes="JAN" : "";
			($mes=='02') ? $nomeMes="FEV" : "";
			($mes=='03') ? $nomeMes="MAR" : "";
			($mes=='04') ? $nomeMes="ABR" : "";
			($mes=='05') ? $nomeMes="MAI" : "";
			($mes=='06') ? $nomeMes="JUN" : "";
			($mes=='07') ? $nomeMes="JUL" : "";
			($mes=='08') ? $nomeMes="AGO" : "";
			($mes=='09') ? $nomeMes="SET" : "";
			($mes=='10') ? $nomeMes="OUT" : "";
			($mes=='11') ? $nomeMes="NOV" : "";
			($mes=='12') ? $nomeMes="DEZ" : "";

			return $nomeMes;
		}

		function Mes($mes){
			// NOME DOS MESES
			$nomeMes = "";
			($mes=='01') ? $nomeMes="Janeiro" : "";
			($mes=='02') ? $nomeMes="Fevereiro" : "";
			($mes=='03') ? $nomeMes="Março" : "";
			($mes=='04') ? $nomeMes="Abril" : "";
			($mes=='05') ? $nomeMes="Maio" : "";
			($mes=='06') ? $nomeMes="Junho" : "";
			($mes=='07') ? $nomeMes="Julho" : "";
			($mes=='08') ? $nomeMes="Agosto" : "";
			($mes=='09') ? $nomeMes="Setembro" : "";
			($mes=='10') ? $nomeMes="Outubro" : "";
			($mes=='11') ? $nomeMes="Novembro" : "";
			($mes=='12') ? $nomeMes="Dezembro" : "";

			return $nomeMes;
		}


		function DiaSemana($data){  

	   	// Traz o dia da semana para qualquer data informada
	    $dia =  substr($data,0,2);
	    $mes =  substr($data,3,2);
	    $ano =  substr($data,6,9);

	    $diasemana = date("w", mktime(0,0,0,$mes,$dia,$ano) );

	    switch($diasemana){            
	       case"0": 
	          $diasemana = "Domingo";    
	          break;

	       case"1": 
	          $diasemana = "Segunda-Feira"; 
	          break;    

	       case"2": 
	          $diasemana = "Terça-Feira";   
	          break;  

	       case"3": 
	          $diasemana = "Quarta-Feira";  
	          break;  

	       case"4": 
	          $diasemana = "Quinta-Feira";  
	          break;  

	       case"5": 
	          $diasemana = "Sexta-Feira";   
	          break;   
	                   
	       case"6": 
	          $diasemana = "Sábado";     
	          break;          
	    }
	    return $diasemana;
	   }

	}
 ?>